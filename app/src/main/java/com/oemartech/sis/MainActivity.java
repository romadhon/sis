package com.oemartech.sis;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.SearchView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.oemartech.sis.activity.admin.PegawaiActivity;
import com.oemartech.sis.adapter.NavigationDrawerAdapter;
import com.oemartech.sis.adapter.RecipeListAdapter;
import com.oemartech.sis.fragment.DashboardFragment;
import com.oemartech.sis.fragment.InputWishlistFragment;
import com.oemartech.sis.fragment.RefundBankFragment;
import com.oemartech.sis.fragment.master.EntryCarFragment;
import com.oemartech.sis.fragment.master.EntryProductFragment;
import com.oemartech.sis.fragment.master.MasterCarFragment;
import com.oemartech.sis.fragment.master.MasterProductFragment;
import com.oemartech.sis.fragment.master.MasterTypeCar;
import com.oemartech.sis.fragment.penggajian.GajiDanKomisiFragment;
import com.oemartech.sis.fragment.penggajian.GajiPokokFragment;
import com.oemartech.sis.fragment.penggajian.GenerateKomisiFragment;
import com.oemartech.sis.fragment.report.LaporanKomisiKaryawanFragment;
import com.oemartech.sis.fragment.report.LaporanKreditFragment;
import com.oemartech.sis.fragment.report.LaporanLabaRugiFragment;
import com.oemartech.sis.fragment.report.LaporanMarketingFragment;
import com.oemartech.sis.fragment.report.LaporanMobilTerlarisFragment;
import com.oemartech.sis.fragment.report.LaporanPembelianFragment;
import com.oemartech.sis.fragment.report.LaporanPengeluaranFragment;
import com.oemartech.sis.fragment.report.LaporanPenjualanFragment;
import com.oemartech.sis.fragment.report.LaporanShowroomFragment;
import com.oemartech.sis.fragment.report.LaporanStockFragment;
import com.oemartech.sis.fragment.transaksi.PembelianFragment;
import com.oemartech.sis.fragment.transaksi.PengeluaranFragment;
import com.oemartech.sis.fragment.transaksi.PenjualanFragment;
import com.oemartech.sis.helper.CustomDialog;
import com.oemartech.sis.helper.MyDividerItemDecoration;
import com.oemartech.sis.model.Car;
import com.oemartech.sis.model.MenuModel;
import com.oemartech.sis.model.Recipe;
import com.oemartech.sis.resful.SingletonRequestQueue;
import com.oemartech.sis.utils.Constants;
import com.oemartech.sis.utils.PermissionUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private static final String TAG = MainActivity.class.getSimpleName();
    private RecyclerView recyclerView;
    private SearchView searchView;
    private List<Recipe> cartList;
    private RecipeListAdapter mAdapter;

    private FragmentManager fragmentManager;
    private Fragment fragment;
    ExpandableListAdapter expandableListAdapter;
    ExpandableListView expandableListView;
    List<MenuModel> headerList = new ArrayList<>();
    HashMap<MenuModel, List<MenuModel>> childList = new HashMap<>();
    Toolbar toolbar;
    MenuItem itemAdd;
    MenuItem itemSearch;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        setSupportActionBar(toolbar);
        fragmentManager = getSupportFragmentManager();

        cartList = new ArrayList<>();
        mAdapter = new RecipeListAdapter(this, cartList);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new MyDividerItemDecoration(this, LinearLayoutManager.VERTICAL, 16));
        recyclerView.setAdapter(mAdapter);


        expandableListView = findViewById(R.id.expandableListView);
        prepareMenuData();
        populateExpandableList();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        dashboard();
    }

    @Override
    public void onBackPressed() {
        CustomDialog.keluar(MainActivity.this);
    }

    public void hideDrawer(){
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        itemAdd = menu.findItem(R.id.action_add);
        itemSearch = menu.findItem(R.id.action_search);
        itemAdd.setVisible(false);
        itemSearch.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_add) {
            if (getSupportActionBar().getTitle().toString().equals(Constants.TITLE_MASTER_PRODUK)){
                FragmentManager manager = getSupportFragmentManager();
                Fragment frag = manager.findFragmentByTag("product");
                if (frag != null) {
                    manager.beginTransaction().remove(frag).commit();
                }
                EntryProductFragment entryProductFragment = new EntryProductFragment();
                Bundle bundle = new Bundle();
                entryProductFragment.setArguments(bundle);
                entryProductFragment.show(manager, "product");
            }else if (getSupportActionBar().getTitle().toString().equals(Constants.TITLE_MASTER_MOBIL)){
                FragmentManager manager = getSupportFragmentManager();
                Fragment frag = manager.findFragmentByTag("car");
                if (frag != null) {
                    manager.beginTransaction().remove(frag).commit();
                }
                EntryCarFragment entryCarFragment = new EntryCarFragment();
                Bundle bundle = new Bundle();
                bundle.putString("photo", "");
                bundle.putInt("user", 0);
                entryCarFragment.setArguments(bundle);
                entryCarFragment.show(manager, "car");
            }else if(getSupportActionBar().getTitle().toString().equals("SIS")){

            }
            return true;
        }else if(id == R.id.action_search){
            if (getSupportActionBar().getTitle().toString().equals(Constants.PENJUALAN)){
                SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
                searchView = (SearchView) itemSearch
                        .getActionView();
                searchView.setSearchableInfo(searchManager
                        .getSearchableInfo(getComponentName()));
                searchView.setMaxWidth(Integer.MAX_VALUE);
                searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String query) {
                        return false;
                    }
                });
            } else if (getSupportActionBar().getTitle().toString().equals(Constants.PEMBELIAN)){

            }
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_dashboard) {
            // Handle the camera action
        } else if (id == R.id.nav_master) {

        } else if (id == R.id.nav_show) {

        } else if (id == R.id.nav_setting) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }



    private void prepareMenuData() {

        int ic_home = getResources().getIdentifier("ic_home_black_24dp" , "drawable", getPackageName());
        int ic_master = getResources().getIdentifier("ic_master_24dp" , "drawable", getPackageName());
        int ic_setting = getResources().getIdentifier("ic_settings_black_24dp" , "drawable", getPackageName());

        MenuModel menuModel = new MenuModel("Dashboard", true, false, "", ic_home); //Menu of Android Tutorial. No sub menus
        headerList.add(menuModel);

        if (!menuModel.hasChildren) {
            childList.put(menuModel, null);
        }

        menuModel = new MenuModel("Master", true, true,"", 1); //Menu of Java Tutorials
        headerList.add(menuModel);
        List<MenuModel> childModelsList = new ArrayList<>();
        MenuModel childModel = new MenuModel(Constants.TITLE_MASTER_PRODUK, false, false,"", 1);
        childModelsList.add(childModel);

        childModel = new MenuModel(Constants.TITLE_MASTER_TIPE_MOBIL, false, false,"", ic_master);
        childModelsList.add(childModel);

        childModel = new MenuModel(Constants.TITLE_MASTER_MOBIL, false, false,"", ic_master);
        childModelsList.add(childModel);


        if (menuModel.hasChildren) {
            Log.d("API123","here");
            childList.put(menuModel, childModelsList);
        }

        childModelsList = new ArrayList<>();
        menuModel = new MenuModel("Salary", true, true,"", ic_setting); //Menu of Python Tutorials
        headerList.add(menuModel);
        childModel = new MenuModel(Constants.TITLE_GAJI_POKOK, false, false, "",1);
        childModelsList.add(childModel);
        childModel = new MenuModel(Constants.TITLE_GENERATE_KOMISI, false, false, "",1);
        childModelsList.add(childModel);
        childModel = new MenuModel(Constants.TITLE_GAJI_DAN_KOMISI, false, false, "",1);
        childModelsList.add(childModel);

        if (menuModel.hasChildren) {
            Log.d("API123","here");
            childList.put(menuModel, childModelsList);
        }

        childModelsList = new ArrayList<>();
        menuModel = new MenuModel("Transaction", true, true,"", ic_setting); //Menu of Python Tutorials
        headerList.add(menuModel);
        childModel = new MenuModel(Constants.TITLE_INPUT_WISHLIST_CUSTOMER, false, false, "",1);
        childModelsList.add(childModel);
        childModel = new MenuModel(Constants.PENJUALAN, false, false, "",1);
        childModelsList.add(childModel);
        childModel = new MenuModel(Constants.PEMBELIAN, false, false, "",1);
        childModelsList.add(childModel);
        childModel = new MenuModel(Constants.PENGELUARAN, false, false, "",1);
        childModelsList.add(childModel);
        childModel = new MenuModel(Constants.TITLE_REFUND_BANK, false, false, "",1);
        childModelsList.add(childModel);
        if (menuModel.hasChildren) {
            Log.d("API123","here");
            childList.put(menuModel, childModelsList);
        }

        childModelsList = new ArrayList<>();
        menuModel = new MenuModel("Report", true, true,"", ic_setting); //Menu of Python Tutorials
        headerList.add(menuModel);
        childModel = new MenuModel(Constants.TITLE_LAPORAN_PENJUALAN, false, false, "",1);
        childModelsList.add(childModel);
        childModel = new MenuModel(Constants.TITLE_LAPORAN_KREDIT, false, false, "",1);
        childModelsList.add(childModel);
        childModel = new MenuModel(Constants.TITLE_LAPORAN_SHOWROOM, false, false, "",1);
        childModelsList.add(childModel);
        childModel = new MenuModel(Constants.TITLE_LAPORAN_PEMBELIAN, false, false, "",1);
        childModelsList.add(childModel);
        childModel = new MenuModel(Constants.TITLE_LAPORAN_PENGELUARAN, false, false, "",1);
        childModelsList.add(childModel);
        childModel = new MenuModel(Constants.TITLE_LAPORAN_KOMISI_KARYAWAN, false, false, "",1);
        childModelsList.add(childModel);
        childModel = new MenuModel(Constants.TITLE_LAPORAN_LABA_RUGI, false, false, "",1);
        childModelsList.add(childModel);
        childModel = new MenuModel(Constants.TITLE_LAPORAN_MARKETING, false, false, "",1);
        childModelsList.add(childModel);
        childModel = new MenuModel(Constants.TITLE_LAPORAN_MOBIL_TERLARIS, false, false, "",1);
        childModelsList.add(childModel);
        childModel = new MenuModel(Constants.TITLE_LAPORAN_STOCK, false, false, "",1);
        childModelsList.add(childModel);

        if (menuModel.hasChildren) {
            Log.d("API123","here");
            childList.put(menuModel, childModelsList);
        }

        childModelsList = new ArrayList<>();
        menuModel = new MenuModel("Admin", true, true,"", ic_setting); //Menu of Python Tutorials
        headerList.add(menuModel);
        childModel = new MenuModel(Constants.ADD_USER, false, false, "",1);
        childModelsList.add(childModel);
        if (menuModel.hasChildren) {
            childList.put(menuModel, childModelsList);
        }

        menuModel = new MenuModel("Kirim Notifikasi", false, false, "", ic_home); //Menu of Android Tutorial. No sub menus
        headerList.add(menuModel);

        if (!menuModel.hasChildren) {
            childList.put(menuModel, null);
        }
    }

    private void populateExpandableList() {

        expandableListAdapter = new NavigationDrawerAdapter(this, headerList, childList);
        expandableListView.setAdapter(expandableListAdapter);

        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {

                if (headerList.get(groupPosition).isGroup) {
                    if (!headerList.get(groupPosition).hasChildren) {
                        itemAdd.setVisible(false);
                        itemSearch.setVisible(false);
                        dashboard();
                        hideDrawer();
                    }
                } else {
                    if (headerList.get(groupPosition).menuName.equals("Kirim Notifikasi")){
                        sendNotification();
                    }
                }

                return false;
            }
        });

        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

                if (childList.get(headerList.get(groupPosition)) != null) {
                    MenuModel model = childList.get(headerList.get(groupPosition)).get(childPosition);
                    if (model.menuName.equals(Constants.TITLE_MASTER_PRODUK)) {
                        if (PermissionUtils.permissionMasterProductOrCarMenu()){
                            getSupportActionBar().setTitle(Constants.TITLE_MASTER_PRODUK);
                            itemAdd.setVisible(true);
                            itemSearch.setVisible(false);
                            fragmentManager.beginTransaction().
                                    replace(R.id.frameLayout, MasterProductFragment.newInstance()).
                                    commit();
                        } else {
                            CustomDialog.pesan(MainActivity.this, "You do not have permission");
                        }

                        hideDrawer();
                    }else if (model.menuName.equals(Constants.TITLE_MASTER_TIPE_MOBIL)) {
                        if (PermissionUtils.permissionMasterProductOrCarMenu()){
                            getSupportActionBar().setTitle(Constants.TITLE_MASTER_TIPE_MOBIL);
                            itemAdd.setVisible(true);
                            itemSearch.setVisible(false);
                            fragmentManager.beginTransaction().
                                    replace(R.id.frameLayout, MasterTypeCar.newInstance()).
                                    commit();
                        } else {
                            CustomDialog.pesan(MainActivity.this, "You do not have permission");
                        }

                        hideDrawer();
                    }else if (model.menuName.equals(Constants.TITLE_MASTER_MOBIL)) {
                        if (PermissionUtils.permissionMasterProductOrCarMenu()){
                            getSupportActionBar().setTitle(Constants.TITLE_MASTER_MOBIL);
                            itemAdd.setVisible(true);
                            itemSearch.setVisible(false);
                            fragmentManager.beginTransaction().
                                    replace(R.id.frameLayout, MasterCarFragment.newInstance()).
                                    commit();
                        } else {
                            CustomDialog.pesan(MainActivity.this, "You do not have permission");
                        }

                        hideDrawer();
                    }else if (model.menuName.equals(Constants.TITLE_GAJI_POKOK)) {
                        if (PermissionUtils.permissionSalaryMenu()){
                            getSupportActionBar().setTitle(Constants.TITLE_GAJI_POKOK);
                            itemAdd.setVisible(false);
                            itemSearch.setVisible(false);
                            fragmentManager.beginTransaction().
                                    replace(R.id.frameLayout, GajiPokokFragment.newInstance()).
                                    commit();
                        } else {
                            CustomDialog.pesan(MainActivity.this, "You do not have permission");
                        }

                        hideDrawer();
                    }else if (model.menuName.equals(Constants.TITLE_GENERATE_KOMISI)) {

                        if (PermissionUtils.permissionSalaryMenu()){

                            getSupportActionBar().setTitle(Constants.TITLE_GENERATE_KOMISI);
                            itemAdd.setVisible(false);
                            itemSearch.setVisible(false);
                            fragmentManager.beginTransaction().
                                    replace(R.id.frameLayout, GenerateKomisiFragment.newInstance()).
                                    commit();
                        } else {
                            CustomDialog.pesan(MainActivity.this, "You do not have permission");
                        }

                        hideDrawer();

                    }else if (model.menuName.equals(Constants.TITLE_GAJI_DAN_KOMISI)) {

                            getSupportActionBar().setTitle(Constants.TITLE_GAJI_DAN_KOMISI);
                            itemAdd.setVisible(false);
                            itemSearch.setVisible(false);
                            fragmentManager.beginTransaction().
                                    replace(R.id.frameLayout, GajiDanKomisiFragment.newInstance()).
                                    commit();
                        hideDrawer();

                    }else if (model.menuName.equals(Constants.PENJUALAN)) {
                        if (PermissionUtils.permissionSalesMenu()){
                            getSupportActionBar().setTitle(Constants.PENJUALAN);
                            itemAdd.setVisible(false);
                            itemSearch.setVisible(false);
                            fragmentManager.beginTransaction().
                                    replace(R.id.frameLayout, PenjualanFragment.newInstance()).
                                    commit();
                        } else {
                            CustomDialog.pesan(MainActivity.this, "You do not have permission");
                        }

                        hideDrawer();
                    }else if (model.menuName.equals(Constants.PEMBELIAN)) {
                        if (PermissionUtils.permissionPurchaseMenu()){
                            getSupportActionBar().setTitle(Constants.PEMBELIAN);
                            itemAdd.setVisible(false);
                            itemSearch.setVisible(false);
                            fragmentManager.beginTransaction().
                                    replace(R.id.frameLayout, PembelianFragment.newInstance()).
                                    commit();
                        } else {
                            CustomDialog.pesan(MainActivity.this, "You do not have permission");
                        }

                        hideDrawer();
                    }else if (model.menuName.equals(Constants.TITLE_REFUND_BANK)) {
                        if (PermissionUtils.permissionPurchaseMenu()){
                            getSupportActionBar().setTitle(Constants.TITLE_REFUND_BANK);
                            itemAdd.setVisible(false);
                            itemSearch.setVisible(false);
                            fragmentManager.beginTransaction().
                                    replace(R.id.frameLayout, RefundBankFragment.newInstance()).
                                    commit();
                        } else {
                            CustomDialog.pesan(MainActivity.this, "You do not have permission");
                        }

                        hideDrawer();
                    }else if (model.menuName.equals(Constants.PENGELUARAN)) {
                        if (PermissionUtils.permissionPurchaseMenu()){
                            getSupportActionBar().setTitle(Constants.PENGELUARAN);
                            itemAdd.setVisible(false);
                            itemSearch.setVisible(false);
                            fragmentManager.beginTransaction().
                                    replace(R.id.frameLayout, PengeluaranFragment.newInstance()).
                                    commit();
                        } else {
                            CustomDialog.pesan(MainActivity.this, "You do not have permission");
                        }

                        hideDrawer();
                    }else if (model.menuName.equals(Constants.TITLE_INPUT_WISHLIST_CUSTOMER)) {
                        if (PermissionUtils.permissionPurchaseMenu()){
                            getSupportActionBar().setTitle(Constants.TITLE_INPUT_WISHLIST_CUSTOMER);
                            itemAdd.setVisible(false);
                            itemSearch.setVisible(false);
                            fragmentManager.beginTransaction().
                                    replace(R.id.frameLayout, InputWishlistFragment.newInstance()).
                                    commit();
                        } else {
                            CustomDialog.pesan(MainActivity.this, "You do not have permission");
                        }

                        hideDrawer();
                    }else if (model.menuName.equals(Constants.TITLE_LAPORAN_PENJUALAN)) {

                        if (PermissionUtils.permissionSalesReportMenu()){
                            getSupportActionBar().setTitle(Constants.TITLE_LAPORAN_PENJUALAN);
                            itemAdd.setVisible(false);
                            itemSearch.setVisible(false);
                            fragmentManager.beginTransaction().
                                    replace(R.id.frameLayout, LaporanPenjualanFragment.newInstance()).
                                    commit();
                        } else {
                            CustomDialog.pesan(MainActivity.this, "You do not have permission");
                        }

                        hideDrawer();
                    }else if (model.menuName.equals(Constants.TITLE_LAPORAN_PEMBELIAN)) {

                        if (PermissionUtils.permissionPurchaseReportMenu()){
                            getSupportActionBar().setTitle(Constants.TITLE_LAPORAN_PEMBELIAN);
                            itemAdd.setVisible(false);
                            itemSearch.setVisible(false);
                            fragmentManager.beginTransaction().
                                    replace(R.id.frameLayout, LaporanPembelianFragment.newInstance()).
                                    commit();
                        } else {
                            CustomDialog.pesan(MainActivity.this, "You do not have permission");
                        }

                        hideDrawer();
                    }else if (model.menuName.equals(Constants.TITLE_LAPORAN_STOCK)) {

                        if (PermissionUtils.permissionStockReportMenu()){
                            getSupportActionBar().setTitle(Constants.TITLE_LAPORAN_STOCK);
                            itemAdd.setVisible(false);
                            itemSearch.setVisible(false);
                            fragmentManager.beginTransaction().
                                    replace(R.id.frameLayout, LaporanStockFragment.newInstance()).
                                    commit();
                        } else {
                            CustomDialog.pesan(MainActivity.this, "You do not have permission");
                        }

                        hideDrawer();
                    }else if (model.menuName.equals(Constants.TITLE_LAPORAN_LABA_RUGI)) {
                        if (PermissionUtils.permissionLabaRugiReportMenu()){
                            getSupportActionBar().setTitle(Constants.TITLE_LAPORAN_LABA_RUGI);
                            itemAdd.setVisible(false);
                            itemSearch.setVisible(false);
                            fragmentManager.beginTransaction().
                                    replace(R.id.frameLayout, LaporanLabaRugiFragment.newInstance()).
                                    commit();
                        } else {
                            CustomDialog.pesan(MainActivity.this, "You do not have permission");
                        }

                        hideDrawer();

                    } else if (model.menuName.equals(Constants.TITLE_LAPORAN_KOMISI_KARYAWAN)) {
                        if (PermissionUtils.permissionLabaRugiReportMenu()){
                            getSupportActionBar().setTitle(Constants.TITLE_LAPORAN_KOMISI_KARYAWAN);
                            itemAdd.setVisible(false);
                            itemSearch.setVisible(false);
                            fragmentManager.beginTransaction().
                                    replace(R.id.frameLayout, LaporanKomisiKaryawanFragment.newInstance()).
                                    commit();
                        } else {
                            CustomDialog.pesan(MainActivity.this, "You do not have permission");
                        }

                        hideDrawer();

                    }else if (model.menuName.equals(Constants.TITLE_LAPORAN_PENGELUARAN)) {
                        if (PermissionUtils.permissionLabaRugiReportMenu()){
                            getSupportActionBar().setTitle(Constants.TITLE_LAPORAN_PENGELUARAN);
                            itemAdd.setVisible(false);
                            itemSearch.setVisible(false);
                            fragmentManager.beginTransaction().
                                    replace(R.id.frameLayout, LaporanPengeluaranFragment.newInstance()).
                                    commit();
                        } else {
                            CustomDialog.pesan(MainActivity.this, "You do not have permission");
                        }

                        hideDrawer();

                    }else if (model.menuName.equals(Constants.TITLE_LAPORAN_MOBIL_TERLARIS)) {
                        if (PermissionUtils.permissionLabaRugiReportMenu()){
                            getSupportActionBar().setTitle(Constants.TITLE_LAPORAN_MOBIL_TERLARIS);
                            itemAdd.setVisible(false);
                            itemSearch.setVisible(false);
                            fragmentManager.beginTransaction().
                                    replace(R.id.frameLayout, LaporanMobilTerlarisFragment.newInstance()).
                                    commit();
                        } else {
                            CustomDialog.pesan(MainActivity.this, "You do not have permission");
                        }

                        hideDrawer();

                    }else if (model.menuName.equals(Constants.TITLE_LAPORAN_KREDIT)) {
                        if (PermissionUtils.permissionLabaRugiReportMenu()){
                            getSupportActionBar().setTitle(Constants.TITLE_LAPORAN_KREDIT);
                            itemAdd.setVisible(false);
                            itemSearch.setVisible(false);
                            fragmentManager.beginTransaction().
                                    replace(R.id.frameLayout, LaporanKreditFragment.newInstance()).
                                    commit();
                        } else {
                            CustomDialog.pesan(MainActivity.this, "You do not have permission");
                        }

                        hideDrawer();

                    }else if (model.menuName.equals(Constants.TITLE_LAPORAN_MARKETING)) {

                        if (PermissionUtils.permissionKomisiReportMenu()){
                            getSupportActionBar().setTitle(Constants.TITLE_LAPORAN_MARKETING);
                            itemAdd.setVisible(false);
                            itemSearch.setVisible(false);
                            fragmentManager.beginTransaction().
                                    replace(R.id.frameLayout, LaporanMarketingFragment.newInstance()).
                                    commit();
                        } else {
                            CustomDialog.pesan(MainActivity.this, "You do not have permission");
                        }


                        hideDrawer();
                    }else if (model.menuName.equals(Constants.TITLE_LAPORAN_SHOWROOM)) {
                        if (PermissionUtils.permissionPengeluaranReportMenu()){
                            getSupportActionBar().setTitle(Constants.TITLE_LAPORAN_SHOWROOM);
                            itemAdd.setVisible(false);
                            itemSearch.setVisible(false);
                            fragmentManager.beginTransaction().
                                    replace(R.id.frameLayout, LaporanShowroomFragment.newInstance()).
                                    commit();
                        } else {
                            CustomDialog.pesan(MainActivity.this, "You do not have permission");
                        }


                        hideDrawer();
                    }else if (model.menuName.equals(Constants.ADD_STAFF)) {
                        Intent intent = new Intent(MainActivity.this, PegawaiActivity.class);
                        startActivity(intent);

                    }else if (model.menuName.equals(Constants.ADD_USER)) {
                        if (PermissionUtils.permissionAddUserMenu()){
                            Intent intent = new Intent(MainActivity.this, PegawaiActivity.class);
                            startActivity(intent);
                        } else {
                            CustomDialog.pesan(MainActivity.this, "You do not have permission");
                        }
                    }

                }

                return false;
            }
        });
    }

    public void dashboard(){
        getSupportActionBar().setTitle(Constants.TITLE_DASHBOARD);
        fragmentManager.beginTransaction().
                replace(R.id.frameLayout, DashboardFragment.newInstance()).
                commit();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    public void refresh(String type){
        if (type.equals(Constants.TITLE_MASTER_PRODUK)){
            fragmentManager.beginTransaction().
                    replace(R.id.frameLayout, MasterProductFragment.newInstance()).
                    commit();
        }else if (type.equals(Constants.TITLE_MASTER_MOBIL)){
            fragmentManager.beginTransaction().
                    replace(R.id.frameLayout, MasterCarFragment.newInstance()).
                    commit();
        }
    }

    private void sendNotification() {

        RequestQueue queue = SingletonRequestQueue.getInstance(this).getRequestQueue();

        VolleyLog.DEBUG = true;
        String uri = Constants.IP+"broadcast";

        StringRequest stringRequestPOSTJSON = new StringRequest(Request.Method.GET, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

            }
        }, errorListener) {
            @Override
            public Request.Priority getPriority() {
                return Request.Priority.HIGH;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", Constants.CONTENT_TYPE);
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                String requestBody = null;


                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        queue.add(stringRequestPOSTJSON);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request request) {
            }
        });
    }

    Response.ErrorListener errorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            if (error instanceof NetworkError) {
                Toast.makeText(MainActivity.this, "No network available", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(MainActivity.this, error.toString(), Toast.LENGTH_LONG).show();
            }
        }
    };

}
