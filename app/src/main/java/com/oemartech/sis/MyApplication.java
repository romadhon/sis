package com.oemartech.sis;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

/**
 * Created by Romadhon Akbar Sholeh on 9/3/2018.
 */

public class MyApplication extends Application {
    private static final String TAG = Context.class.getName();

    private static Context instance;
    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    //for multidex
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        MultiDex.install(this);
    }
}