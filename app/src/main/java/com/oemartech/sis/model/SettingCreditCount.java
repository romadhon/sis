package com.oemartech.sis.model;

public class SettingCreditCount {

    private int provisi;
    private int bunga;
    private float asuransi;
    private float credit_protection;
    private String admin_cost;

    public SettingCreditCount(int provisi,
            int bunga,
            float asuransi,
            float credit_protection,
            String admin_cost){
        this.provisi = provisi;
        this.bunga = bunga;
        this.asuransi = asuransi;
        this.credit_protection = credit_protection;
        this.admin_cost = admin_cost;
    }

    public void setProvisi(int provisi) {
        this.provisi = provisi;
    }

    public int getProvisi() {
        return provisi;
    }

    public void setBunga(int bunga) {
        this.bunga = bunga;
    }

    public int getBunga() {
        return bunga;
    }

    public void setAsuransi(float asuransi) {
        this.asuransi = asuransi;
    }

    public float getAsuransi() {
        return asuransi;
    }

    public void setCredit_protection(float credit_protection) {
        this.credit_protection = credit_protection;
    }

    public float getCredit_protection() {
        return credit_protection;
    }

    public void setAdmin_cost(String admin_cost) {
        this.admin_cost = admin_cost;
    }

    public String getAdmin_cost() {
        return admin_cost;
    }
}
