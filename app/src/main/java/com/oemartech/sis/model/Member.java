package com.oemartech.sis.model;

public class Member {
    int id;
    String nama;
    String telp;
    String alamat;

    public Member(int id, String nama,String telp, String alamat){
        this.id = id;
        this.nama = nama;
        this.telp = telp;
        this.alamat = alamat;
    }

    public int getId() {
        return id;
    }

    public String getNama() {
        return nama;
    }

    public String getTelp() {
        return telp;
    }

    public String getAlamat() {
        return alamat;
    }

}
