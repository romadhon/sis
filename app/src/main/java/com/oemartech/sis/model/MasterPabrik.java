package com.oemartech.sis.model;

public class MasterPabrik {
    int ID_Pabrikan;
    String Nama_Pabrikan;
    String Keterangan;

    public void setID_Pabrikan(int ID_Pabrikan) {
        this.ID_Pabrikan = ID_Pabrikan;
    }

    public int getID_Pabrikan() {
        return ID_Pabrikan;
    }

    public void setNama_Pabrikan(String nama_Pabrikan) {
        Nama_Pabrikan = nama_Pabrikan;
    }

    public String getNama_Pabrikan() {
        return Nama_Pabrikan;
    }

    public void setKeterangan(String keterangan) {
        Keterangan = keterangan;
    }

    public String getKeterangan() {
        return Keterangan;
    }
}
