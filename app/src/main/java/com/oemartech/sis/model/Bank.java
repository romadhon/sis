package com.oemartech.sis.model;

import java.io.Serializable;

public class Bank implements Serializable {

    String id;
    String namabank;
    String created_at;
    String updated_at;

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setNamabank(String namabank) {
        this.namabank = namabank;
    }

    public String getNamabank() {
        return namabank;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }
}
