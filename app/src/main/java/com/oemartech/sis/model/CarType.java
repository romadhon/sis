package com.oemartech.sis.model;

public class CarType {
    String idjenismobil;
    String namajenismobil;
    String Nama_Pabrikan;


    public void setIdjenismobil(String idjenismobil) {
        this.idjenismobil = idjenismobil;
    }

    public String getIdjenismobil() {
        return idjenismobil;
    }

    public void setNama_Pabrikan(String nama_Pabrikan) {
        Nama_Pabrikan = nama_Pabrikan;
    }

    public String getNama_Pabrikan() {
        return Nama_Pabrikan;
    }

    public void setNamajenismobil(String namajenismobil) {
        this.namajenismobil = namajenismobil;
    }

    public String getNamajenismobil() {
        return namajenismobil;
    }
}
