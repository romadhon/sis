package com.oemartech.sis.model;

public class Komisi {

    private String name;
    private String nominal;
    private String iddbeli;
    private String status;
    private String bulan;

    public void setIddbeli(String iddbeli) {
        this.iddbeli = iddbeli;
    }

    public String getIddbeli() {
        return iddbeli;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setNominal(String nominal) {
        this.nominal = nominal;
    }

    public String getNominal() {
        return nominal;
    }

    public void setBulan(String bulan) {
        this.bulan = bulan;
    }

    public String getBulan() {
        return bulan;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}
