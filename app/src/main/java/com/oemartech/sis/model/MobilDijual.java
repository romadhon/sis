package com.oemartech.sis.model;

public class MobilDijual {
    int id;
    String platnomor;
    String warna;
    String price;
    int status_booking;
    int status_hold;
    String kondisi;
    String url;
    String name;
    String type;
    String transmission;
    String year;

    public MobilDijual(
            int id,
            String platnomor,
            String warna,
            String price,
            int status_booking,
            int status_hold,
            String kondisi,
            String url,
            String name,
            String type,
            String transmission,
            String year){
                this.id = id;
                this.platnomor = platnomor;
                this.warna = warna;
                this.price = price;
                this.status_booking = status_booking;
                this.status_hold =status_hold;
                this.kondisi = kondisi;
                this.url = url;
                this.name = name;
                this.type = type;
                this.transmission = transmission;
                this.year = year;
    }

    public int getId() {
        return id;
    }

    public String getPlatnomor() {
        return platnomor;
    }

    public String getWarna() {
        return warna;
    }

    public String getKondisi() {
        return kondisi;
    }

    public int getStatus_booking() {
        return status_booking;
    }

    public int getStatus_hold() {
        return status_hold;
    }

    public String getUrl() {
        return url;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public String getTransmission() {
        return transmission;
    }

    public String getYear() {
        return year;
    }

    public String getPrice() {
        return price;
    }
}
