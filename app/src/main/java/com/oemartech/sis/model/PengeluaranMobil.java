package com.oemartech.sis.model;

public class PengeluaranMobil {

    private String id_datamobil;
    private String id_pabrikan;
    private String id_jenismobil;
    private String id_varian;
    private String idshowroom;
    private String platnomor;
    private String kilometer;
    private String bahanbakar;
    private String expstnk;
    private String warna;
    private String tahun;
    private String hargabeli;
    private String hargajual;
    private String url;

    public void setPlatnomor(String platnomor) {
        this.platnomor = platnomor;
    }

    public String getPlatnomor() {
        return platnomor;
    }

    public void setBahanbakar(String bahanbakar) {
        this.bahanbakar = bahanbakar;
    }

    public String getBahanbakar() {
        return bahanbakar;
    }

    public void setTahun(String tahun) {
        this.tahun = tahun;
    }

    public String getTahun() {
        return tahun;
    }

    public void setIdshowroom(String idshowroom) {
        this.idshowroom = idshowroom;
    }

    public String getIdshowroom() {
        return idshowroom;
    }

    public void setExpstnk(String expstnk) {
        this.expstnk = expstnk;
    }

    public String getExpstnk() {
        return expstnk;
    }

    public void setId_datamobil(String id_datamobil) {
        this.id_datamobil = id_datamobil;
    }

    public String getId_datamobil() {
        return id_datamobil;
    }

    public void setHargabeli(String hargabeli) {
        this.hargabeli = hargabeli;
    }

    public String getHargabeli() {
        return hargabeli;
    }

    public void setHargajual(String hargajual) {
        this.hargajual = hargajual;
    }

    public String getHargajual() {
        return hargajual;
    }

    public void setId_jenismobil(String id_jenismobil) {
        this.id_jenismobil = id_jenismobil;
    }

    public String getId_jenismobil() {
        return id_jenismobil;
    }

    public void setId_pabrikan(String id_pabrikan) {
        this.id_pabrikan = id_pabrikan;
    }

    public String getId_pabrikan() {
        return id_pabrikan;
    }

    public void setId_varian(String id_varian) {
        this.id_varian = id_varian;
    }

    public String getId_varian() {
        return id_varian;
    }

    public void setKilometer(String kilometer) {
        this.kilometer = kilometer;
    }

    public String getKilometer() {
        return kilometer;
    }

    public void setWarna(String warna) {
        this.warna = warna;
    }

    public String getWarna() {
        return warna;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}
