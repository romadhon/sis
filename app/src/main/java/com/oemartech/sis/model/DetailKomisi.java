package com.oemartech.sis.model;

public class DetailKomisi {

    private String namajenismobil;
    private String platnomor;
    private String namashowroom;
    private String hargaakhir;
    private String namacust;

    public void setNamacust(String namacust) {
        this.namacust = namacust;
    }

    public String getNamacust() {
        return namacust;
    }

    public void setHargaakhir(String hargaakhir) {
        this.hargaakhir = hargaakhir;
    }

    public String getHargaakhir() {
        return hargaakhir;
    }

    public void setPlatnomor(String platnomor) {
        this.platnomor = platnomor;
    }

    public String getPlatnomor() {
        return platnomor;
    }

    public void setNamashowroom(String namashowroom) {
        this.namashowroom = namashowroom;
    }

    public String getNamashowroom() {
        return namashowroom;
    }

    public void setNamajenismobil(String namajenismobil) {
        this.namajenismobil = namajenismobil;
    }

    public String getNamajenismobil() {
        return namajenismobil;
    }
}
