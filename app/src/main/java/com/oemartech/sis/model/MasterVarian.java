package com.oemartech.sis.model;

public class MasterVarian {
    int id_varian;
    String namajenismobil;
    String nama_varian;

    public void setId_varian(int id_varian) {
        this.id_varian = id_varian;
    }

    public int getId_varian() {
        return id_varian;
    }

    public void setNamajenismobil(String namajenismobil) {
        this.namajenismobil = namajenismobil;
    }

    public String getNamajenismobil() {
        return namajenismobil;
    }

    public void setNama_varian(String nama_varian) {
        this.nama_varian = nama_varian;
    }

    public String getNama_varian() {
        return nama_varian;
    }
}
