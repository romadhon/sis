package com.oemartech.sis.model;

public class Staff {
    int id;
    String nama;
    String password;
    String role;

    public Staff(int id, String nama, String password, String role){
        this.id = id;
        this.nama = nama;
        this.password = password;
        this.role = role;
    }

    public int getId() {
        return id;
    }

    public String getNama() {
        return nama;
    }

    public String getPassword() {
        return password;
    }

    public String getRole() {
        return role;
    }
}
