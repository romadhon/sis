package com.oemartech.sis.model;


import android.app.Activity;
import android.content.Context;

import java.io.Serializable;
import java.util.ArrayList;

public class GlobalVar implements Serializable {
    private static final long serialVersionUID = 4209785791151665560L;

    private static GlobalVar instance;
    private String id;
    private String role;
    private String password;
    private String nama;
    private String email;
    private String nohp;
    private String alamat;
    private String imageUrl;
    private Activity activity;

    static {
        instance = new GlobalVar();
    }

    private GlobalVar() {
    }

    public static GlobalVar getInstance() {
        return GlobalVar.instance;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public String getRole() {
        return role;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNama() {
        return nama;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setNohp(String nohp) {
        this.nohp = nohp;
    }

    public String getNohp() {
        return nohp;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public Activity getActivity() {
        return activity;
    }
}
