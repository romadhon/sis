package com.oemartech.sis.model;

public class MasterMobildijual {
    private int id_datamobil;
    private int id_varian;
    private int id_pabrikan;
    private int id_jenismobil;
    private int idshowroom;
    private String namashowroom;
    private String Nama_Pabrikan;
    private String namajenismobil;
    private String nama_varian;
    private String bahanbakar;
    private String cc;
    private String warna;
    private String platnomor;
    private String kilometer;
    private String tahun;
    private String expstnk;
    private double hargabeli;
    private double hargajual;
    private int status_booking;
    private int status_hold;
    private String kondisi;
    private String url;
    private String transmisi;

    public void setIdshowroom(int idshowroom) {
        this.idshowroom = idshowroom;
    }

    public int getIdshowroom() {
        return idshowroom;
    }

    public void setId_varian(int id_varian) {
        this.id_varian = id_varian;
    }

    public int getId_varian() {
        return id_varian;
    }

    public void setId_jenismobil(int id_jenismobil) {
        this.id_jenismobil = id_jenismobil;
    }

    public int getId_jenismobil() {
        return id_jenismobil;
    }

    public void setId_pabrikan(int id_pabrikan) {
        this.id_pabrikan = id_pabrikan;
    }

    public int getId_pabrikan() {
        return id_pabrikan;
    }

    public void setNamashowroom(String namashowroom) {
        this.namashowroom = namashowroom;
    }

    public String getNamashowroom() {
        return namashowroom;
    }

    public void setNama_varian(String nama_varian) {
        this.nama_varian = nama_varian;
    }

    public String getNama_varian() {
        return nama_varian;
    }

    public void setNamajenismobil(String namajenismobil) {
        this.namajenismobil = namajenismobil;
    }

    public String getNamajenismobil() {
        return namajenismobil;
    }

    public void setNama_Pabrikan(String nama_Pabrikan) {
        Nama_Pabrikan = nama_Pabrikan;
    }

    public String getNama_Pabrikan() {
        return Nama_Pabrikan;
    }

    public void setBahanbakar(String bahanbakar) {
        this.bahanbakar = bahanbakar;
    }

    public String getBahanbakar() {
        return bahanbakar;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    public String getCc() {
        return cc;
    }

    public void setId_datamobil(int id_datamobil) {
        this.id_datamobil = id_datamobil;
    }

    public int getId_datamobil() {
        return id_datamobil;
    }

    public void setWarna(String warna) {
        this.warna = warna;
    }

    public String getWarna() {
        return warna;
    }

    public void setExpstnk(String expstnk) {
        this.expstnk = expstnk;
    }

    public String getExpstnk() {
        return expstnk;
    }

    public void setHargabeli(double hargabeli) {
        this.hargabeli = hargabeli;
    }

    public double getHargabeli() {
        return hargabeli;
    }

    public void setKilometer(String kilometer) {
        this.kilometer = kilometer;
    }

    public String getKilometer() {
        return kilometer;
    }

    public void setHargajual(double hargajual) {
        this.hargajual = hargajual;
    }

    public double getHargajual() {
        return hargajual;
    }

    public void setKondisi(String kondisi) {
        this.kondisi = kondisi;
    }

    public String getKondisi() {
        return kondisi;
    }

    public void setPlatnomor(String platnomor) {
        this.platnomor = platnomor;
    }

    public String getPlatnomor() {
        return platnomor;
    }

    public void setStatus_booking(int status_booking) {
        this.status_booking = status_booking;
    }

    public int getStatus_booking() {
        return status_booking;
    }

    public void setTahun(String tahun) {
        this.tahun = tahun;
    }

    public String getTahun() {
        return tahun;
    }

    public void setStatus_hold(int status_hold) {
        this.status_hold = status_hold;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getStatus_hold() {
        return status_hold;
    }

    public void setTransmisi(String transmisi) {
        this.transmisi = transmisi;
    }

    public String getTransmisi() {
        return transmisi;
    }
}
