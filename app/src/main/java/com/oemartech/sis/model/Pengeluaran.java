package com.oemartech.sis.model;

import java.io.Serializable;

public class Pengeluaran implements Serializable {

    String id_pengeluaran;
    String nama_pengeluaran;
    String nominal;
    String created_at;
    String updated_at;

    public void setId_pengeluaran(String id_pengeluaran) {
        this.id_pengeluaran = id_pengeluaran;
    }

    public String getId_pengeluaran() {
        return id_pengeluaran;
    }

    public void setNama_pengeluaran(String nama_pengeluaran) {
        this.nama_pengeluaran = nama_pengeluaran;
    }

    public String getNama_pengeluaran() {
        return nama_pengeluaran;
    }

    public void setNominal(String nominal) {
        this.nominal = nominal;
    }

    public String getNominal() {
        return nominal;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }
}
