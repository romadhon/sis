package com.oemartech.sis.model;

public class MasterMobil {
    int idjenismobil;
    String Nama_Pabrikan;
    String namajenismobil;

    public void setIdjenismobil(int idjenismobil) {
        this.idjenismobil = idjenismobil;
    }

    public int getIdjenismobil() {
        return idjenismobil;
    }

    public void setNama_Pabrikan(String nama_Pabrikan) {
        Nama_Pabrikan = nama_Pabrikan;
    }

    public String getNama_Pabrikan() {
        return Nama_Pabrikan;
    }

    public void setNamajenismobil(String namajenismobil) {
        this.namajenismobil = namajenismobil;
    }

    public String getNamajenismobil() {
        return namajenismobil;
    }
}
