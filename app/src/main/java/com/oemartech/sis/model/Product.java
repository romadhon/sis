package com.oemartech.sis.model;

public class Product {
    int id;
    String name;
    String desc;

    public Product(int id, String name, String desc){
        this.id = id;
        this.name = name;
        this.desc = desc;
    }

    public int getId(){
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDesc() {
        return desc;
    }
}
