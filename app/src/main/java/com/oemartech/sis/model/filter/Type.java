package com.oemartech.sis.model.filter;

import java.io.Serializable;

public class Type implements Serializable {

    int id;
    String namajenis;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setNamajenis(String namajenis) {
        this.namajenis = namajenis;
    }

    public String getNamajenis() {
        return namajenis;
    }
}
