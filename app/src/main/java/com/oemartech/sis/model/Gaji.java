package com.oemartech.sis.model;

public class Gaji {

    private String name;
    private String nominal_gaji;
    private String nominal_komisi;
    private String kategori;
    private String bulan;

    public void setNominal_gaji(String nominal_gaji) {
        this.nominal_gaji = nominal_gaji;
    }

    public String getNominal_gaji() {
        return nominal_gaji;
    }

    public void setNominal_komisi(String nominal_komisi) {
        this.nominal_komisi = nominal_komisi;
    }

    public String getNominal_komisi() {
        return nominal_komisi;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }

    public String getKategori() {
        return kategori;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }


    public void setBulan(String bulan) {
        this.bulan = bulan;
    }

    public String getBulan() {
        return bulan;
    }

}
