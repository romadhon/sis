package com.oemartech.sis.model.filter;

import java.io.Serializable;

public class Showroom implements Serializable {

    int idshowroom;
    String namashowroom;

    public void setIdshowroom(int idshowroom) {
        this.idshowroom = idshowroom;
    }

    public int getIdshowroom() {
        return idshowroom;
    }

    public void setNamashowroom(String namashowroom) {
        this.namashowroom = namashowroom;
    }

    public String getNamashowroom() {
        return namashowroom;
    }
}
