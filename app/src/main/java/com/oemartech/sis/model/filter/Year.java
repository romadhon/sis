package com.oemartech.sis.model.filter;

import java.io.Serializable;

public class Year implements Serializable {
    int tahun;

    public void setTahun(int tahun) {
        this.tahun = tahun;
    }

    public int getTahun() {
        return tahun;
    }
}
