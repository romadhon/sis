package com.oemartech.sis.model;

import java.io.Serializable;

public class Customer implements Serializable {

    private String idcust;
    private String namacust;
    private String telpcust;
    private String alamatcust;
    private String birthday;
    private String keterangan;

    public void setIdcust(String idcust) {
        this.idcust = idcust;
    }

    public String getIdcust() {
        return idcust;
    }

    public void setNamacust(String namacust) {
        this.namacust = namacust;
    }

    public String getNamacust() {
        return namacust;
    }

    public void setAlamatcust(String alamatcust) {
        this.alamatcust = alamatcust;
    }

    public String getAlamatcust() {
        return alamatcust;
    }

    public void setTelpcust(String telpcust) {
        this.telpcust = telpcust;
    }

    public String getTelpcust() {
        return telpcust;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getKeterangan() {
        return keterangan;
    }
}
