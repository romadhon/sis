package com.oemartech.sis.model;

public class Car {
    int id;
    String id_jenismobil;
    String nama_varian;
    String keterangan;

    public Car(int id, String id_jenismobil, String nama_varian, String keterangan){
        this.id = id;
        this.id_jenismobil = id_jenismobil;
        this.nama_varian = nama_varian;
        this.keterangan = keterangan;
    }

    public int getId() {
        return id;
    }

    public String getId_jenismobil() {
        return id_jenismobil;
    }

    public String getNama_varian() {
        return nama_varian;
    }

    public String getKeterangan() {
        return keterangan;
    }
}
