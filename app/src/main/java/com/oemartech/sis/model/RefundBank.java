package com.oemartech.sis.model;

import java.io.Serializable;

public class RefundBank implements Serializable {

    private String iddebeli;
    private String iddatamobil;
    private String idusers;
    private String platnomor;
    private String url;
    private String namacust;
    private String namashowroom;
    private double hargaakhir;

    public void setIddebeli(String iddebeli) {
        this.iddebeli = iddebeli;
    }

    public String getIddebeli() {
        return iddebeli;
    }

    public void setIdusers(String idusers) {
        this.idusers = idusers;
    }

    public String getIdusers() {
        return idusers;
    }

    public void setIddatamobil(String iddatamobil) {
        this.iddatamobil = iddatamobil;
    }

    public String getIddatamobil() {
        return iddatamobil;
    }

    public void setNamashowroom(String namashowroom) {
        this.namashowroom = namashowroom;
    }

    public String getNamashowroom() {
        return namashowroom;
    }

    public void setNamacust(String namacust) {
        this.namacust = namacust;
    }

    public String getNamacust() {
        return namacust;
    }

    public void setPlatnomor(String platnomor) {
        this.platnomor = platnomor;
    }

    public String getPlatnomor() {
        return platnomor;
    }

    public void setHargaakhir(double hargaakhir) {
        this.hargaakhir = hargaakhir;
    }

    public double getHargaakhir() {
        return hargaakhir;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}
