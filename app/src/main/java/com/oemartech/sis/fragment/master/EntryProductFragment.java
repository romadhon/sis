package com.oemartech.sis.fragment.master;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.oemartech.sis.MainActivity;
import com.oemartech.sis.R;
import com.oemartech.sis.resful.SingletonRequestQueue;

import java.util.HashMap;
import java.util.Map;

public class EntryProductFragment extends DialogFragment {
    protected LinearLayoutManager layoutManager;
    private Dialog dg;
    Context context;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        layoutManager = new LinearLayoutManager(getActivity());

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        dg = new Dialog(getActivity(), R.style.FullscreenAppCompatMenuRightTheme);
        dg.setContentView(R.layout.fragment_input_master_product);
        TextView title = (TextView) dg.findViewById(R.id.title);
        title.setText("Input Master Car");

        final EditText etName = (EditText) dg.findViewById(R.id.etName);
        final EditText etDesc = (EditText) dg.findViewById(R.id.etDesc);

        Button simpan = (Button) dg.findViewById(R.id.btn_simpan);
        simpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (etName.getText().toString().isEmpty()){
                    etName.setError("Wajib diisi");
                }else{
                    sendData(etName.getText().toString(),etDesc.getText().toString());
                }
            }
        });

        ImageView back = (ImageView) dg.findViewById(R.id.btn_back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        ImageView add = (ImageView) dg.findViewById(R.id.btn_add);
        add.setVisibility(View.GONE);

        context = dg.getContext();
        return dg;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    private void sendData(final String nama, final String desc) {

        RequestQueue queue = SingletonRequestQueue.getInstance(context).getRequestQueue();

        VolleyLog.DEBUG = true;
        String uri = "http://showroom.theidealstore.id/public/api/masterpabrikan";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                VolleyLog.wtf(response);
                Toast.makeText(getContext(), response, Toast.LENGTH_LONG).show();
                ((MainActivity) getActivity()).refresh("Master Produk");
                dismiss();
            }
        }, errorListener) {
            @Override
            public Priority getPriority() {
                return Priority.LOW;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("Nama_Pabrikan", nama);
                params.put("Keterangan", desc);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
                return headers;
            }

        };
        queue.add(stringRequest);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request request) {
            }
        });
    }

    Response.ErrorListener errorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            if (error instanceof NetworkError) {
                Toast.makeText(context, "No network available", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(context, error.toString(), Toast.LENGTH_LONG).show();
            }
        }
    };

}