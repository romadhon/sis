package com.oemartech.sis.fragment.penggajian;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.oemartech.sis.R;
import com.oemartech.sis.adapter.CarAdapter;
import com.oemartech.sis.model.Car;
import com.oemartech.sis.utils.DateUtils;

import java.util.ArrayList;

/**
 * Created by Sevima on 9/11/2018.
 */

public class GajiPokokFragment extends Fragment {

    View view;
    WebView webView;

    public static GajiPokokFragment newInstance() {
        GajiPokokFragment laporanKomisiMarketingFragment = new GajiPokokFragment();
        Bundle bundle = new Bundle();

        laporanKomisiMarketingFragment.setArguments(bundle);
        return laporanKomisiMarketingFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_gaji_pokok, parent, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        webView = view.findViewById(R.id.webview);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new MyBrowser());
        webView.loadUrl("http://laporan.theidealstore.id/payroll/"+ DateUtils.getMonth());
        setView();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }


    public void setView() {

    }

    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
}
