package com.oemartech.sis.fragment.report;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.DownloadListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.oemartech.sis.R;
import com.oemartech.sis.adapter.CarAdapter;
import com.oemartech.sis.model.Car;

import java.util.ArrayList;

/**
 * Created by Sevima on 9/11/2018.
 */

public class LaporanStockFragment extends Fragment {

    View view;
    WebView webView;
    public static LaporanStockFragment newInstance() {
        LaporanStockFragment laporanKomisiMarketingFragment = new LaporanStockFragment();
        Bundle bundle = new Bundle();

        laporanKomisiMarketingFragment.setArguments(bundle);
        return laporanKomisiMarketingFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_laporan_stock, parent, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        setView();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }


    public void setView() {
        webView = view.findViewById(R.id.webview);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new MyBrowser());
        webView.loadUrl("http://laporan.theidealstore.id/stok");
        webView.setDownloadListener(new DownloadListener() {
            public void onDownloadStart(String url, String userAgent,
                                        String contentDisposition, String mimetype,
                                        long contentLength) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });
    }

    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
}
