package com.oemartech.sis.fragment.penggajian;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.oemartech.sis.R;
import com.oemartech.sis.adapter.GajiAdapter;
import com.oemartech.sis.model.Gaji;
import com.oemartech.sis.model.GlobalVar;
import com.oemartech.sis.model.Komisi;
import com.oemartech.sis.resful.SingletonRequestQueue;
import com.oemartech.sis.utils.Constants;
import com.oemartech.sis.utils.DateUtils;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Sevima on 9/11/2018.
 */

public class GajiFragment extends Fragment {

    View view;
    RecyclerView recyclerView;
    ArrayList<Gaji> gajis = new ArrayList<>();
    GajiAdapter gajiAdapter;

    public static GajiFragment newInstance() {
        GajiFragment laporanKomisiMarketingFragment = new GajiFragment();
        Bundle bundle = new Bundle();

        laporanKomisiMarketingFragment.setArguments(bundle);
        return laporanKomisiMarketingFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_gaji, parent, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        recyclerView = view.findViewById(R.id.rv_gaji);

        setView();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }


    public void setView() {
        getGaji(GlobalVar.getInstance().getId());
    }

    private void setGajiList(){

        gajiAdapter = new GajiAdapter(gajis);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(gajiAdapter);
    }

    private void getGaji(String id_user) {

        RequestQueue queue = SingletonRequestQueue.getInstance(getContext()).getRequestQueue();

        VolleyLog.DEBUG = true;
        String uri = Constants.IP+"gajikaryawan/"+id_user;

        StringRequest stringRequestPOSTJSON = new StringRequest(Request.Method.GET, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Gson gson = new Gson();
                try{

                    Type gajiType = new TypeToken<ArrayList<Gaji>>(){}.getType();
                    gajis = gson.fromJson(response, gajiType);
                }catch (Exception e){

                }

                setGajiList();
            }
        }, errorListener) {
            @Override
            public Request.Priority getPriority() {
                return Request.Priority.HIGH;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", Constants.CONTENT_TYPE);
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                String requestBody = null;


                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        queue.add(stringRequestPOSTJSON);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request request) {
            }
        });
    }

    Response.ErrorListener errorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            if (error instanceof NetworkError) {
                Toast.makeText(getContext(), "No network available", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getContext(), error.toString(), Toast.LENGTH_LONG).show();
            }
        }
    };

}
