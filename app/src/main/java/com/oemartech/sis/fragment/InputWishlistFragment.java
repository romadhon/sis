package com.oemartech.sis.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.oemartech.sis.R;
import com.oemartech.sis.activity.pembelian.InputPembelianActivity;
import com.oemartech.sis.activity.refund.RefundBackDetailActivity;
import com.oemartech.sis.adapter.RefundBankAdapter;
import com.oemartech.sis.model.Customer;
import com.oemartech.sis.model.MasterMobil;
import com.oemartech.sis.model.MasterPabrik;
import com.oemartech.sis.model.RefundBank;
import com.oemartech.sis.resful.SingletonRequestQueue;
import com.oemartech.sis.utils.Constants;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;

public class InputWishlistFragment extends Fragment {
    View view;
    ProgressBar progressBar;
    Spinner sp_customer;
    Spinner sp_produsen;
    Spinner sp_type;
    Button btn_save;
    ArrayList<Customer> customers = new ArrayList<>();
    ArrayList<MasterPabrik> masterPabrikList = new ArrayList<>();
    ArrayList<MasterMobil> masterMobilList = new ArrayList<>();
    String strCustomerId;
    String strIdPabrik;
    String strIdCar;
    public static InputWishlistFragment newInstance() {
        InputWishlistFragment masterTypeCar = new InputWishlistFragment();
        Bundle bundle = new Bundle();

        masterTypeCar.setArguments(bundle);
        return masterTypeCar;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_input_wishlist, parent, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        sp_customer = view.findViewById(R.id.sp_customer);
        sp_produsen = view.findViewById(R.id.sp_produsen);
        sp_type = view.findViewById(R.id.sp_type);
        btn_save = view.findViewById(R.id.btn_save);
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setWishlist();
            }
        });
        progressBar = view.findViewById(R.id.progressBar);
        getCustomer();
        getFactoryRequest();
    }

    private void setCustomers(){

        String[]spCustomers = new String[customers.size()];

        for (int i=0; i<customers.size();i++){
            spCustomers[i] = customers.get(i).getNamacust();
        }

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_dropdown_item, spCustomers);
        sp_customer.setAdapter(adapter);
        sp_customer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                strCustomerId = customers.get(i).getIdcust();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    public void factoryList(){

        List<String> value = new ArrayList<>();
        for (int i = 0; i<masterPabrikList.size();i++){
            value.add(masterPabrikList.get(i).getNama_Pabrikan());
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_dropdown_item, value);
        sp_produsen.setAdapter(adapter);
        sp_produsen.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                strIdPabrik = String.valueOf(masterPabrikList.get(i).getID_Pabrikan());
                getCarsRequest(view.getContext(),strIdPabrik);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public void CarList(){
        List<String> value = new ArrayList<>();
        for (int i = 0; i<masterMobilList.size();i++){
            value.add(masterMobilList.get(i).getNamajenismobil());
        }
        final ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_spinner_dropdown_item, value);
        sp_type.setAdapter(adapter);
        sp_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                strIdCar = String.valueOf(masterMobilList.get(i).getIdjenismobil());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void getCustomer() {
        progressBar.setVisibility(View.VISIBLE);
        RequestQueue queue = SingletonRequestQueue.getInstance(getContext()).getRequestQueue();

        VolleyLog.DEBUG = true;
        String uri = Constants.IP+"customers";

        StringRequest stringRequestPOSTJSON = new StringRequest(Request.Method.GET, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Gson gson = new Gson();
                try{

                    Type customerType = new TypeToken<ArrayList<Customer>>(){}.getType();
                    customers = gson.fromJson(response, customerType);
                }catch (Exception e){

                }

                setCustomers();

            }
        }, errorListener) {
            @Override
            public Priority getPriority() {
                return Priority.HIGH;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", Constants.CONTENT_TYPE);
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                String requestBody = null;


                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        queue.add(stringRequestPOSTJSON);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request request) {
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    public void getFactoryRequest() {
        final Map<Integer, String> map = new IdentityHashMap<Integer, String>();


        RequestQueue queue = SingletonRequestQueue.getInstance(getContext()).getRequestQueue();

        VolleyLog.DEBUG = true;
        String uri = Constants.IP+"masterpabrikan";

        StringRequest stringRequestPOSTJSON = new StringRequest(Request.Method.GET, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Gson gson = new Gson();
                try{

                    Type masterPabrikListType = new TypeToken<ArrayList<MasterPabrik>>(){}.getType();
                    masterPabrikList = gson.fromJson(response, masterPabrikListType);
                }catch (Exception e){

                }

                factoryList();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof NetworkError) {
                    Toast.makeText(getContext(), "No network available", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getContext(), error.toString(), Toast.LENGTH_LONG).show();
                }
            }
        }) {
            @Override
            public Request.Priority getPriority() {
                return Request.Priority.HIGH;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", Constants.CONTENT_TYPE);
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                String requestBody = null;


                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        queue.add(stringRequestPOSTJSON);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request request) {
            }
        });

    }

    public void getCarsRequest(final Context context, String id) {
        final Map<Integer, String> map = new IdentityHashMap<Integer, String>();


        RequestQueue queue = SingletonRequestQueue.getInstance(context).getRequestQueue();

        VolleyLog.DEBUG = true;
        String uri = Constants.IP+"getjenismobilby/"+id;

        StringRequest stringRequestPOSTJSON = new StringRequest(Request.Method.GET, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Gson gson = new Gson();
                try{

                    Type masterMobilListType = new TypeToken<ArrayList<MasterMobil>>(){}.getType();
                    masterMobilList = gson.fromJson(response, masterMobilListType);
                }catch (Exception e){

                }

                CarList();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof NetworkError) {
                    Toast.makeText(context, "No network available", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(context, error.toString(), Toast.LENGTH_LONG).show();
                }
            }
        }) {
            @Override
            public Request.Priority getPriority() {
                return Request.Priority.HIGH;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", Constants.CONTENT_TYPE);
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                String requestBody = null;


                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        queue.add(stringRequestPOSTJSON);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request request) {
            }
        });

    }


    private void setWishlist() {

        RequestQueue queue = SingletonRequestQueue.getInstance(getContext()).getRequestQueue();

        VolleyLog.DEBUG = true;
        String uri = Constants.IP+"insertwishlist";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(getContext(), response.replace("\"",""), Toast.LENGTH_SHORT).show();
                try{

                }catch (Exception e){

                }

            }
        }, errorListener) {
            @Override
            public Priority getPriority() {
                return Priority.LOW;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("idcust", strCustomerId);
                params.put("star", null);
                params.put("iddatamobil", null);
                params.put("id_pabrikan", strIdPabrik);
                params.put("id_jenismobil", strIdCar);

                System.out.println("PARAM : "+params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
                return headers;
            }

        };
        queue.add(stringRequest);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request request) {
                progressBar.setVisibility(View.GONE);
            }
        });
    }


    Response.ErrorListener errorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            if (error instanceof NetworkError) {
                Toast.makeText(getContext(), "No network available", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getContext(), error.toString(), Toast.LENGTH_LONG).show();
            }
        }
    };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }


    public void setView() {

    }
}
