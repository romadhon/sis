package com.oemartech.sis.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.oemartech.sis.MainActivity;
import com.oemartech.sis.R;
import com.oemartech.sis.activity.EmployeeBirthDayActivity;
import com.oemartech.sis.activity.PerhitunganKreditActivity;
import com.oemartech.sis.activity.lain.ProfilActivity;
import com.oemartech.sis.activity.pembelian.AddMemberActivity;
import com.oemartech.sis.activity.pembelian.InputPembelianActivity;
import com.oemartech.sis.activity.penjualan.MobilDijualActivity;
import com.oemartech.sis.helper.CustomDialog;
import com.oemartech.sis.reference.MasterMobilReference;
import com.oemartech.sis.utils.PermissionUtils;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

/**
 * Created by Sevima on 9/11/2018.
 */

public class DashboardFragment extends Fragment {

    View view;


    public static DashboardFragment newInstance() {
        DashboardFragment laporanKomisiMarketingFragment = new DashboardFragment();
        Bundle bundle = new Bundle();

        laporanKomisiMarketingFragment.setArguments(bundle);
        return laporanKomisiMarketingFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_dashboard, parent, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        CardView listmobil_card = (CardView) view.findViewById(R.id.listmobil_card);
        listmobil_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(),MobilDijualActivity.class);
                getActivity().startActivity(intent);
            }
        });

        CardView perhitungankredit_card = (CardView) view.findViewById(R.id.perhitungankredit_card);
        perhitungankredit_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(),PerhitunganKreditActivity.class);
                getActivity().startActivity(intent);
            }
        });

        CardView add_member = (CardView) view.findViewById(R.id.register_card);
        add_member.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (PermissionUtils.permissionMasterCustomerMenu()){
                    Intent intent = new Intent(getContext(),AddMemberActivity.class);
                    getActivity().startActivity(intent);
                } else {
                    CustomDialog.pesan(getActivity(), "You do not have permission");
                }
            }
        });

        CardView purchase_card = (CardView) view.findViewById(R.id.purchase_card);
        purchase_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (PermissionUtils.permissionPurchaseMenu()){
                    MasterMobilReference.getCarsRequest(getContext());
                    Intent intent = new Intent(getContext(),InputPembelianActivity.class);
                    getActivity().startActivity(intent);
                } else {
                    CustomDialog.pesan(getActivity(), "You do not have permission");
                }

            }
        });

        CardView profile_card = (CardView) view.findViewById(R.id.profile_card);
        profile_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getContext(),ProfilActivity.class);
                getActivity().startActivity(intent);
            }
        });

        CardView birthday = (CardView) view.findViewById(R.id.birthday);
        birthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getContext(), EmployeeBirthDayActivity.class);
                getActivity().startActivity(intent);
            }
        });

        CardView logout_card = (CardView) view.findViewById(R.id.logout_card);
        logout_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CustomDialog.keluar(getContext());
            }
        });

        setView();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }


    public void setView() {
        if (checkPermission()){

        }else{
            requestPermission();
        }
    }

    public void onClick(View v){
        Intent i;

        switch (v.getId()) {
            case R.id.perhitungankredit_card: i = new Intent(getContext(),PerhitunganKreditActivity.class); startActivity(i); break;
            default:break;
        }
    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(getContext(), WRITE_EXTERNAL_STORAGE);

        return result == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {

        ActivityCompat.requestPermissions(getActivity(), new String[]{WRITE_EXTERNAL_STORAGE}, 1);

    }

    @Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults) {

        switch (permsRequestCode) {

            case 200:

                boolean galerryAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;

                break;

        }

    }
}
