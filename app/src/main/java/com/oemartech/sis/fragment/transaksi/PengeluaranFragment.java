package com.oemartech.sis.fragment.transaksi;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.oemartech.sis.R;
import com.oemartech.sis.activity.DetailPengeluaranActivity;
import com.oemartech.sis.adapter.PengeluaranAdapter;
import com.oemartech.sis.model.PengeluaranMobil;
import com.oemartech.sis.resful.SingletonRequestQueue;
import com.oemartech.sis.utils.Constants;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Sevima on 9/11/2018.
 */

public class PengeluaranFragment extends Fragment {

    View view;
    RecyclerView rv_refund_bank;
    ProgressBar progressBar;
    PengeluaranAdapter pengeluaranAdapter;
    ArrayList<PengeluaranMobil> pengeluaranMobils = new ArrayList<>();

    public static PengeluaranFragment newInstance() {
        PengeluaranFragment masterProductFragment = new PengeluaranFragment();
        Bundle bundle = new Bundle();
        masterProductFragment.setArguments(bundle);
        return masterProductFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_pengeluaran, parent, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        rv_refund_bank = view.findViewById(R.id.rv_refund_bank);
        progressBar = view.findViewById(R.id.progressBar);
        getPengeluaranMobil();
    }

    private void refundList(){
        pengeluaranAdapter = new PengeluaranAdapter(pengeluaranMobils, getContext(), new PengeluaranAdapter.PengeluaranListener() {
            @Override
            public void onPengeluaranClick (PengeluaranMobil pengeluaranMobil) {
                Intent intent = new Intent(getContext(), DetailPengeluaranActivity.class);
                intent.putExtra("strImg",pengeluaranMobil.getUrl());
                intent.putExtra("strId",pengeluaranMobil.getId_datamobil());
//                intent.putExtra("strIdDatamobil",pengeluaranMobil.getIddatamobil());
//                intent.putExtra("strIdUser",pengeluaranMobil.getId());
                intent.putExtra("strPoliceNumber",pengeluaranMobil.getPlatnomor());
                intent.putExtra("strCarName","");
                intent.putExtra("strPengeluaran", new Gson().toJson(pengeluaranMobil));
//                intent.putExtra("strCustomerName",pengeluaranMobil.getNamacust());
//                intent.putExtra("strMount",pengeluaranMobil.getHargaakhir());
                startActivity(intent);
            }
        });

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        rv_refund_bank.setLayoutManager(layoutManager);
        rv_refund_bank.setAdapter(pengeluaranAdapter);
    }

    private void getPengeluaranMobil() {
        progressBar.setVisibility(View.VISIBLE);
        RequestQueue queue = SingletonRequestQueue.getInstance(getContext()).getRequestQueue();

        VolleyLog.DEBUG = true;
        String uri = Constants.IP+"listpengeluaranmobil";

        StringRequest stringRequestPOSTJSON = new StringRequest(Request.Method.GET, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Gson gson = new Gson();
                try{

                    Type refundType = new TypeToken<ArrayList<PengeluaranMobil>>(){}.getType();
                    pengeluaranMobils = gson.fromJson(response, refundType);
                }catch (Exception e){

                }
                refundList();
            }
        }, errorListener) {
            @Override
            public Priority getPriority() {
                return Priority.HIGH;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", Constants.CONTENT_TYPE);
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                String requestBody = null;


                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        queue.add(stringRequestPOSTJSON);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request request) {
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    Response.ErrorListener errorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {

            if (error instanceof NetworkError) {
                Toast.makeText(getContext(), "No network available", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getContext(), error.toString(), Toast.LENGTH_LONG).show();
            }
        }
    };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

}
