package com.oemartech.sis.fragment;

import android.Manifest;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.gson.JsonObject;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.oemartech.sis.R;
import com.oemartech.sis.activity.lain.ProfilActivity;
import com.oemartech.sis.activity.pembelian.AddMemberActivity;
import com.oemartech.sis.activity.pembelian.InputPembelianActivity;
import com.oemartech.sis.activity.penjualan.MobilDijualActivity;
import com.oemartech.sis.adapter.CarPicturesAdapter;
import com.oemartech.sis.helper.Images;
import com.oemartech.sis.model.GlobalVar;
import com.oemartech.sis.resful.SingletonRequestQueue;
import com.oemartech.sis.resful.VolleyMultipartRequest;
import com.oemartech.sis.utils.Constants;
import com.oemartech.sis.utils.CurrencyUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileDescriptor;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.app.Activity.RESULT_OK;

public class CarDetailFragment extends DialogFragment {
    Dialog dg;
    ImageView imgcar,imgcarEdit,btn_back,btnEdit;
    TextView jmlImage;
    TextView txtname,txtprice,txtpolice,txtyear,txttransmission,txtcondition,txttype,txtcolor, txtbahanbakar;
    EditText etName,etPrice,etPolice,etYear,etTransmission,etCondition,etType,etColor, etBahanbakar;
    Button btnDelete,btnSave;
    ImageView add_image;
    LinearLayout linearoption;
    RecyclerView rvCarPictures;
    ProgressBar progressBar,progressBarImg;

    CarPicturesAdapter carPicturesAdapter;
    Bitmap bFile;
    private RequestQueue rQueue;
    private static final int SELECT_PICTURE = 1;

    double paramHargaBeli;
    int paramidjenismobil;
    int paramidpabrikan;
    int paramidvarian;
    int paramidshowroom;
    String paramexpstnk;
    String parambahanbakar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        dg = new Dialog(getActivity(), R.style.FullscreenAppCompatMenuRightTheme);
        dg.getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        dg.setContentView(R.layout.fragment_detail_car);
//        requestMultiplePermissions();

        rvCarPictures = dg.findViewById(R.id.rvCarPictures);

        imgcar = dg.findViewById(R.id.imgCar);
        imgcarEdit = dg.findViewById(R.id.imgCarEdit);
        jmlImage = dg.findViewById(R.id.jmlImage);
        txtname = dg.findViewById(R.id.txtName);
        txtprice = dg.findViewById(R.id.txtPrice);
        txtpolice = dg.findViewById(R.id.txtPolice);
        txtyear =  dg.findViewById(R.id.txtYear);
        txtcondition =  dg.findViewById(R.id.txtCondition);
        txttransmission =  dg.findViewById(R.id.txtTransmission);
        txttype =  dg.findViewById(R.id.txtType);
        txtcolor =  dg.findViewById(R.id.txtColor);
        txtbahanbakar = dg.findViewById(R.id.txtBahanbakar);

        etName = dg.findViewById(R.id.etName);
        etPrice = dg.findViewById(R.id.etPrice);
        etPolice = dg.findViewById(R.id.etPolice);
        etYear =  dg.findViewById(R.id.etYear);
        etCondition =  dg.findViewById(R.id.etCondition);
        etTransmission =  dg.findViewById(R.id.etTransmission);
        etType =  dg.findViewById(R.id.etType);
        etColor =  dg.findViewById(R.id.etColor);
        etBahanbakar = dg.findViewById(R.id.etBahanbakar);

        linearoption = dg.findViewById(R.id.linearoption);
        btnDelete = dg.findViewById(R.id.btn_delete);
        btnEdit = dg.findViewById(R.id.btn_edit);
        add_image = dg.findViewById(R.id.add_image);
        btnSave = dg.findViewById(R.id.btn_save);
        btnSave.setVisibility(View.GONE);

        progressBar = dg.findViewById(R.id.progressBar);
        progressBarImg = dg.findViewById(R.id.progressBarImg);

        if (getArguments()!=null){
            paramHargaBeli = getArguments().getDouble("buy");
            paramidjenismobil = getArguments().getInt("idjenismobil");
            paramidpabrikan = getArguments().getInt("idpabrikan");
            paramidvarian = getArguments().getInt("idvarian");
            paramidshowroom = getArguments().getInt("idshowroom");
            paramexpstnk = getArguments().getString("expstnk");
            parambahanbakar = getArguments().getString("bahanbakar");
        }
        detail_request(String.valueOf(getArguments().getInt("id")));
        goneEditText();
        visibleTextView();

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteCar();
            }
        });

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnDelete.setVisibility(View.GONE);
                btnSave.setVisibility(View.VISIBLE);
                add_image.setVisibility(View.VISIBLE);

                etName.setText(txtname.getText());
                etPrice.setText(String.format ("%.0f", getArguments().getDouble("price")));
                etPolice.setText(txtpolice.getText());
                etYear.setText(txtyear.getText());
                etCondition.setText(txtcondition.getText());
                etTransmission.setText(txttransmission.getText());
                etType.setText(txttype.getText());
                etColor.setText(txtcolor.getText());
                etBahanbakar.setText(parambahanbakar);

                visibleEditText();
                goneTextView();
            }
        });


        add_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                galleryIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                startActivityForResult(galleryIntent, 1);
            }
        });


        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                add_image.setVisibility(View.GONE);
                imgcarEdit.setVisibility(View.GONE);
                edit_request(getArguments().getInt("id"));
            }
        });

//        if (GlobalVar.getInstance().getRole().equals(Constants.ROLE_ADMIN)){
//            linearoption.setVisibility(View.VISIBLE);
//            btnSave.setVisibility(View.GONE);
//        }


        if (getArguments()!=null){
            Glide.with(dg.getContext())
                    .load(Constants.ASSETS+getArguments().getString("img"))
                    .centerCrop()
                    .placeholder(R.drawable.ic_photo_black_24dp)
                    .into(imgcar);

            txtname.setText(getArguments().getString("name"));
            txtprice.setText(CurrencyUtils.rupiah(getArguments().getDouble("price")));
            txtpolice.setText(getArguments().getString("police"));
            txttype.setText(getArguments().getString("type"));
            txtcolor.setText(getArguments().getString("color"));
            txttransmission.setText(getArguments().getString("transmission"));
            txtyear.setText(getArguments().getString("year"));
            txtcondition.setText(getArguments().getString("condition"));
            txtbahanbakar.setText(parambahanbakar);
        }


        btn_back = dg.findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dg.dismiss();
            }
        });

        return dg;
    }

    public void setRecyclerview(List<String> dataList){
        jmlImage.setVisibility(View.VISIBLE);
        jmlImage.setText(String.valueOf(dataList.size())+" Gambar");
        carPicturesAdapter = new CarPicturesAdapter(dataList, dg.getOwnerActivity());
        LinearLayoutManager layoutManager = new LinearLayoutManager(dg.getContext(), LinearLayoutManager.HORIZONTAL, false);
        rvCarPictures.setLayoutManager(layoutManager);
        rvCarPictures.setAdapter(carPicturesAdapter);
    }

    public void visibleEditText(){
        etName.setVisibility(View.VISIBLE);
        etPrice.setVisibility(View.VISIBLE);
        etPolice.setVisibility(View.VISIBLE);
        etYear.setVisibility(View.VISIBLE);
        etCondition.setVisibility(View.VISIBLE);
        etTransmission.setVisibility(View.VISIBLE);
        etType.setVisibility(View.VISIBLE);
        etColor.setVisibility(View.VISIBLE);
        etBahanbakar.setVisibility(View.VISIBLE);
    }

    public void visibleTextView(){
        txtname.setVisibility(View.VISIBLE);
        txtprice.setVisibility(View.VISIBLE);
        txtpolice.setVisibility(View.VISIBLE);
        txtyear.setVisibility(View.VISIBLE);
        txtcondition.setVisibility(View.VISIBLE);
        txttransmission.setVisibility(View.VISIBLE);
        txttype.setVisibility(View.VISIBLE);
        txtcolor.setVisibility(View.VISIBLE);
        txtbahanbakar.setVisibility(View.VISIBLE);
    }

    public void goneEditText(){
        etName.setVisibility(View.GONE);
        etPrice.setVisibility(View.GONE);
        etPolice.setVisibility(View.GONE);
        etYear.setVisibility(View.GONE);
        etCondition.setVisibility(View.GONE);
        etTransmission.setVisibility(View.GONE);
        etType.setVisibility(View.GONE);
        etColor.setVisibility(View.GONE);
        etBahanbakar.setVisibility(View.GONE);
    }

    public void goneTextView(){
        txtname.setVisibility(View.GONE);
        txtprice.setVisibility(View.GONE);
        txtpolice.setVisibility(View.GONE);
        txtyear.setVisibility(View.GONE);
        txtcondition.setVisibility(View.GONE);
        txttransmission.setVisibility(View.GONE);
        txttype.setVisibility(View.GONE);
        txtcolor.setVisibility(View.GONE);
        txtbahanbakar.setVisibility(View.GONE);
    }

    public void deleteCar(){
        new AlertDialog.Builder(dg.getContext())
                .setMessage("Delete Car ?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        delete_request(String.valueOf(getArguments().getInt("id")));
                    }
                })

                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                })
                .show();
    }

    private void detail_request(final String id) {

        RequestQueue queue = SingletonRequestQueue.getInstance(dg.getContext()).getRequestQueue();
        progressBarImg.setVisibility(View.VISIBLE);
        VolleyLog.DEBUG = true;
        String uriHold = "http://showroom.theidealstore.id/public/api/getmobildijual/"+id;

        final StringRequest unbooking = new StringRequest(Request.Method.GET, uriHold, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                VolleyLog.wtf(response);
                progressBarImg.setVisibility(View.GONE);

                List<String> allImg = new ArrayList<String>();

                try {
                    JSONObject jsonDetailCar = new JSONObject(response);
                    JSONArray cast = jsonDetailCar.getJSONArray("galeri");
                    for (int i=0; i<cast.length(); i++) {

                        JSONObject jsonUrl = new JSONObject(cast.getString(i));

                        allImg.add(jsonUrl.getString("url"));
                    }

                    setRecyclerview(allImg);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, errorListener) {
            @Override
            public Priority getPriority() {
                return Priority.LOW;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();


                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
                return headers;
            }

        };

        queue.add(unbooking);


        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request request) {

                progressBarImg.setVisibility(View.GONE);
            }
        });
    }


    private void delete_request(final String id) {

        RequestQueue queue = SingletonRequestQueue.getInstance(dg.getContext()).getRequestQueue();
        progressBar.setVisibility(View.VISIBLE);
        VolleyLog.DEBUG = true;
        String uriHold = "http://showroom.theidealstore.id/public/api/deletemobildijual/"+id;

        final StringRequest unbooking = new StringRequest(Request.Method.GET, uriHold, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                VolleyLog.wtf(response);
                progressBar.setVisibility(View.GONE);
                Toast.makeText(dg.getContext(), response, Toast.LENGTH_SHORT).show();
                ((MobilDijualActivity) dg.getOwnerActivity()).refreshCar();
                dg.dismiss();
            }
        }, errorListener) {
            @Override
            public Priority getPriority() {
                return Priority.LOW;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();


                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
                return headers;
            }

        };

        queue.add(unbooking);


        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request request) {
                System.out.println("PARAM : "+request);
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    public void edit_request(final int id){
        RequestQueue queue = SingletonRequestQueue.getInstance(dg.getContext()).getRequestQueue();
        progressBar.setVisibility(View.VISIBLE);
        VolleyLog.DEBUG = true;
        String uri = "http://showroom.theidealstore.id/public/api/editmobildijual";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                VolleyLog.wtf(response);
                progressBar.setVisibility(View.GONE);
                dg.dismiss();
            }
        }, errorListener) {
            @Override
            public Priority getPriority() {
                return Priority.LOW;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("id_datamobil", String.valueOf(id));
                params.put("id_pabrikan", String.valueOf(paramidpabrikan));
                params.put("id_jenismobil", String.valueOf(paramidjenismobil));
                params.put("idshowroom", String.valueOf(paramidshowroom));
                params.put("id_varian", String.valueOf(paramidvarian));
                params.put("platnomor", etPolice.getText().toString());
                params.put("warna", etColor.getText().toString());
                params.put("hargabeli", String.format ("%.0f", paramHargaBeli));
                params.put("hargajual", etPrice.getText().toString().replace(",",""));
                params.put("tahun", etYear.getText().toString());
                params.put("expstnk", paramexpstnk);
                params.put("bahanbakar", etBahanbakar.getText().toString());
                params.put("kondisi", etCondition.getText().toString());

                System.out.println("PARAM : "+params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
                return headers;
            }

        };
        queue.add(stringRequest);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request request) {
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    Response.ErrorListener errorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            progressBar.setVisibility(View.GONE);
            if (error instanceof NetworkError) {
                Toast.makeText(dg.getContext(), "No network available", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(dg.getContext(), error.toString(), Toast.LENGTH_LONG).show();
            }
        }
    };

    public String getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    private Bitmap getBitmapFromUri(Uri uri) throws IOException {
        ParcelFileDescriptor parcelFileDescriptor =
                dg.getContext().getContentResolver().openFileDescriptor(uri, "r");
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
        parcelFileDescriptor.close();
        return image;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_PICTURE) {
                Uri selectedImageUri = data.getData();
                try {
                    Toast.makeText(dg.getContext(), "UPLOAD = "+selectedImageUri.toString(), Toast.LENGTH_SHORT).show();
                    imgcar.setVisibility(View.GONE);
                    imgcarEdit.setVisibility(View.VISIBLE);
                    imgcarEdit.setImageBitmap(getBitmapFromUri(selectedImageUri));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

//        try {
//
//            if (requestCode == 1 && resultCode == RESULT_OK && null != data) {
//                Uri imageUri = data.getData();
//                Uri selectedImage = data.getData();
//                String[] filePathColumn = {MediaStore.Images.Media.DATA};
//                Cursor cursor = dg.getContext().getContentResolver().query(selectedImage,
//                        filePathColumn, null, null, null);
//                cursor.moveToFirst();
//                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
//
//                imgDecodableString = cursor.getString(columnIndex);
//
//                cursor.close();
//
//                imgcar.setImageBitmap(getBitmapFromUri(selectedImage));
//                bFile = MediaStore.Images.Media.getBitmap(dg.getContext().getContentResolver(), imageUri);
//
//            } else {
//                System.out.println("Ada sesuatu yang salah");
//            }
//
//        } catch (Exception e) {
//            Toast.makeText(dg.getContext(), "Something went wrong", Toast.LENGTH_LONG)
//                    .show();
//
//        }
    }


    private void upload(){
        String uriUploadPhoto = "http://showroom.theidealstore.id/public/api/uploadmobildijual";
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, uriUploadPhoto,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        Log.d("ressssssoo",new String(response.data));
                        rQueue.getCache().clear();

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(dg.getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                return params;
            }

            /*
             *pass files using below method
             * */
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                long imagename = System.currentTimeMillis();
                params.put("file", new DataPart(imagename + ".png", getFileDataFromDrawable(bFile)));
                return params;
            }
        };


        System.out.println("PARAM : "+volleyMultipartRequest.getPostBodyContentType());
        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        rQueue = Volley.newRequestQueue(dg.getContext());
        rQueue.add(volleyMultipartRequest);
    }

    public byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 80, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    private void  requestMultiplePermissions(){
        Dexter.withActivity(dg.getOwnerActivity())
                .withPermissions(

                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            Toast.makeText(dg.getContext(), "All permissions are granted by user!", Toast.LENGTH_SHORT).show();
                        }

                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings

                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        Toast.makeText(dg.getContext(), "Some Error! ", Toast.LENGTH_SHORT).show();
                    }
                })
                .onSameThread()
                .check();
    }




}
