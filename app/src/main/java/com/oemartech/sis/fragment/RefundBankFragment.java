package com.oemartech.sis.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.oemartech.sis.R;
import com.oemartech.sis.activity.penjualan.MobilDijualActivity;
import com.oemartech.sis.activity.refund.RefundBackDetailActivity;
import com.oemartech.sis.adapter.RefundBankAdapter;
import com.oemartech.sis.model.MasterMobildijual;
import com.oemartech.sis.model.Product;
import com.oemartech.sis.model.RefundBank;
import com.oemartech.sis.resful.SingletonRequestQueue;
import com.oemartech.sis.utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class RefundBankFragment extends Fragment {
    View view;
    RecyclerView rv_refund_bank;
    ProgressBar progressBar;
    RefundBankAdapter refundBankAdapter;
    ArrayList<RefundBank> refundBanks = new ArrayList<>();
    public static RefundBankFragment newInstance() {
        RefundBankFragment masterTypeCar = new RefundBankFragment();
        Bundle bundle = new Bundle();

        masterTypeCar.setArguments(bundle);
        return masterTypeCar;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_refund_bank, parent, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        rv_refund_bank = view.findViewById(R.id.rv_refund_bank);
        progressBar = view.findViewById(R.id.progressBar);
        getRefundBank();
    }

    private void refundList(){
        refundBankAdapter = new RefundBankAdapter(refundBanks, getContext(), new RefundBankAdapter.RefundBankListener() {
            @Override
            public void onRefundClick(RefundBank refundBank) {
                Intent intent = new Intent(getContext(), RefundBackDetailActivity.class);
                intent.putExtra("strImg",refundBank.getUrl());
                intent.putExtra("strId",refundBank.getIddebeli());
                intent.putExtra("strIdUser",refundBank.getIdusers());
                intent.putExtra("strPoliceNumber",refundBank.getPlatnomor());
                intent.putExtra("strCarName","");
                intent.putExtra("strCustomerName",refundBank.getNamacust());
                intent.putExtra("strMount",refundBank.getHargaakhir());
                startActivity(intent);
            }
        });

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        rv_refund_bank.setLayoutManager(layoutManager);
        rv_refund_bank.setAdapter(refundBankAdapter);
    }

    private void getRefundBank() {
        progressBar.setVisibility(View.VISIBLE);
        RequestQueue queue = SingletonRequestQueue.getInstance(getContext()).getRequestQueue();

        VolleyLog.DEBUG = true;
        String uri = Constants.IP+"listpembeliankredit";

        StringRequest stringRequestPOSTJSON = new StringRequest(Request.Method.GET, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Gson gson = new Gson();
                try{

                    Type refundType = new TypeToken<ArrayList<RefundBank>>(){}.getType();
                    refundBanks = gson.fromJson(response, refundType);
                }catch (Exception e){

                }
                refundList();
            }
        }, errorListener) {
            @Override
            public Priority getPriority() {
                return Priority.HIGH;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", Constants.CONTENT_TYPE);
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                String requestBody = null;


                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        queue.add(stringRequestPOSTJSON);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request request) {
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    Response.ErrorListener errorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            if (error instanceof NetworkError) {
                Toast.makeText(getContext(), "No network available", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getContext(), error.toString(), Toast.LENGTH_LONG).show();
            }
        }
    };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }


    public void setView() {

    }
}
