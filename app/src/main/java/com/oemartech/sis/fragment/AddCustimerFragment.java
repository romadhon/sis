package com.oemartech.sis.fragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.oemartech.sis.R;
import com.oemartech.sis.activity.pembelian.AddMemberActivity;
import com.oemartech.sis.activity.penjualan.AddBookingActivity;
import com.oemartech.sis.activity.penjualan.AddHoldActivity;
import com.oemartech.sis.activity.penjualan.AddPurchaseActivity;
import com.oemartech.sis.resful.SingletonRequestQueue;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class AddCustimerFragment extends DialogFragment {
    EditText etnama;
    EditText ettelp;
    EditText etalamat;
    TextView txtDate;
    Dialog dg;
    boolean isCreate;
    String from;
    int id;
    String name,telp,address,date;
    private int mYear, mMonth, mDay;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        dg = new Dialog(getActivity(), R.style.FullscreenAppCompatMenuRightTheme);
        dg.getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        dg.setContentView(R.layout.fragment_add_customer);
        etnama = dg.findViewById(R.id.etNama);
        ettelp = dg.findViewById(R.id.etTelp);
        etalamat = dg.findViewById(R.id.etAlamat);
        txtDate =  dg.findViewById(R.id.btn_date);

        isCreate = getArguments().getBoolean("iscreate");
        from = getArguments().getString("from");

        if (!isCreate){
            id = getArguments().getInt("id");
            name = getArguments().getString("name");
            telp = getArguments().getString("telp");
            address = getArguments().getString("address");
            date = getArguments().getString("date");

            etnama.setText(name);
            ettelp.setText(telp);
            etalamat.setText(address);
            txtDate.setText(date);
        }

        txtDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tanggal_mulai();
            }
        });

        Button btn_save = (Button) dg.findViewById(R.id.btn_simpan);
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (etnama.getText().toString().isEmpty()){
                    Toast.makeText(dg.getOwnerActivity(), "Name is empty", Toast.LENGTH_SHORT).show();
                } else if (ettelp.getText().toString().isEmpty()){
                    Toast.makeText(dg.getOwnerActivity(), "No Hp is empty", Toast.LENGTH_SHORT).show();
                } else if (etalamat.getText().toString().isEmpty()){
                    Toast.makeText(dg.getOwnerActivity(), "Address is empty", Toast.LENGTH_SHORT).show();
                } else if (txtDate.getText().toString().isEmpty()){
                    Toast.makeText(dg.getOwnerActivity(), "Birth is empty", Toast.LENGTH_SHORT).show();
                } else {
                    if (!isCreate){
                        edit_customer_request(id,etnama.getText().toString(),ettelp.getText().toString(),etalamat.getText().toString());
                    } else {
                        add_customer_request(etnama.getText().toString(),ettelp.getText().toString(),etalamat.getText().toString());
                    }
                }

            }
        });

        return dg;
    }

    public void tanggal_mulai(){
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(dg.getOwnerActivity(),
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        txtDate.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    public void add_customer_request(final String nama, final String telp, final String alamat){
        RequestQueue queue = SingletonRequestQueue.getInstance(dg.getContext()).getRequestQueue();

        VolleyLog.DEBUG = true;
        String uri = "http://showroom.theidealstore.id/public/api/insertcustomer";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                VolleyLog.wtf(response);
                Toast.makeText(dg.getContext(), response, Toast.LENGTH_SHORT).show();
                if (from.equals("booking")){
                    ((AddBookingActivity) dg.getOwnerActivity()).refreshCustomerList();
                } else if(from.equals("hold")){
                    ((AddHoldActivity) dg.getOwnerActivity()).refreshCustomerList();
                } else if(from.equals("buy")){
                    ((AddPurchaseActivity) dg.getOwnerActivity()).refreshCustomerList();
                } else if(from.equals("addmember")){
                    ((AddMemberActivity) dg.getOwnerActivity()).refreshCustomer();
                }

                dg.dismiss();
            }
        }, errorListener) {
            @Override
            public Priority getPriority() {
                return Priority.LOW;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                params.put("nama", nama);
                params.put("telp", telp);
                params.put("alamat", alamat);
                params.put("birthday", txtDate.getText().toString());

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
                return headers;
            }

        };
        queue.add(stringRequest);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request request) {
            }
        });
    }

    public void edit_customer_request(final int id, final String nama, final String telp, final String alamat){
        RequestQueue queue = SingletonRequestQueue.getInstance(dg.getContext()).getRequestQueue();

        VolleyLog.DEBUG = true;
        String uri = "http://showroom.theidealstore.id/public/api/editcustomer/"+id;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                VolleyLog.wtf(response);
                Toast.makeText(dg.getContext(), response, Toast.LENGTH_SHORT).show();
                ((AddMemberActivity) dg.getOwnerActivity()).refreshCustomer();
                dg.dismiss();
            }
        }, errorListener) {
            @Override
            public Priority getPriority() {
                return Priority.LOW;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("nama", nama);
                params.put("telp", telp);
                params.put("alamat", alamat);
//                params.put("birthday", txtDate.getText().toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
                return headers;
            }

        };
        queue.add(stringRequest);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request request) {
            }
        });
    }

    Response.ErrorListener errorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            if (error instanceof NetworkError) {
                Toast.makeText(dg.getContext(), "No network available", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(dg.getContext(), error.toString(), Toast.LENGTH_LONG).show();
            }
        }
    };

}
