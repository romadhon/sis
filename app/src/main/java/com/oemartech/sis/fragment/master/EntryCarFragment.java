package com.oemartech.sis.fragment.master;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.oemartech.sis.MainActivity;
import com.oemartech.sis.R;
import com.oemartech.sis.model.Car;
import com.oemartech.sis.model.CarType;
import com.oemartech.sis.model.MasterMobildijual;
import com.oemartech.sis.model.Product;
import com.oemartech.sis.resful.SingletonRequestQueue;
import com.oemartech.sis.utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EntryCarFragment extends DialogFragment {
    protected LinearLayoutManager layoutManager;
    private Dialog dg;
    private Spinner spProduct;
    ArrayList<CarType> carTypes = new ArrayList<>();
    String[] pValue;
    String[] pId;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        layoutManager = new LinearLayoutManager(getActivity());

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        dg = new Dialog(getActivity(), R.style.FullscreenAppCompatMenuRightTheme);
        dg.setContentView(R.layout.fragment_input_master_car);
        TextView title = (TextView) dg.findViewById(R.id.title);
        title.setText("Input Master Car");

        spProduct = (Spinner) dg.findViewById(R.id.spProduct);
        final EditText etName = (EditText) dg.findViewById(R.id.etName);

        ImageView back = (ImageView) dg.findViewById(R.id.btn_back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        ImageView add = (ImageView) dg.findViewById(R.id.btn_add);
        add.setVisibility(View.GONE);

        Button simpan = (Button) dg.findViewById(R.id.btn_simpan);
        simpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (etName.getText().toString().isEmpty()){
                    etName.setError("Please entry varian");
                }else{
                    sendData(pId[spProduct.getSelectedItemPosition()],
                            etName.getText().toString());
                }

            }
        });
        getProducts();
        return dg;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    public void productList(){
        System.out.println("VALUE : "+ pValue.toString());
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_spinner_item, pValue);
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        spProduct.setAdapter(adapter);

    }

    private void getProducts() {

        RequestQueue queue = SingletonRequestQueue.getInstance(getContext()).getRequestQueue();

        VolleyLog.DEBUG = true;
        String uri = Constants.IP+"getjenismobil";

        StringRequest stringRequestPOSTJSON = new StringRequest(Request.Method.GET, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    pId = new String[jsonArray.length()];
                    pValue = new String[jsonArray.length()];

                    for (int i = 0; i< jsonArray.length(); i++){
                        String id_jenis = jsonArray.getJSONObject(i).getString("idjenismobil");
                        String name_product = jsonArray.getJSONObject(i).getString("Nama_Pabrikan");
                        String name_type = jsonArray.getJSONObject(i).getString("namajenismobil");
                        pId[i] = id_jenis;
                        pValue[i] = name_product + " - " + name_type;
                    }
                    productList();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, errorListener) {
            @Override
            public Request.Priority getPriority() {
                return Request.Priority.HIGH;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", Constants.CONTENT_TYPE);
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                String requestBody = null;


                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        queue.add(stringRequestPOSTJSON);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request request) {
            }
        });
    }

    private void sendData(final String id, final String nama) {

        RequestQueue queue = SingletonRequestQueue.getInstance(getContext()).getRequestQueue();

        VolleyLog.DEBUG = true;
        String uri = Constants.IP+"uploadvarianmobil";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                VolleyLog.wtf(response);
                Toast.makeText(getContext(), response, Toast.LENGTH_LONG).show();
                ((MainActivity) getActivity()).refresh(Constants.TITLE_MASTER_MOBIL);
                dismiss();
            }
        }, errorListener) {
            @Override
            public Priority getPriority() {
                return Priority.LOW;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                params.put("id_jenismobil", id);
                params.put("nama_varian", nama);

                System.out.println("PARAM"+ params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
                return headers;
            }

        };
        queue.add(stringRequest);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request request) {
            }
        });
    }

    Response.ErrorListener errorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            if (error instanceof NetworkError) {
                Toast.makeText(getContext(), "No network available", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getContext(), error.toString(), Toast.LENGTH_LONG).show();
            }
        }
    };
}