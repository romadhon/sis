package com.oemartech.sis.fragment.master;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.oemartech.sis.R;
import com.oemartech.sis.adapter.CarAdapter;
import com.oemartech.sis.model.Car;
import com.oemartech.sis.model.Product;
import com.oemartech.sis.resful.SingletonRequestQueue;
import com.oemartech.sis.utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MasterTypeCar extends Fragment {
    View view;
    Button btnSave;
    EditText etType;
    Spinner spProduct;

    ArrayList<Product> products = new ArrayList<>();
    private String[] pValue;
    private String[] pId;

    public static MasterTypeCar newInstance() {
        MasterTypeCar masterTypeCar = new MasterTypeCar();
        Bundle bundle = new Bundle();

        masterTypeCar.setArguments(bundle);
        return masterTypeCar;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_master_car_type, parent, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        spProduct = view.findViewById(R.id.spProduct);
        etType = view.findViewById(R.id.etType);
        btnSave = view.findViewById(R.id.btnSave);

        getProduct();

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validate();
            }
        });
    }


    private void validate(){
        if (etType.getText().toString().isEmpty()){
            Toast.makeText(getContext(), "Please entry type", Toast.LENGTH_SHORT).show();
        } else {
            saveTypeCar();
        }
    }

    private void productList(){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_spinner_item, pValue);
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        spProduct.setAdapter(adapter);
    }


    private void saveTypeCar(){
        RequestQueue queue = SingletonRequestQueue.getInstance(getContext()).getRequestQueue();

        VolleyLog.DEBUG = true;
        String uri = Constants.IP+"insertjenismobil";

        StringRequest stringRequestPOSTJSON = new StringRequest(Request.Method.POST, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Toast.makeText(getContext(), response, Toast.LENGTH_SHORT).show();
            }
        }, errorListener) {
            @Override
            public Request.Priority getPriority() {
                return Request.Priority.HIGH;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                params.put("idpabrikan", pId[spProduct.getSelectedItemPosition()]);
                params.put("nama_mobil", etType.getText().toString());

                System.out.println("PARAM"+ params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", Constants.CONTENT_TYPE);
                return headers;
            }

        };
        queue.add(stringRequestPOSTJSON);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request request) {
            }
        });
    }


    private void getProduct() {

        RequestQueue queue = SingletonRequestQueue.getInstance(getContext()).getRequestQueue();

        VolleyLog.DEBUG = true;
        String uri = Constants.IP+"masterpabrikan";

        StringRequest stringRequestPOSTJSON = new StringRequest(Request.Method.GET, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);

                    pId = new String[jsonArray.length()];
                    pValue = new String[jsonArray.length()];
                    for (int i = 0; i< jsonArray.length(); i++){
                        pId[i] = jsonArray.getJSONObject(i).getString("ID_Pabrikan");
                        pValue[i] = jsonArray.getJSONObject(i).getString("Nama_Pabrikan");
                    }
                    productList();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("RESPONSE : "+response);
            }
        }, errorListener) {
            @Override
            public Request.Priority getPriority() {
                return Request.Priority.HIGH;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", Constants.CONTENT_TYPE);
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                String requestBody = null;


                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        queue.add(stringRequestPOSTJSON);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request request) {
            }
        });
    }

    Response.ErrorListener errorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            if (error instanceof NetworkError) {
                Toast.makeText(getContext(), "No network available", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getContext(), error.toString(), Toast.LENGTH_LONG).show();
            }
        }
    };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }


    public void setView() {

    }
}
