package com.oemartech.sis.fragment.transaksi;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.oemartech.sis.R;
import com.oemartech.sis.activity.penjualan.MobilDijualActivity;
import com.oemartech.sis.adapter.CarAdapter;
import com.oemartech.sis.adapter.PembelianAdapter;
import com.oemartech.sis.fragment.master.EntryCarFragment;
import com.oemartech.sis.model.Car;
import com.oemartech.sis.model.MasterMobildijual;
import com.oemartech.sis.model.Pembelian;
import com.oemartech.sis.resful.SingletonRequestQueue;
import com.oemartech.sis.utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by Sevima on 9/11/2018.
 */

public class PembelianFragment extends Fragment {
    View view;
    ImageView btn_search;
    EditText etKeyword;
    boolean isSearch = false;
    ArrayList<Pembelian> pembelians = new ArrayList<>();
    PembelianAdapter pembelianAdapter;
    private RecyclerView recyclerView;
    ProgressDialog pDialog;
    public static PembelianFragment newInstance() {
        PembelianFragment masterProductFragment = new PembelianFragment();
        Bundle bundle = new Bundle();
//        bundle.putString("keyword", keyword);
//        bundle.putBoolean("autofit", autofit);
        masterProductFragment.setArguments(bundle);
        return masterProductFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_pembelian, parent, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        recyclerView = (RecyclerView) view.findViewById(R.id.rvPembelian);

        etKeyword = view.findViewById(R.id.et_keyword);
        etKeyword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                pembelianAdapter.getFilter().filter(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        btn_search = (ImageView) view.findViewById(R.id.btn_search);
        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isSearch){
                    etKeyword.setVisibility(View.INVISIBLE);
                    isSearch = false;
                }else{
                    etKeyword.setVisibility(View.VISIBLE);
                    etKeyword.setText("");
                    etKeyword.setFocusable(true);
                    isSearch = true;
                }
            }
        });

        getPembelian();
    }

    public void CarList(){

        pembelianAdapter = new PembelianAdapter(pembelians, getContext(), new PembelianAdapter.PembelianAdapterListener() {
            @Override
            public void onDetailClick(Pembelian pembelian) {

            }
        });
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(pembelianAdapter);
    }

    private void getPembelian() {
        pDialog = new ProgressDialog(getContext());
        pDialog.setCancelable(false);
        pDialog.setMessage("Loading ...");
        showDialog();
        RequestQueue queue = SingletonRequestQueue.getInstance(getActivity()).getRequestQueue();

        VolleyLog.DEBUG = true;
        String uri = "http://showroom.theidealstore.id/public/api/mobildijual";

        StringRequest stringRequestPOSTJSON = new StringRequest(Request.Method.GET, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                hideDialog();
                Gson gson = new Gson();
                try{

                    Type mobilDijualListType = new TypeToken<ArrayList<Pembelian>>(){}.getType();
                    pembelians = gson.fromJson(response, mobilDijualListType);
                }catch (Exception e){

                }
                CarList();
            }
        }, errorListener) {
            @Override
            public Request.Priority getPriority() {
                return Request.Priority.HIGH;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", Constants.CONTENT_TYPE);
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                String requestBody = null;
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        queue.add(stringRequestPOSTJSON);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request request) {
                hideDialog();
            }
        });
    }

    Response.ErrorListener errorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            hideDialog();
            if (error instanceof NetworkError) {
                Toast.makeText(getContext(), "No network available", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getContext(), error.toString(), Toast.LENGTH_LONG).show();
            }
        }
    };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }


    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}