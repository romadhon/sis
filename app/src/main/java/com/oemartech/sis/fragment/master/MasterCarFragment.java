package com.oemartech.sis.fragment.master;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.oemartech.sis.R;
import com.oemartech.sis.adapter.CarAdapter;
import com.oemartech.sis.model.Car;
import com.oemartech.sis.resful.SingletonRequestQueue;
import com.oemartech.sis.utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Sevima on 9/11/2018.
 */

public class MasterCarFragment extends Fragment {
    View view;
    ArrayList<Car> cars = new ArrayList<>();
    private RecyclerView recyclerView;
    private CarAdapter carAdapter;

    public static MasterCarFragment newInstance() {
        MasterCarFragment masterProductFragment = new MasterCarFragment();
        Bundle bundle = new Bundle();
//        bundle.putString("keyword", keyword);
//        bundle.putBoolean("autofit", autofit);
        masterProductFragment.setArguments(bundle);
        return masterProductFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_master_car, parent, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        TextView title = (TextView) view.findViewById(R.id.title);
        title.setText(Constants.TITLE_MASTER_MOBIL);

        recyclerView = (RecyclerView) view.findViewById(R.id.rvCar);

        ImageView back = (ImageView) view.findViewById(R.id.btn_back);
        back.setVisibility(View.GONE);

        ImageView add = (ImageView) view.findViewById(R.id.btn_add);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager manager = getFragmentManager();
                Fragment frag = manager.findFragmentByTag("car");
                if (frag != null) {
                    manager.beginTransaction().remove(frag).commit();
                }
                EntryCarFragment entryCarFragment = new EntryCarFragment();
                Bundle bundle = new Bundle();
                bundle.putString("photo", "");
                bundle.putInt("user", 0);
                entryCarFragment.setArguments(bundle);
                entryCarFragment.show(manager, "car");
            }
        });

        getCars();
        setView();
    }

    public void CarList(){

        carAdapter = new CarAdapter(cars);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(carAdapter);
    }

    private void getCars() {

        RequestQueue queue = SingletonRequestQueue.getInstance(getContext()).getRequestQueue();

        VolleyLog.DEBUG = true;
        String uri = "http://showroom.theidealstore.id/public/api/varianmobil";

        StringRequest stringRequestPOSTJSON = new StringRequest(Request.Method.GET, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0; i< jsonArray.length(); i++){
                        int id = jsonArray.getJSONObject(i).getInt("id_varian");
                        String id_jenis = jsonArray.getJSONObject(i).getString("id_jenismobil");
                        String name = jsonArray.getJSONObject(i).getString("nama_varian");
                        String keterangan = jsonArray.getJSONObject(i).getString("keterangan");
                        cars.add(new Car(id, id_jenis, name, keterangan));
                    }
                    CarList();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("RESPONSE : "+response);
            }
        }, errorListener) {
            @Override
            public Request.Priority getPriority() {
                return Request.Priority.HIGH;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", Constants.CONTENT_TYPE);
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                String requestBody = null;


                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        queue.add(stringRequestPOSTJSON);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request request) {
            }
        });
    }

    Response.ErrorListener errorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            if (error instanceof NetworkError) {
                Toast.makeText(getContext(), "No network available", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getContext(), error.toString(), Toast.LENGTH_LONG).show();
            }
        }
    };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }


    public void setView() {

    }
}
