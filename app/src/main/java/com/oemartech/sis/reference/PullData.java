package com.oemartech.sis.reference;

import android.app.Activity;
import android.content.Context;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Map;

/**
 * Created by Sevima on 10/1/2018.
 */

public class PullData implements Serializable {
    private static final long serialVersionUID = 4209785791151665560L;

    private static PullData instance;
    private Long counter;
    private Context context;
    private Activity activity;

    private Map<Integer, String> carsMap;
    private ArrayList<String> arraylist = new ArrayList<String>();


    public void setProfil(String profil) {
        this.arraylist.add(profil);
    }

    public ArrayList<String> getArraylist(){return arraylist;}

    public void setContext(Context context){this.context = context;}
    public Context getContext(){return context;}

    public void setActivity(Activity activity){this.activity = activity;}
    public Activity getActivity(){return activity;}

    static {
        instance = new PullData();
    }

    private PullData() {
    }

    public static PullData getInstance() {
        return PullData.instance;
    }

    public void setCarsMap(Map<Integer, String> carsMap) {
        this.carsMap = carsMap;
    }

    public Map<Integer, String> getCarsMap() {
        return carsMap;
    }
}
