package com.oemartech.sis.reference;

import android.app.Activity;
import android.util.Log;

import com.oemartech.sis.model.MenuModel;
import com.oemartech.sis.utils.Constants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MainMenu {

    public static List<MenuModel> setMenu(Activity activity){
        List<MenuModel> headerList = new ArrayList<>();
        HashMap<MenuModel, List<MenuModel>> childList = new HashMap<>();

        int ic_home = activity.getResources().getIdentifier("ic_home_black_24dp" , "drawable", activity.getPackageName());
        int ic_master = activity.getResources().getIdentifier("ic_master_24dp" , "drawable", activity.getPackageName());
        int ic_setting = activity.getResources().getIdentifier("ic_settings_black_24dp" , "drawable", activity.getPackageName());

        MenuModel menuModel = new MenuModel("Dashboard", true, false, "", ic_home); //Menu of Android Tutorial. No sub menus
        headerList.add(menuModel);

        if (!menuModel.hasChildren) {
            childList.put(menuModel, null);
        }

        menuModel = new MenuModel("Master", true, true,"", 1); //Menu of Java Tutorials
        headerList.add(menuModel);
        List<MenuModel> childModelsList = new ArrayList<>();
        MenuModel childModel = new MenuModel(Constants.TITLE_MASTER_PRODUK, false, false,"", 1);
        childModelsList.add(childModel);

        childModel = new MenuModel(Constants.TITLE_MASTER_TIPE_MOBIL, false, false,"", ic_master);
        childModelsList.add(childModel);

        childModel = new MenuModel(Constants.TITLE_MASTER_MOBIL, false, false,"", ic_master);
        childModelsList.add(childModel);


        if (menuModel.hasChildren) {
            Log.d("API123","here");
            childList.put(menuModel, childModelsList);
        }

        childModelsList = new ArrayList<>();
        menuModel = new MenuModel("Salary", true, true,"", ic_setting); //Menu of Python Tutorials
        headerList.add(menuModel);
        childModel = new MenuModel(Constants.TITLE_GAJI_POKOK, false, false, "",1);
        childModelsList.add(childModel);
        childModel = new MenuModel(Constants.TITLE_GENERATE_KOMISI, false, false, "",1);
        childModelsList.add(childModel);

        if (menuModel.hasChildren) {
            Log.d("API123","here");
            childList.put(menuModel, childModelsList);
        }

        childModelsList = new ArrayList<>();
        menuModel = new MenuModel("Transaction", true, true,"", ic_setting); //Menu of Python Tutorials
        headerList.add(menuModel);
        childModel = new MenuModel(Constants.TITLE_INPUT_WISHLIST_CUSTOMER, false, false, "",1);
        childModelsList.add(childModel);
        childModel = new MenuModel(Constants.PENJUALAN, false, false, "",1);
        childModelsList.add(childModel);
        childModel = new MenuModel(Constants.PEMBELIAN, false, false, "",1);
        childModelsList.add(childModel);
        childModel = new MenuModel(Constants.PENGELUARAN, false, false, "",1);
        childModelsList.add(childModel);
        childModel = new MenuModel(Constants.TITLE_REFUND_BANK, false, false, "",1);
        childModelsList.add(childModel);
        if (menuModel.hasChildren) {
            Log.d("API123","here");
            childList.put(menuModel, childModelsList);
        }

        childModelsList = new ArrayList<>();
        menuModel = new MenuModel("Report", true, true,"", ic_setting); //Menu of Python Tutorials
        headerList.add(menuModel);
        childModel = new MenuModel(Constants.TITLE_LAPORAN_PENJUALAN, false, false, "",1);
        childModelsList.add(childModel);
        childModel = new MenuModel(Constants.TITLE_LAPORAN_PEMBELIAN, false, false, "",1);
        childModelsList.add(childModel);
        childModel = new MenuModel(Constants.TITLE_LAPORAN_LABA_RUGI, false, false, "",1);
        childModelsList.add(childModel);
        childModel = new MenuModel(Constants.TITLE_LAPORAN_MARKETING, false, false, "",1);
        childModelsList.add(childModel);
        childModel = new MenuModel(Constants.TITLE_LAPORAN_KREDIT, false, false, "",1);
        childModelsList.add(childModel);
        childModel = new MenuModel(Constants.TITLE_LAPORAN_SHOWROOM, false, false, "",1);
        childModelsList.add(childModel);
        if (menuModel.hasChildren) {
            Log.d("API123","here");
            childList.put(menuModel, childModelsList);
        }

        childModelsList = new ArrayList<>();
        menuModel = new MenuModel("Admin", true, true,"", ic_setting); //Menu of Python Tutorials
        headerList.add(menuModel);
        childModel = new MenuModel(Constants.ADD_USER, false, false, "",1);
        childModelsList.add(childModel);
        if (menuModel.hasChildren) {
            childList.put(menuModel, childModelsList);
        }

        menuModel = new MenuModel("Kirim Notifikasi", false, false, "", ic_home); //Menu of Android Tutorial. No sub menus
        headerList.add(menuModel);

        if (!menuModel.hasChildren) {
            childList.put(menuModel, null);
        }

        return headerList;
    }

    public static HashMap<MenuModel, List<MenuModel>> setSubMenu(Activity activity){
        List<MenuModel> headerList = new ArrayList<>();
        HashMap<MenuModel, List<MenuModel>> childList = new HashMap<>();

        int ic_home = activity.getResources().getIdentifier("ic_home_black_24dp" , "drawable", activity.getPackageName());
        int ic_master = activity.getResources().getIdentifier("ic_master_24dp" , "drawable", activity.getPackageName());
        int ic_setting = activity.getResources().getIdentifier("ic_settings_black_24dp" , "drawable", activity.getPackageName());

        MenuModel menuModel = new MenuModel("Dashboard", true, false, "", ic_home); //Menu of Android Tutorial. No sub menus
        headerList.add(menuModel);

        if (!menuModel.hasChildren) {
            childList.put(menuModel, null);
        }

        menuModel = new MenuModel("Master", true, true,"", 1); //Menu of Java Tutorials
        headerList.add(menuModel);
        List<MenuModel> childModelsList = new ArrayList<>();
        MenuModel childModel = new MenuModel(Constants.TITLE_MASTER_PRODUK, false, false,"", 1);
        childModelsList.add(childModel);

        childModel = new MenuModel(Constants.TITLE_MASTER_TIPE_MOBIL, false, false,"", ic_master);
        childModelsList.add(childModel);

        childModel = new MenuModel(Constants.TITLE_MASTER_MOBIL, false, false,"", ic_master);
        childModelsList.add(childModel);


        if (menuModel.hasChildren) {
            Log.d("API123","here");
            childList.put(menuModel, childModelsList);
        }

        childModelsList = new ArrayList<>();
        menuModel = new MenuModel("Salary", true, true,"", ic_setting); //Menu of Python Tutorials
        headerList.add(menuModel);
        childModel = new MenuModel(Constants.TITLE_GAJI_POKOK, false, false, "",1);
        childModelsList.add(childModel);
        childModel = new MenuModel(Constants.TITLE_GENERATE_KOMISI, false, false, "",1);
        childModelsList.add(childModel);

        if (menuModel.hasChildren) {
            Log.d("API123","here");
            childList.put(menuModel, childModelsList);
        }

        childModelsList = new ArrayList<>();
        menuModel = new MenuModel("Transaction", true, true,"", ic_setting); //Menu of Python Tutorials
        headerList.add(menuModel);
        childModel = new MenuModel(Constants.TITLE_INPUT_WISHLIST_CUSTOMER, false, false, "",1);
        childModelsList.add(childModel);
        childModel = new MenuModel(Constants.PENJUALAN, false, false, "",1);
        childModelsList.add(childModel);
        childModel = new MenuModel(Constants.PEMBELIAN, false, false, "",1);
        childModelsList.add(childModel);
        childModel = new MenuModel(Constants.PENGELUARAN, false, false, "",1);
        childModelsList.add(childModel);
        childModel = new MenuModel(Constants.TITLE_REFUND_BANK, false, false, "",1);
        childModelsList.add(childModel);
        if (menuModel.hasChildren) {
            Log.d("API123","here");
            childList.put(menuModel, childModelsList);
        }

        childModelsList = new ArrayList<>();
        menuModel = new MenuModel("Report", true, true,"", ic_setting); //Menu of Python Tutorials
        headerList.add(menuModel);
        childModel = new MenuModel(Constants.TITLE_LAPORAN_PENJUALAN, false, false, "",1);
        childModelsList.add(childModel);
        childModel = new MenuModel(Constants.TITLE_LAPORAN_PEMBELIAN, false, false, "",1);
        childModelsList.add(childModel);
        childModel = new MenuModel(Constants.TITLE_LAPORAN_LABA_RUGI, false, false, "",1);
        childModelsList.add(childModel);
        childModel = new MenuModel(Constants.TITLE_LAPORAN_MARKETING, false, false, "",1);
        childModelsList.add(childModel);
        childModel = new MenuModel(Constants.TITLE_LAPORAN_KREDIT, false, false, "",1);
        childModelsList.add(childModel);
        childModel = new MenuModel(Constants.TITLE_LAPORAN_SHOWROOM, false, false, "",1);
        childModelsList.add(childModel);
        if (menuModel.hasChildren) {
            Log.d("API123","here");
            childList.put(menuModel, childModelsList);
        }

        childModelsList = new ArrayList<>();
        menuModel = new MenuModel("Admin", true, true,"", ic_setting); //Menu of Python Tutorials
        headerList.add(menuModel);
        childModel = new MenuModel(Constants.ADD_USER, false, false, "",1);
        childModelsList.add(childModel);
        if (menuModel.hasChildren) {
            childList.put(menuModel, childModelsList);
        }

        menuModel = new MenuModel("Kirim Notifikasi", false, false, "", ic_home); //Menu of Android Tutorial. No sub menus
        headerList.add(menuModel);

        if (!menuModel.hasChildren) {
            childList.put(menuModel, null);
        }

        return childList;
    }
}
