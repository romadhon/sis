package com.oemartech.sis.reference;

import android.content.Context;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.oemartech.sis.resful.SingletonRequestQueue;
import com.oemartech.sis.utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.UnsupportedEncodingException;
import java.util.Comparator;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by Sevima on 10/1/2018.
 */

public class MasterMobilReference {
    static Map<Integer, String> treeMap;
    static Context context;


    public static void getCarsRequest(final Context context) {
        final Map<Integer, String> map = new IdentityHashMap<Integer, String>();


        RequestQueue queue = SingletonRequestQueue.getInstance(context).getRequestQueue();

        VolleyLog.DEBUG = true;
        String uri = "http://showroom.theidealstore.id/public/api/varianmobil";

        StringRequest stringRequestPOSTJSON = new StringRequest(Request.Method.GET, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                System.out.println("RESPONSE CAR : "+response);
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0; i< jsonArray.length(); i++){
                        int id = jsonArray.getJSONObject(i).getInt("id_varian");
                        String name = jsonArray.getJSONObject(i).getString("nama_mobil");
                        String type = jsonArray.getJSONObject(i).getString("tipe");
                        String transmission = jsonArray.getJSONObject(i).getString("transmisi");
                        String year = jsonArray.getJSONObject(i).getString("tahun_pembuatan");
                        map.put(id, name+" "+type+" "+transmission+" "+year);
                    }
                   treeMap = new TreeMap<Integer, String>(
                            new Comparator<Integer>() {

                                @Override
                                public int compare(Integer o1, Integer o2) {
                                    return o1.compareTo(o2);
                                }

                            });

                    treeMap.putAll(map);
                    PullData.getInstance().setCarsMap(treeMap);
                    printMap(treeMap);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof NetworkError) {
                    Toast.makeText(context, "No network available", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(context, error.toString(), Toast.LENGTH_LONG).show();
                }
            }
        }) {
            @Override
            public Request.Priority getPriority() {
                return Request.Priority.HIGH;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", Constants.CONTENT_TYPE);
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                String requestBody = null;


                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        queue.add(stringRequestPOSTJSON);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request request) {
            }
        });

    }

    public static <K, V> void printMap(Map<K, V> map) {
        for (Map.Entry<K, V> entry : map.entrySet()) {
            System.out.println("Key : " + entry.getKey()
                    + " Value : " + entry.getValue());
        }
    }
}
