package com.oemartech.sis.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.oemartech.sis.R;
import com.oemartech.sis.model.MenuModel;

import java.util.HashMap;
import java.util.List;

public class NavigationDrawerAdapter extends BaseExpandableListAdapter {
    private Context context;
    private List<MenuModel> listDataHeader;
    private HashMap<MenuModel, List<MenuModel>> listDataChild;

    public NavigationDrawerAdapter(Context context, List<MenuModel> listDataHeader,
                                 HashMap<MenuModel, List<MenuModel>> listChildData) {
        this.context = context;
        this.listDataHeader = listDataHeader;
        this.listDataChild = listChildData;
    }

    @Override
    public MenuModel getChild(int groupPosition, int childPosititon) {
        return this.listDataChild.get(this.listDataHeader.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final String childText = getChild(groupPosition, childPosition).menuName;

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_group_child, null);
        }

        TextView txtListChild = convertView
                .findViewById(R.id.lblListItem);

        txtListChild.setText(childText);
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {

        if (this.listDataChild.get(this.listDataHeader.get(groupPosition)) == null)
            return 0;
        else
            return this.listDataChild.get(this.listDataHeader.get(groupPosition))
                    .size();
    }

    @Override
    public MenuModel getGroup(int groupPosition) {
        return this.listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this.listDataHeader.size();

    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle = getGroup(groupPosition).menuName;
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_group_header, null);
        }

        TextView lblListHeader = convertView.findViewById(R.id.lblListHeader);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);

        ImageView ic = convertView.findViewById(R.id.ic_menu);
        if (getGroup(groupPosition).menuName.startsWith("D")){
            ic.setImageResource(R.drawable.ic_home_black_24dp);
        }else if (getGroup(groupPosition).menuName.startsWith("M")){
            ic.setImageResource(R.drawable.ic_master_24dp);
        }else if (getGroup(groupPosition).menuName.startsWith("S")){
            ic.setImageResource(R.drawable.ic_credit_card_black_24dp);
        }else if (getGroup(groupPosition).menuName.startsWith("T")){
            ic.setImageResource(R.drawable.ic_transaction_black_24dp);
        }else if (getGroup(groupPosition).menuName.startsWith("I")){
            ic.setImageResource(R.drawable.ic_assignment_black_24dp);
        }else if (getGroup(groupPosition).menuName.startsWith("R")){
            ic.setImageResource(R.drawable.ic_report_black_24dp);
        }else if (getGroup(groupPosition).menuName.startsWith("A")){
            ic.setImageResource(R.drawable.ic_admin_black_24dp);
        }else if (getGroup(groupPosition).menuName.startsWith("K")){
            ic.setImageResource(R.drawable.ic_notifications_active_black_24dp);
        }

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}