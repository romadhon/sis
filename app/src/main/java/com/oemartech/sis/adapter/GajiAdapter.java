package com.oemartech.sis.adapter;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oemartech.sis.R;
import com.oemartech.sis.model.Car;
import com.oemartech.sis.model.Gaji;
import com.oemartech.sis.utils.CurrencyUtils;

import java.util.ArrayList;

public class GajiAdapter extends RecyclerView.Adapter<GajiAdapter.GajiViewHolder> {


    private ArrayList<Gaji> dataList;

    public GajiAdapter(ArrayList<Gaji> dataList) {
        this.dataList = dataList;
    }

    @Override
    public GajiViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_gaji, parent, false);
        return new GajiViewHolder(view);
    }

    @Override
    public void onBindViewHolder(GajiViewHolder holder, int position) {
        double gaji;
        double komisi;
        if (dataList.get(position)!=null){
            if (dataList.get(position).getNominal_gaji()!=null)
                 gaji = Double.parseDouble(dataList.get(position).getNominal_gaji());
            else
                gaji = 0;

            if (dataList.get(position).getNominal_komisi()!=null)
                komisi = Double.parseDouble(dataList.get(position).getNominal_komisi());
            else
                komisi= 0;

            holder.tv_bulan.setText(dataList.get(position).getBulan());
            holder.tv_gaji.setText(CurrencyUtils.rupiah(gaji));
            holder.tv_komisi.setText(CurrencyUtils.rupiah(komisi));
            holder.tv_total.setText(CurrencyUtils.rupiah(gaji+komisi));
        }




    }

    @Override
    public int getItemCount() {
        return (dataList != null) ? dataList.size() : 0;
    }

    public class GajiViewHolder extends RecyclerView.ViewHolder{
        private TextView tv_bulan, tv_gaji, tv_komisi, tv_total;

        public GajiViewHolder(View itemView) {
            super(itemView);
            tv_bulan = itemView.findViewById(R.id.tv_bulan);
            tv_gaji = itemView.findViewById(R.id.tv_gaji);
            tv_komisi = itemView.findViewById(R.id.tv_komisi);
            tv_total = itemView.findViewById(R.id.tv_total);
        }
    }
}