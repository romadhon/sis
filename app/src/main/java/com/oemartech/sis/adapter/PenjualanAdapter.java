package com.oemartech.sis.adapter;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.oemartech.sis.R;
import com.oemartech.sis.model.Car;
import com.oemartech.sis.model.MobilDijual;
import com.oemartech.sis.model.Penjualan;
import com.oemartech.sis.utils.CurrencyUtils;

import java.util.ArrayList;
import java.util.List;

public class PenjualanAdapter extends RecyclerView.Adapter<PenjualanAdapter.PenjualanViewHolder> implements Filterable {


    private ArrayList<Penjualan> dataList;
    private List<Penjualan> dataListFiltered;
    PenjualanAdapterListener mListener;

    public PenjualanAdapter(ArrayList<Penjualan> dataList, PenjualanAdapterListener mListener) {
        this.dataList = dataList;
        this.dataListFiltered = dataList;
        this.mListener = mListener;
    }

    @Override
    public PenjualanViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_penjualan, parent, false);
        return new PenjualanViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PenjualanViewHolder holder, final int position) {
        Penjualan penjualan = dataListFiltered.get(position);
        holder.txtNama.setText(penjualan.getNama_Pabrikan()+" "
                +penjualan.getNamajenismobil()+" "+penjualan.getNama_varian());
        holder.txtPrice.setText(CurrencyUtils.rupiah(penjualan.getHargabeli()));
        holder.btnMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onMoreClick(dataListFiltered.get(position));
            }
        });

    }

    @Override
    public int getItemCount() {
        return dataListFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    dataListFiltered = dataList;
                } else {
                    List<Penjualan> filteredList = new ArrayList<>();
                    for (Penjualan row : dataList) {
                        if (row.getTahun().toLowerCase().contains(charString.toLowerCase()) ||
                                row.getNama_Pabrikan().toLowerCase().contains(charString.toLowerCase()) ||
                                row.getNama_varian().toLowerCase().contains(charString.toLowerCase()) ||
                                row.getNamajenismobil().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    dataListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = dataListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                dataListFiltered = (ArrayList<Penjualan>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public class PenjualanViewHolder extends RecyclerView.ViewHolder{
        private TextView txtNama, txtPrice;
        private ImageView btnMore;

        public PenjualanViewHolder(View itemView) {
            super(itemView);
            txtNama = (TextView) itemView.findViewById(R.id.tvName);
            txtPrice = (TextView) itemView.findViewById(R.id.tvPrice);
            btnMore = itemView.findViewById(R.id.btn_more);
        }
    }

    public interface PenjualanAdapterListener {
        void onMoreClick(Penjualan penjualan);
    }
}