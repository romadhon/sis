package com.oemartech.sis.adapter.filter;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oemartech.sis.R;
import com.oemartech.sis.model.Car;
import com.oemartech.sis.model.filter.Showroom;
import com.oemartech.sis.model.filter.Year;

import java.util.ArrayList;

public class YearAdapter extends RecyclerView.Adapter<YearAdapter.YearViewHolder> {


    private ArrayList<Year> dataList;
    protected YearListener yearListener;

    public YearAdapter(ArrayList<Year> dataList, YearListener yearListener) {
        this.dataList = dataList;
        this.yearListener = yearListener;
    }

    @Override
    public YearViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_filter, parent, false);
        return new YearViewHolder(view);
    }

    @Override
    public void onBindViewHolder(YearViewHolder h, int position) {
        final Year year = dataList.get(position);
        h.txtName.setText(String.valueOf(year.getTahun()));
        h.txtName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                yearListener.onBodyClick(year);
            }
        });
    }

    @Override
    public int getItemCount() {
        return (dataList != null) ? dataList.size() : 0;
    }

    public class YearViewHolder extends RecyclerView.ViewHolder{
        private TextView txtName;

        public YearViewHolder(View itemView) {
            super(itemView);
            txtName = itemView.findViewById(R.id.tv_name);
        }
    }

    public interface YearListener {
        void onBodyClick(Year year);
    }
}