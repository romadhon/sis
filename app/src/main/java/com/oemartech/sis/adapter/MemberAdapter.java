package com.oemartech.sis.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.oemartech.sis.R;
import com.oemartech.sis.model.Member;

import java.util.ArrayList;

public class MemberAdapter extends RecyclerView.Adapter<MemberAdapter.MemberViewHolder> {

    protected MemberAdapter.MemberAdapterListener mListener;
    private ArrayList<Member> dataList;
    Context context;
    public MemberAdapter(ArrayList<Member> dataList,Context context,MemberAdapter.MemberAdapterListener mListener) {
        this.dataList = dataList;
        this.context = context;
        this.mListener = mListener;
    }

    @Override
    public MemberViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_member, parent, false);
        return new MemberViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MemberViewHolder holder, final int position) {
        holder.txtNama.setText(dataList.get(position).getNama());
        holder.txtTelp.setText(dataList.get(position).getTelp());
        holder.txtAlamat.setText(dataList.get(position).getAlamat());

        holder.btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onEditClick(dataList.get(position).getId(),dataList.get(position).getNama(),dataList.get(position).getTelp(),dataList.get(position).getAlamat());
            }
        });

        holder.btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onDeleteClick(dataList.get(position).getId());
            }
        });
    }

    @Override
    public int getItemCount() {
        System.out.println("DATA LIST : "+ dataList.size());
        return (dataList != null) ? dataList.size() : 0;
    }

    public class MemberViewHolder extends RecyclerView.ViewHolder{
        private TextView txtNama, txtTelp, txtAlamat;
        private Button btn_edit, btn_delete;

        public MemberViewHolder(View itemView) {
            super(itemView);
            txtNama = (TextView) itemView.findViewById(R.id.tvNama);
            txtTelp = (TextView) itemView.findViewById(R.id.tvTelp);
            txtAlamat = (TextView) itemView.findViewById(R.id.tvAlamat);
            btn_delete =  itemView.findViewById(R.id.btn_delete);
            btn_edit =  itemView.findViewById(R.id.btn_edit);
        }
    }

    public interface MemberAdapterListener {
        void onDeleteClick(int id);
        void onEditClick(int id, String nama, String telp, String alamat);
    }
}