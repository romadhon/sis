package com.oemartech.sis.adapter;


import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.oemartech.sis.R;
import com.oemartech.sis.model.MasterMobildijual;
import com.oemartech.sis.model.Pembelian;
import com.oemartech.sis.utils.Constants;
import com.oemartech.sis.utils.CurrencyUtils;

import java.util.ArrayList;
import java.util.List;

public class PembelianAdapter extends RecyclerView.Adapter<PembelianAdapter.PembelianHolder> implements Filterable{

    protected PembelianAdapterListener mListener;
    private ArrayList<Pembelian> dataList;
    private List<Pembelian> dataListFiltered;
    private Context context;

    public PembelianAdapter(ArrayList<Pembelian> dataList, Context context, PembelianAdapterListener mListener) {
        this.dataList = dataList;
        this.dataListFiltered = dataList;
        this.context = context;
        this.mListener = mListener;
    }

    @Override
    public PembelianHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_pembelian, parent, false);
        return new PembelianHolder(view);
    }

    @Override
    public void onBindViewHolder(PembelianHolder holder, final int position) {
        Pembelian pembelian = dataListFiltered.get(position);
        holder.tvName.setText(pembelian.getNama_Pabrikan()+" "
                +pembelian.getNamajenismobil()+" "+pembelian.getNama_varian());
        holder.tvPrice.setText(CurrencyUtils.rupiah(pembelian.getHargabeli()));
    }

    @Override
    public int getItemCount() {
        return (dataListFiltered != null) ? dataListFiltered.size() : 0;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    dataListFiltered = dataList;
                } else {
                    List<Pembelian> filteredList = new ArrayList<>();
                    for (Pembelian row : dataList) {

                        if (row.getTahun().toLowerCase().contains(charString.toLowerCase()) ||
                                row.getNama_Pabrikan().toLowerCase().contains(charString.toLowerCase()) ||
                                row.getNama_varian().toLowerCase().contains(charString.toLowerCase()) ||
                                row.getNamajenismobil().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    dataListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = dataListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                dataListFiltered = (ArrayList<Pembelian>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public class PembelianHolder extends RecyclerView.ViewHolder{
        private TextView tvName, tvPrice;

        public PembelianHolder(View itemView) {
            super(itemView);

            tvName = (TextView) itemView.findViewById(R.id.tvName);
            tvPrice = (TextView) itemView.findViewById(R.id.tvPrice);
        }
    }

    public interface PembelianAdapterListener {
        void onDetailClick(Pembelian pembelian);
    }

}