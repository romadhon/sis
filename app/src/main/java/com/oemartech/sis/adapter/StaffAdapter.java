package com.oemartech.sis.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.oemartech.sis.R;
import com.oemartech.sis.model.Member;
import com.oemartech.sis.model.Staff;
import com.oemartech.sis.utils.Constants;

import java.util.ArrayList;

public class StaffAdapter extends RecyclerView.Adapter<StaffAdapter.MemberViewHolder> {

    protected StaffAdapter.StaffAdapterListener mListener;
    private ArrayList<Staff> dataList;
    Context context;
    public StaffAdapter(ArrayList<Staff> dataList, Context context, StaffAdapter.StaffAdapterListener mListener) {
        this.dataList = dataList;
        this.context = context;
        this.mListener = mListener;
    }

    @Override
    public MemberViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_staff, parent, false);
        return new MemberViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MemberViewHolder holder, final int position) {
        holder.txtNama.setText(dataList.get(position).getNama());
        holder.txtPassword.setText(dataList.get(position).getPassword());

//        if (dataList.get(position).getRole().equals(Constants.ROLE_OPERATOR)){
//            holder.txtRole.setText("Admin");
//        }

        holder.btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onEditClick(dataList.get(position).getId(),dataList.get(position).getNama(),dataList.get(position).getPassword(),dataList.get(position).getRole());
            }
        });

        holder.btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onDeleteClick(dataList.get(position).getId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return (dataList != null) ? dataList.size() : 0;
    }

    public class MemberViewHolder extends RecyclerView.ViewHolder{
        private TextView txtNama, txtPassword, txtRole;
        private ImageView btn_edit, btn_delete;

        public MemberViewHolder(View itemView) {
            super(itemView);
            txtNama = (TextView) itemView.findViewById(R.id.tvNama);
            txtPassword = (TextView) itemView.findViewById(R.id.tvPassword);
            txtPassword.setVisibility(View.GONE);
            txtRole = (TextView) itemView.findViewById(R.id.tvRole);
            btn_delete = (ImageView) itemView.findViewById(R.id.btn_delete);
            btn_edit = (ImageView) itemView.findViewById(R.id.btn_edit);
        }
    }

    public interface StaffAdapterListener {
        void onDeleteClick(int id);
        void onEditClick(int id, String nama, String password, String role);
    }
}