package com.oemartech.sis.adapter.filter;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oemartech.sis.R;
import com.oemartech.sis.model.Car;
import com.oemartech.sis.model.filter.Showroom;

import java.util.ArrayList;

public class ShowroomAdapter extends RecyclerView.Adapter<ShowroomAdapter.ShowroomViewHolder> {


    private ArrayList<Showroom> dataList;
    protected ShowroomListener showroomListener;

    public ShowroomAdapter(ArrayList<Showroom> dataList, ShowroomListener showroomListener) {
        this.dataList = dataList;
        this.showroomListener = showroomListener;
    }

    @Override
    public ShowroomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_filter, parent, false);
        return new ShowroomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ShowroomViewHolder h, int position) {
        final Showroom showroom = dataList.get(position);
        h.txtName.setText(showroom.getNamashowroom());
        h.txtName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showroomListener.onBodyClick(showroom);
            }
        });
    }

    @Override
    public int getItemCount() {
        return (dataList != null) ? dataList.size() : 0;
    }

    public class ShowroomViewHolder extends RecyclerView.ViewHolder{
        private TextView txtName;

        public ShowroomViewHolder(View itemView) {
            super(itemView);
            txtName = itemView.findViewById(R.id.tv_name);
        }
    }

    public interface ShowroomListener {
        void onBodyClick(Showroom showroom);
    }
}