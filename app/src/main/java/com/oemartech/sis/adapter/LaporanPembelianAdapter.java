package com.oemartech.sis.adapter;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oemartech.sis.R;
import com.oemartech.sis.model.Car;

import java.util.ArrayList;

public class LaporanPembelianAdapter extends RecyclerView.Adapter<LaporanPembelianAdapter.MahasiswaViewHolder> {


    private ArrayList<Car> dataList;

    public LaporanPembelianAdapter(ArrayList<Car> dataList) {
        this.dataList = dataList;
    }

    @Override
    public MahasiswaViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_laporan_pembelian, parent, false);
        return new MahasiswaViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MahasiswaViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
//        return (dataList != null) ? dataList.size() : 0;
        return 5;
    }

    public class MahasiswaViewHolder extends RecyclerView.ViewHolder{
        private TextView txtNama, txtKode, txtPrice;

        public MahasiswaViewHolder(View itemView) {
            super(itemView);
            txtNama = itemView.findViewById(R.id.tvNama);
            txtKode = itemView.findViewById(R.id.tvKodePembelian);
            txtPrice = itemView.findViewById(R.id.tvPrice);
        }
    }
}