package com.oemartech.sis.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.oemartech.sis.R;
import com.oemartech.sis.model.Customer;
import com.oemartech.sis.model.Member;
import com.oemartech.sis.utils.DateUtils;

import java.util.ArrayList;

public class CustomerAdapter extends RecyclerView.Adapter<CustomerAdapter.MemberViewHolder> {

    private ArrayList<Customer> dataList;
    Context context;
    public CustomerAdapter(ArrayList<Customer> dataList, Context context) {
        this.dataList = dataList;
        this.context = context;
    }

    @Override
    public MemberViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_customer, parent, false);
        return new MemberViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MemberViewHolder holder, final int position) {

        holder.txtNama.setText(dataList.get(position).getNamacust());
        holder.txtTelp.setText(dataList.get(position).getTelpcust());
        System.out.println("DATE : "+ dataList.get(position).getBirthday());
        holder.txtDate.setText(DateUtils.getDate(dataList.get(position).getBirthday()));
        holder.body.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Toast.makeText(context, "KLIK", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return (dataList != null) ? dataList.size() : 0;
    }

    public class MemberViewHolder extends RecyclerView.ViewHolder{
        private TextView txtNama, txtDate, txtTelp;
        private LinearLayout body;

        public MemberViewHolder(View itemView) {
            super(itemView);
            txtNama = (TextView) itemView.findViewById(R.id.tvNama);
            txtTelp = (TextView) itemView.findViewById(R.id.tvTelp);
            txtDate = (TextView) itemView.findViewById(R.id.tvDate);
            body = itemView.findViewById(R.id.body);
        }
    }


}