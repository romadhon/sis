package com.oemartech.sis.adapter;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oemartech.sis.R;
import com.oemartech.sis.activity.penggajian.DetailKomisiActivity;
import com.oemartech.sis.model.Car;
import com.oemartech.sis.model.Komisi;
import com.oemartech.sis.utils.CurrencyUtils;

import java.util.ArrayList;

public class KomisiAdapter extends RecyclerView.Adapter<KomisiAdapter.KomisiViewHolder> {


    private ArrayList<Komisi> dataList;
    private Context context;

    public KomisiAdapter(ArrayList<Komisi> dataList, Context context) {
        this.dataList = dataList;
        this.context = context;
    }

    @Override
    public KomisiViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_komisi, parent, false);
        return new KomisiViewHolder(view);
    }

    @Override
    public void onBindViewHolder(KomisiViewHolder holder, final int position) {
        holder.tv_bulan.setText(dataList.get(position).getBulan());
        holder.tv_komisi.setText(CurrencyUtils.rupiah(Double.parseDouble(dataList.get(position).getNominal())));
        holder.detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, DetailKomisiActivity.class);
                intent.putExtra("idbeli", dataList.get(position).getIddbeli());
                context.startActivity(intent);

            }
        });
    }

    @Override
    public int getItemCount() {
        return (dataList != null) ? dataList.size() : 0;
    }

    public class KomisiViewHolder extends RecyclerView.ViewHolder{
        private TextView tv_bulan, tv_komisi, detail;

        public KomisiViewHolder(View itemView) {
            super(itemView);
            tv_bulan = itemView.findViewById(R.id.tv_bulan);
            tv_komisi = itemView.findViewById(R.id.tv_komisi);
            detail = itemView.findViewById(R.id.detail);
        }
    }
}