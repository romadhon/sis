package com.oemartech.sis.adapter;


import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.oemartech.sis.R;
import com.oemartech.sis.activity.GalleryActivity;
import com.oemartech.sis.model.Car;
import com.oemartech.sis.utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class CarPicturesAdapter extends RecyclerView.Adapter<CarPicturesAdapter.MahasiswaViewHolder> {


    private List<String> dataList;
    Activity activity;

    public CarPicturesAdapter(List<String> dataList, Activity activity) {
        this.dataList = dataList;
        this.activity = activity;
    }

    @Override
    public MahasiswaViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_car_image, parent, false);
        return new MahasiswaViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MahasiswaViewHolder holder, int position) {
        System.out.println("URL IMG : "+Constants.ASSETS+dataList.get(position));
        Glide.with(activity).load(Constants.ASSETS+dataList.get(position)).
                centerCrop().
                placeholder(activity.getResources().getDrawable(R.drawable.brio)).
                into(holder.imgCar);
        holder.imgCar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                JSONArray arrayUrl = new JSONArray();
                for (int i = 0 ; i<dataList.size();i++){
                        arrayUrl.put(dataList.get(i));
                }

                Intent intent = new Intent(activity, GalleryActivity.class);
                intent.putExtra("url",arrayUrl.toString());
                activity.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return (dataList != null) ? dataList.size() : 0;
    }

    public class MahasiswaViewHolder extends RecyclerView.ViewHolder{
        private ImageView imgCar;

        public MahasiswaViewHolder(View itemView) {
            super(itemView);
            imgCar = (ImageView) itemView.findViewById(R.id.imgCar);
        }
    }
}