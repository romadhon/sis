package com.oemartech.sis.adapter.filter;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oemartech.sis.R;
import com.oemartech.sis.model.Car;
import com.oemartech.sis.model.filter.Fuel;

import java.util.ArrayList;

public class FuelAdapter extends RecyclerView.Adapter<FuelAdapter.FuelViewHolder> {


    private ArrayList<Fuel> dataList;
    protected FuelListener fuelListener;

    public FuelAdapter(ArrayList<Fuel> dataList, FuelListener fuelListener) {
        this.dataList = dataList;
        this.fuelListener = fuelListener;
    }

    @Override
    public FuelViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_filter, parent, false);
        return new FuelViewHolder(view);
    }

    @Override
    public void onBindViewHolder(FuelViewHolder h, int position) {
        final Fuel fuel = dataList.get(position);
        h.txtName.setText(fuel.getBahanbakar());
        h.txtName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fuelListener.onBodyClick(fuel);
            }
        });
    }

    @Override
    public int getItemCount() {
        return (dataList != null) ? dataList.size() : 0;
    }

    public class FuelViewHolder extends RecyclerView.ViewHolder{
        private TextView txtName;

        public FuelViewHolder(View itemView) {
            super(itemView);
            txtName = itemView.findViewById(R.id.tv_name);
        }
    }

    public interface FuelListener {
        void onBodyClick(Fuel fuel);
    }
}