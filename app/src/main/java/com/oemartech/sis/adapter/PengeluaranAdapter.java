package com.oemartech.sis.adapter;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.oemartech.sis.R;
import com.oemartech.sis.activity.penggajian.DetailKomisiActivity;
import com.oemartech.sis.model.Komisi;
import com.oemartech.sis.model.PengeluaranMobil;
import com.oemartech.sis.model.RefundBank;
import com.oemartech.sis.utils.Constants;
import com.oemartech.sis.utils.CurrencyUtils;

import java.util.ArrayList;

public class PengeluaranAdapter extends RecyclerView.Adapter<PengeluaranAdapter.PengeluaranViewHolder> {


    private ArrayList<PengeluaranMobil> dataList;
    private Context context;
    private PengeluaranListener pengeluaranListener;

    public PengeluaranAdapter(ArrayList<PengeluaranMobil> dataList, Context context, PengeluaranListener pengeluaranListener) {
        this.dataList = dataList;
        this.context = context;
        this.pengeluaranListener = pengeluaranListener;
    }

    @Override
    public PengeluaranViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_pengeluaran, parent, false);
        return new PengeluaranViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PengeluaranViewHolder holder, final int position) {
        Glide.with(context).load(Constants.ASSETS+dataList.get(position).getUrl()).into(holder.img);
        holder.tv_id.setText(dataList.get(position).getId_datamobil());
        holder.tv_plat.setText(dataList.get(position).getPlatnomor());
        holder.tv_harga.setText(CurrencyUtils.rupiah(Double.parseDouble(dataList.get(position).getHargajual())));
        holder.body.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pengeluaranListener.onPengeluaranClick(dataList.get(position));
            }
        });

    }

    @Override
    public int getItemCount() {
        return (dataList != null) ? dataList.size() : 0;
    }

    public class PengeluaranViewHolder extends RecyclerView.ViewHolder{
        private TextView tv_plat, tv_harga, tv_id;
        private ImageView img;
        private RelativeLayout body;

        public PengeluaranViewHolder(View itemView) {
            super(itemView);
            body = itemView.findViewById(R.id.body);
            img = itemView.findViewById(R.id.img);
            tv_id = itemView.findViewById(R.id.tv_id);
            tv_plat = itemView.findViewById(R.id.tv_police_number);
            tv_harga = itemView.findViewById(R.id.tv_mount);
        }
    }

    public interface PengeluaranListener{
        void onPengeluaranClick(PengeluaranMobil pengeluaranMobil);
    }
}