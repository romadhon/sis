package com.oemartech.sis.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.oemartech.sis.R;
import com.oemartech.sis.model.Pengeluaran;
import com.oemartech.sis.model.RefundBank;
import com.oemartech.sis.utils.Constants;
import com.oemartech.sis.utils.CurrencyUtils;

import java.util.ArrayList;

public class PengeluaranDetailAdapter extends RecyclerView.Adapter<PengeluaranDetailAdapter.PengeluaranViewHolder> {


    protected PengeluaranListener refundBankListener;
    private ArrayList<Pengeluaran> dataList;
    private Context context;

    public PengeluaranDetailAdapter(ArrayList<Pengeluaran> dataList, Context context, PengeluaranListener refundBankListener) {
        this.dataList = dataList;
        this.context = context;
        this.refundBankListener = refundBankListener;
    }

    @Override
    public PengeluaranViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_detail_pengeluaran, parent, false);
        return new PengeluaranViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PengeluaranViewHolder h, final int position) {

        h.tvNominal.setText(CurrencyUtils.rupiah(Double.parseDouble((dataList.get(position).getNominal()))));
        h.tvType.setText(dataList.get(position).getNama_pengeluaran());
        h.btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                refundBankListener.onRefundClick(dataList.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return (dataList != null) ? dataList.size() : 0;
    }

    public class PengeluaranViewHolder extends RecyclerView.ViewHolder{
        private TextView btn_delete;
        private TextView tvType, tvNominal;

        public PengeluaranViewHolder(View itemView) {
            super(itemView);
            btn_delete = itemView.findViewById(R.id.btn_delete);
            tvType = (TextView) itemView.findViewById(R.id.tv_type);
            tvNominal = (TextView) itemView.findViewById(R.id.tv_nominal);
        }
    }

    public interface PengeluaranListener{
        void onRefundClick(Pengeluaran pengeluaran);
    }
}