package com.oemartech.sis.adapter.filter;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oemartech.sis.R;
import com.oemartech.sis.model.Car;
import com.oemartech.sis.model.filter.Type;

import java.util.ArrayList;

public class TypeAdapter extends RecyclerView.Adapter<TypeAdapter.TypeViewHolder> {


    private ArrayList<Type> dataList;
    protected TypeListener typeListener;

    public TypeAdapter(ArrayList<Type> dataList, TypeListener typeListener) {
        this.dataList = dataList;
        this.typeListener = typeListener;
    }

    @Override
    public TypeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_filter, parent, false);
        return new TypeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TypeViewHolder h, int position) {
        final Type type = dataList.get(position);
        h.txtName.setText(type.getNamajenis());
        h.txtName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                typeListener.onBodyClick(type);
            }
        });
    }

    @Override
    public int getItemCount() {
        return (dataList != null) ? dataList.size() : 0;
    }

    public class TypeViewHolder extends RecyclerView.ViewHolder{
        private TextView txtName;

        public TypeViewHolder(View itemView) {
            super(itemView);
            txtName = itemView.findViewById(R.id.tv_name);
        }
    }

    public interface TypeListener {
        void onBodyClick(Type type);
    }
}