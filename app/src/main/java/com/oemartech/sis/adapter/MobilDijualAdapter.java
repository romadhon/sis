package com.oemartech.sis.adapter;


import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.oemartech.sis.R;
import com.oemartech.sis.model.GlobalVar;
import com.oemartech.sis.model.MasterMobildijual;
import com.oemartech.sis.model.MobilDijual;
import com.oemartech.sis.utils.Constants;
import com.oemartech.sis.utils.CurrencyUtils;

import java.util.ArrayList;
import java.util.List;

public class MobilDijualAdapter extends RecyclerView.Adapter<MobilDijualAdapter.CarViewHolder> implements Filterable{

    protected CarAdapterListener mListener;
    private ArrayList<MasterMobildijual> dataList;
    private List<MasterMobildijual> dataListFiltered;
    private Context context;

    public MobilDijualAdapter(ArrayList<MasterMobildijual> dataList, Context context,CarAdapterListener mListener) {
        this.dataList = dataList;
        this.dataListFiltered = dataList;
        this.context = context;
        this.mListener = mListener;
    }

    @Override
    public CarViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_mobil_dijual, parent, false);
        return new CarViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CarViewHolder holder, final int position) {
        holder.txtNama.setText(dataListFiltered.get(position).getNama_Pabrikan()+" "
                +dataListFiltered.get(position).getNamajenismobil()+" "+dataListFiltered.get(position).getNama_varian());
        holder.txtPrice.setText(CurrencyUtils.rupiah(dataListFiltered.get(position).getHargajual()));
        holder.txtYear.setText(dataListFiltered.get(position).getTahun());
        holder.tv_transmisi.setText(dataListFiltered.get(position).getTransmisi());
        String url = Constants.ASSETS+dataListFiltered.get(position).getUrl();
        Glide.with(context)
                .load(url)
//                .centerCrop()
                .placeholder(R.drawable.ic_photo_black_24dp)
                .into(holder.imgCar);
        if ( dataListFiltered.get(position).getStatus_booking() == 1 ){
            holder.lblBtnBooking.setText("Booked");
            holder.btnBooking.setCardBackgroundColor(context.getResources().getColor(R.color.red_300));
        }

        if (dataListFiltered.get(position).getStatus_hold() == 1 ){
            holder.lblBtnHold.setText("Holded");
            holder.btnHold.setCardBackgroundColor(context.getResources().getColor(R.color.red_300));
        }


        holder.btnBeli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onBuyClick(dataListFiltered.get(position));
            }
        });

        holder.btnBooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onBookingClick(dataListFiltered.get(position));
            }
        });

        holder.btnHold.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onHoldClick(dataListFiltered.get(position));
            }
        });

        holder.btn_rating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onWistlist(dataListFiltered.get(position));
            }
        });

        holder.btnDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onDetailClick(dataListFiltered.get(position));
            }
        });

        System.out.println("WRANA : "+ dataListFiltered.get(position).getWarna());
    }

    @Override
    public int getItemCount() {
        return (dataListFiltered != null) ? dataListFiltered.size() : 0;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String data = charSequence.toString();
                String[] charString = charSequence.toString().split("-");
                if(charString[0].startsWith("n")){
                    System.out.println("MASUK NUMBER");
                    List<MasterMobildijual> filteredList = new ArrayList<>();
                    for (MasterMobildijual row : dataList) {

                        if (charString[1].equals("50000000")){
                            System.out.println("MASUK NUMBER A");
                            if (row.getHargajual() <= 50000000){
                                filteredList.add(row);
                            }

                        } else if (charString[1].equals("100000000")){
                            if ((row.getHargajual() > 50000000) && (row.getHargajual() <= 100000000) ){
                                filteredList.add(row);
                            }
                        } else if (charString[1].equals("200000000")){
                            if ((row.getHargajual() > 100000000) && (row.getHargajual() <= 200000000) ){
                                filteredList.add(row);
                            }
                        } else if (charString[1].equals("400000000")){
                            if ((row.getHargajual() > 200000000) && (row.getHargajual() <= 400000000) ){
                                filteredList.add(row);
                            }
                        } else if (charString[1].equals("1000000000")){
                            if ((row.getHargajual() > 400000000) && (row.getHargajual() <= 1000000000)) {
                                filteredList.add(row);
                            }
                        }
                        dataListFiltered = filteredList;
                    }
                } else if (charString[0].startsWith("t")){
                    List<MasterMobildijual> filteredList = new ArrayList<>();
                    for (MasterMobildijual row : dataList) {

                        if (charString[1].equals("1990")){
                            if ((Integer.parseInt(row.getTahun()) >= 1990) && (Integer.parseInt(row.getTahun()) <= 2000)){
                                filteredList.add(row);
                            }
                        } else if (charString[1].equals("2001")){
                            if ((Integer.parseInt(row.getTahun()) >= 2001) && (Integer.parseInt(row.getTahun()) <= 2010)){
                                filteredList.add(row);
                            }
                        } else if (charString[1].equals("2011")){
                            if ((Integer.parseInt(row.getTahun()) >= 2011) && (Integer.parseInt(row.getTahun()) <= 2019)){
                                filteredList.add(row);
                            }
                        }
                        dataListFiltered = filteredList;
                    }
                }else {
                    List<MasterMobildijual> filteredList = new ArrayList<>();
                    for (MasterMobildijual row : dataList) {

                        System.out.println("FILTER : "+row.getWarna().toLowerCase()+" - "+ charString[1].toLowerCase());
                        if (row.getTahun().toLowerCase().contains(charString[1].toLowerCase()) ||
                                row.getNama_Pabrikan().toLowerCase().contains(charString[1].toLowerCase()) ||
                                row.getNama_varian().toLowerCase().contains(charString[1].toLowerCase()) ||
                                row.getNamajenismobil().toLowerCase().contains(charString[1].toLowerCase()) ||
                                row.getNamashowroom().toLowerCase().contains(charString[1].toLowerCase()) ||
                                row.getWarna().toLowerCase().contains(charString[1].toLowerCase()) ||
                                row.getBahanbakar().toLowerCase().contains(charString[1].toLowerCase())

                        ) {
                            filteredList.add(row);
                        }
                    }

                    dataListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = dataListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                dataListFiltered = (ArrayList<MasterMobildijual>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public class CarViewHolder extends RecyclerView.ViewHolder{
        private TextView txtNama, txtPrice, txtYear, tv_transmisi;
        private ImageView imgCar;
        private LinearLayout carDetail;
        private CardView btnBeli, btnBooking, btnHold, btn_rating;
        private Button btnDetail;
        private TextView lblBtnBooking,lblBtnHold;


        public CarViewHolder(View itemView) {
            super(itemView);
            imgCar = itemView.findViewById(R.id.imgCar);
            txtNama = itemView.findViewById(R.id.tvCarName);
            txtPrice = itemView.findViewById(R.id.tvPrice);
            tv_transmisi = itemView.findViewById(R.id.tv_transmisi);
            txtYear = itemView.findViewById(R.id.tvYear);
            carDetail = itemView.findViewById(R.id.carDetail);
            lblBtnBooking = itemView.findViewById(R.id.lbl_btn_booking);
            lblBtnHold = itemView.findViewById(R.id.lbl_btn_hold);
            btnBeli = itemView.findViewById(R.id.btn_beli);
            btnBooking = itemView.findViewById(R.id.btn_booking);
            btnHold = itemView.findViewById(R.id.btn_hold);
            btn_rating = itemView.findViewById(R.id.btn_rating);
            btnDetail = itemView.findViewById(R.id.btn_detail);
//            if (GlobalVar.getInstance().getRole().equals(Constants.ROLE_MEMBER)){
//                btnDetail.setVisibility(View.GONE);
//            }
        }
    }

    public interface CarAdapterListener {
        void onBuyClick(MasterMobildijual mobilDijual);
        void onBookingClick(MasterMobildijual mobilDijual);
        void onHoldClick(MasterMobildijual mobilDijual);
        void onDetailClick(MasterMobildijual mobilDijual);
        void onWistlist(MasterMobildijual mobilDijual);
    }

}