package com.oemartech.sis.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.oemartech.sis.R;
import com.oemartech.sis.model.Car;
import com.oemartech.sis.model.RefundBank;
import com.oemartech.sis.utils.Constants;
import com.oemartech.sis.utils.CurrencyUtils;

import java.util.ArrayList;

public class RefundBankAdapter extends RecyclerView.Adapter<RefundBankAdapter.RefundBankViewHolder> {


    protected RefundBankListener refundBankListener;
    private ArrayList<RefundBank> dataList;
    private Context context;

    public RefundBankAdapter(ArrayList<RefundBank> dataList, Context context, RefundBankListener refundBankListener) {
        this.dataList = dataList;
        this.context = context;
        this.refundBankListener = refundBankListener;
    }

    @Override
    public RefundBankViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_refund_bank, parent, false);
        return new RefundBankViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RefundBankViewHolder holder, final int position) {
        Glide.with(context).load(Constants.ASSETS+dataList.get(position).getUrl()).into(holder.img);
        holder.tvId.setText(dataList.get(position).getIddebeli());
        holder.tvMount.setText(CurrencyUtils.rupiah(dataList.get(position).getHargaakhir()));
        holder.tvPoliceNumber.setText(dataList.get(position).getPlatnomor());
        holder.body.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                refundBankListener.onRefundClick(dataList.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return (dataList != null) ? dataList.size() : 0;
    }

    public class RefundBankViewHolder extends RecyclerView.ViewHolder{
        private RelativeLayout body;
        private TextView tvId, tvMount, tvPoliceNumber;
        private ImageView img;

        public RefundBankViewHolder(View itemView) {
            super(itemView);
            body = itemView.findViewById(R.id.body);
            img = (ImageView) itemView.findViewById(R.id.img);
            tvId = (TextView) itemView.findViewById(R.id.tv_id);
            tvMount = (TextView) itemView.findViewById(R.id.tv_mount);
            tvPoliceNumber = (TextView) itemView.findViewById(R.id.tv_police_number);
        }
    }

    public interface RefundBankListener{
        void onRefundClick(RefundBank refundBank);
    }
}