package com.oemartech.sis.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.oemartech.sis.R;
import com.oemartech.sis.base.BaseActivity;
import com.oemartech.sis.model.SettingCreditCount;
import com.oemartech.sis.utils.SharedPreferenceUtils;

public class PengaturanPerhitunganKreditActivity extends BaseActivity {

    ImageView btn_back;
    Button btn_save;
    EditText etProvisi;
    EditText etBunga;
    EditText etAsuransi;
    EditText etCreditProtection;
    EditText etAdminCost;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pengaturan_perhitungan_kredit);

        SettingCreditCount settingCreditCount = SharedPreferenceUtils.getCountCreditSetting(this);
        etProvisi = findViewById(R.id.et_provisi);
        etProvisi.setText(String.valueOf(settingCreditCount.getProvisi()));

        etBunga = findViewById(R.id.et_bunga);
        etBunga.setText(String.valueOf(settingCreditCount.getBunga()));

        etAsuransi = findViewById(R.id.et_asuransi);
        etAsuransi.setText(String.valueOf(settingCreditCount.getAsuransi()));

        etCreditProtection = findViewById(R.id.et_credit_protection);
        etCreditProtection.setText(String.valueOf(settingCreditCount.getCredit_protection()));

        etAdminCost = findViewById(R.id.et_admin_cost);
        etAdminCost.setText(String.valueOf(settingCreditCount.getAdmin_cost()));

        btn_save = findViewById(R.id.btn_save);
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SharedPreferenceUtils.seveCountCreditSetting(PengaturanPerhitunganKreditActivity.this, new SettingCreditCount(
                        Integer.parseInt(etProvisi.getText().toString()),
                        Integer.parseInt(etBunga.getText().toString()),
                        Float.parseFloat(etAsuransi.getText().toString()),
                        Float.parseFloat(etCreditProtection.getText().toString()),
                        etAdminCost.getText().toString()
                ));

                Toast.makeText(PengaturanPerhitunganKreditActivity.this, "Pengaturan diperbarui", Toast.LENGTH_SHORT).show();
            }
        });

        btn_back = findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }
}
