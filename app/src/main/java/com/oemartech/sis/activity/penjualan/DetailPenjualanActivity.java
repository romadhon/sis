package com.oemartech.sis.activity.penjualan;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.oemartech.sis.R;
import com.oemartech.sis.base.BaseActivity;
import com.oemartech.sis.utils.Constants;
import com.oemartech.sis.utils.CurrencyUtils;

public class DetailPenjualanActivity extends BaseActivity implements View.OnClickListener {
    ImageView imbCar;
    ImageView btn_back, btn_add;
    TextView title;
    TextView tvCarName, tvPrice, tvYear, tvType, tvColor, tvTransmission, tvCondition, tvPoliceNumber;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_penjualan);
        setView();
    }

    private void setView(){
        title = findViewById(R.id.title);
        tvCarName = findViewById(R.id.tvCarName);
        tvColor = findViewById(R.id.tvColor);
        tvPrice = findViewById(R.id.tvPrice);
        tvYear = findViewById(R.id.tvYear);
        tvTransmission = findViewById(R.id.tvTransmission);
        tvType = findViewById(R.id.tvType);
        tvCondition = findViewById(R.id.tvCondition);
        tvPoliceNumber = findViewById(R.id.tvPoliceNumber);
        imbCar = findViewById(R.id.imgCar);
        btn_back = findViewById(R.id.btn_back);
        btn_back.setOnClickListener(this);
        btn_add = findViewById(R.id.btn_add);
        btn_add.setVisibility(View.GONE);

        title.setText("Purchase Detail");
        if (getIntent().getExtras() != null){
            String url = Constants.ASSETS+getIntent().getExtras().getString("url");
            Glide.with(DetailPenjualanActivity.this)
                    .load(url)
                    .centerCrop()
                    .placeholder(R.drawable.ic_photo_black_24dp)
                    .into(imbCar);
            System.out.println("URL : "+url);
            tvCarName.setText(getIntent().getExtras().getString("carname"));
            tvPrice.setText(CurrencyUtils.rupiah(Double.parseDouble(getIntent().getExtras().getString("price")) ) );
            tvColor.setText(getIntent().getExtras().getString("color"));
            tvYear.setText(getIntent().getExtras().getString("year"));
            tvType.setText(getIntent().getExtras().getString("type"));
            tvTransmission.setText(getIntent().getExtras().getString("transmission"));
            tvPoliceNumber.setText(getIntent().getExtras().getString("pnumber"));
            tvCondition.setText(getIntent().getExtras().getString("condition"));
        }
    }

    @Override
    public void onClick(View view) {
        if (view == btn_back){
            onBackPressed();
        }
    }
}
