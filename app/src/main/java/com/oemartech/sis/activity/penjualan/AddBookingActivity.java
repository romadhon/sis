package com.oemartech.sis.activity.penjualan;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.oemartech.sis.R;
import com.oemartech.sis.activity.RegisterActivity;
import com.oemartech.sis.base.BaseActivity;
import com.oemartech.sis.fragment.AddCustimerFragment;
import com.oemartech.sis.model.GlobalVar;
import com.oemartech.sis.model.Member;
import com.oemartech.sis.resful.SingletonRequestQueue;
import com.oemartech.sis.utils.Constants;
import com.oemartech.sis.utils.CurrencyEditText;
import com.oemartech.sis.utils.CurrencyUtils;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AddBookingActivity extends BaseActivity implements View.OnClickListener {
    public static final int action_booking = 1000;
    public static final int action_booking_result = 1001;
    LinearLayout bodyDetail;
    TextView title;
    TextView tvCar, tvYear, tvPrice;
    TextView tvName, tvPhone, tvAddress;
    TextView btn_dateline;
    EditText etSearchCustomer;
    CurrencyEditText etDp;
    ListView customerList;
    ImageView btn_back, btn_add;
    Button chooseCustomer;
    Button submit;
    ProgressBar progressBar;
    AlertDialog dialog;
    LayoutInflater inflater;
    View dialogView;

    ArrayList<String> customerName;
    ArrayList<Integer> customerId;
    String paramidcustomer;
    String paramiduser;
    String paramiddatamobil;
    FragmentManager manager;
    private int mYear, mMonth, mDay;
    private String strDeadLine;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_booking);
        setView();
    }

    private void setView(){
        title = findViewById(R.id.title);
        title.setText("Booking");

        tvCar = findViewById(R.id.tvCarName);
        tvYear = findViewById(R.id.tvYear);
        tvPrice = findViewById(R.id.tvPrice);

        progressBar = findViewById(R.id.progressBar);

        if (getIntent().getExtras()!=null){
            paramiddatamobil = String.valueOf(getIntent().getExtras().getInt("id"));
            tvCar.setText(getIntent().getExtras().getString("name"));
            tvYear.setText(getIntent().getExtras().getString("year"));
            tvPrice.setText(CurrencyUtils.rupiah(getIntent().getExtras().getDouble("price")));
        }

        tvName = findViewById(R.id.tvName);
        tvPhone = findViewById(R.id.tvPhone);
        tvAddress = findViewById(R.id.tvAddress);
        etDp = findViewById(R.id.etDp);

        bodyDetail = findViewById(R.id.bodydetail);

        chooseCustomer = findViewById(R.id.btn_choose_customer);
        chooseCustomer.setOnClickListener(this);
        btn_dateline = findViewById(R.id.btn_dateline);
        btn_dateline.setOnClickListener(this);
        submit = findViewById(R.id.btn_submit);
        submit.setOnClickListener(this);
        btn_add = findViewById(R.id.btn_add);
        btn_add.setVisibility(View.GONE);
        btn_back = findViewById(R.id.btn_back);
        btn_back.setOnClickListener(this);

        submit.setEnabled(false);
        customer_request(false);
    }

    private void setCustomer(){

            bodyDetail.setVisibility(View.VISIBLE);
            tvName.setText("");
            tvPhone.setText("");
            tvAddress.setText("");

    }

    private void setChooseCustomer(){

        manager = getFragmentManager();
        android.app.Fragment frag = manager.findFragmentByTag("customer");
        if (frag != null) {
            manager.beginTransaction().remove(frag).commit();
        }

        dialog = new AlertDialog.Builder(AddBookingActivity.this).create();
        inflater = getLayoutInflater();
        dialogView = inflater.inflate(R.layout.choose_customer, null);
        dialog.setView(dialogView);
        dialog.setTitle("Customer List");

        etSearchCustomer    = (EditText) dialogView.findViewById(R.id.etSearchName);
        Button btnAddNewCustomer = dialogView.findViewById(R.id.btn_add_customer);
        btnAddNewCustomer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddCustimerFragment addCustomer = new AddCustimerFragment();
                Bundle bundle = new Bundle();
                bundle.putBoolean("iscreate",true);
                bundle.putString("from","booking");
                addCustomer.setArguments(bundle);
                addCustomer.show(manager, "customer");
                dialog.dismiss();
            }
        });

        etSearchCustomer.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        customerList    = (ListView) dialogView.findViewById(R.id.customerList);

        final StableArrayAdapter adapter = new StableArrayAdapter(this,
                android.R.layout.simple_list_item_1, customerName);
        customerList.setAdapter(adapter);
        customerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, final View v, int i, long l) {
                requestCustomerDetail(String.valueOf(customerId.get(i)));

            }
        });

        dialog.show();
    }

    @Override
    public void onClick(View view) {
        if (view == chooseCustomer){
            setChooseCustomer();
        } else if (view == submit){
            String strDP =  etDp.getText().toString().replace(",","");

            if (etDp.getText().toString().isEmpty()){
                etDp.setError("Please Entry");
                Toast.makeText(this, "Please entry down payment", Toast.LENGTH_SHORT).show();
            } else if(Double.parseDouble(strDP)<=5000000){
                Toast.makeText(this, "DP must be more than Rp.5,000,000", Toast.LENGTH_SHORT).show();
            } else {
                requestBooking(strDP,paramidcustomer,paramiduser,paramiddatamobil);
            }
        } else if (view == btn_back){
            onBackPressed();
        } else if (view == btn_dateline){
            setDeadLine();
        }
    }

    public void setDeadLine(){

        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(AddBookingActivity.this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        btn_dateline.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                        strDeadLine = btn_dateline.getText().toString();

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    public void customer_request(final boolean isNewCustomer){
        RequestQueue queue = SingletonRequestQueue.getInstance(AddBookingActivity.this).getRequestQueue();
        progressBar.setVisibility(View.VISIBLE);
        chooseCustomer.setEnabled(false);
        VolleyLog.DEBUG = true;
        String uri = Constants.IP+"customers";

        StringRequest stringRequestPOSTJSON = new StringRequest(Request.Method.GET, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressBar.setVisibility(View.GONE);
                chooseCustomer.setEnabled(true);
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    customerName = new ArrayList<>();
                    customerId = new ArrayList<>();
                    for (int i = 0; i< jsonArray.length(); i++){
                     customerName.add(jsonArray.getJSONObject(i).getString("namacust"));
                     customerId.add(jsonArray.getJSONObject(i).getInt("idcust"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (isNewCustomer)
                    setChooseCustomer();
            }
        }, errorListener) {
            @Override
            public Request.Priority getPriority() {
                return Request.Priority.HIGH;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", Constants.CONTENT_TYPE);
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                String requestBody = null;


                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        queue.add(stringRequestPOSTJSON);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request request) {
                progressBar.setVisibility(View.GONE);
                chooseCustomer.setEnabled(true);
            }
        });
    }

    private void requestCustomerDetail(final String idcustomer) {

        RequestQueue queue = SingletonRequestQueue.getInstance(AddBookingActivity.this).getRequestQueue();

        VolleyLog.DEBUG = true;
        String uriCustomer = Constants.IP+"getcustid/"+idcustomer;

        StringRequest customer = new StringRequest(Request.Method.GET, uriCustomer, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                dialog.dismiss();
                VolleyLog.wtf(response);
                bodyDetail.setVisibility(View.VISIBLE);
                submit.setEnabled(true);
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    paramidcustomer = jsonArray.getJSONObject(0).getString("idcust");
                    paramiduser = paramidcustomer;
                    tvName.setText(jsonArray.getJSONObject(0).getString("namacust"));
                    tvPhone.setText(jsonArray.getJSONObject(0).getString("telpcust"));
                    tvAddress.setText(jsonArray.getJSONObject(0).getString("alamatcust"));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, errorListener) {
            @Override
            public Priority getPriority() {
                return Priority.LOW;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
                return headers;
            }

        };
            queue.add(customer);

        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request request) {
                dialog.dismiss();
                submit.setEnabled(true);
            }
        });
    }

    private void requestBooking(final String strDP, final String idcustomer, final String iduser, final String iddatamobil) {

        RequestQueue queue = SingletonRequestQueue.getInstance(AddBookingActivity.this).getRequestQueue();

        VolleyLog.DEBUG = true;
        String uriBooking = Constants.IP+"bookingmobil";

        final StringRequest booking = new StringRequest(Request.Method.POST, uriBooking, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                VolleyLog.wtf(response);
                Toast.makeText(AddBookingActivity.this, response, Toast.LENGTH_SHORT).show();

                Intent returnIntent = new Intent();
                setResult(action_booking_result, returnIntent);
                finish();
            }
        }, errorListener) {
            @Override
            public Priority getPriority() {
                return Priority.LOW;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                params.put("idcustomer", idcustomer);
                params.put("idshowroom", "1");
                params.put("iduser", GlobalVar.getInstance().getId());
                params.put("iddatamobil", iddatamobil);
                params.put("tanggaldeadline", strDeadLine);
                params.put("dp", strDP);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
                return headers;
            }

        };

            queue.add(booking);


        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request request) {
                System.out.println("PARAM : "+request);
            }
        });
    }

    Response.ErrorListener errorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            if (error instanceof NetworkError) {
                Toast.makeText(AddBookingActivity.this, "No network available", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(AddBookingActivity.this, error.toString(), Toast.LENGTH_LONG).show();
            }
        }
    };
    private class StableArrayAdapter extends ArrayAdapter<String> {

        HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();

        public StableArrayAdapter(Context context, int textViewResourceId,
                                  List<String> objects) {
            super(context, textViewResourceId, objects);
            for (int i = 0; i < objects.size(); ++i) {
                mIdMap.put(objects.get(i), i);
            }
        }

        @Override
        public long getItemId(int position) {
            String item = getItem(position);
            return mIdMap.get(item);
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

    }

    public void refreshCustomerList(){
        customer_request(true);
    }
}
