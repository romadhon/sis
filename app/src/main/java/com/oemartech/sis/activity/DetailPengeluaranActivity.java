package com.oemartech.sis.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.oemartech.sis.R;
import com.oemartech.sis.activity.refund.RefundBackDetailActivity;
import com.oemartech.sis.adapter.PengeluaranDetailAdapter;
import com.oemartech.sis.adapter.RefundBankAdapter;
import com.oemartech.sis.model.Pengeluaran;
import com.oemartech.sis.model.PengeluaranMobil;
import com.oemartech.sis.model.RefundBank;
import com.oemartech.sis.resful.SingletonRequestQueue;
import com.oemartech.sis.utils.Constants;
import com.oemartech.sis.utils.CurrencyUtils;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Sevima on 9/28/2018.
 */

public class DetailPengeluaranActivity extends AppCompatActivity {
    //header

    ImageView img;
    TextView title;
    TextView tv_id;
    TextView tv_police_number;
    TextView tv_car_name;
    TextView tv_customer_name;
    TextView tv_mount;

    //body
    Spinner sp_jenis_pengeluaran;
    EditText et_nominal;
    Button btn_save;
    ImageView btn_back;
    ImageView btn_add;
    RecyclerView rv_pengeluaran;
    ProgressBar progressBar;
    ArrayList<Pengeluaran> pengeluaranList = new ArrayList<>();
    ArrayList<Pengeluaran> pengeluaranDetailList = new ArrayList<>();
    PengeluaranDetailAdapter pengeluaranDetailAdapter;

    String strIdPengeluaran;
    String strIdDataMobil;

    String strImg;
    String strId;
    String strIdUser;
    String strIdBank;
    String strPoliceNumber;
    String strCarName;
    String strCustomerName;
    Double strMount;

    PengeluaranMobil pengeluaranMobil;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_pengeluaran);

        title = findViewById(R.id.title);
        img = findViewById(R.id.img);
        tv_id = findViewById(R.id.tv_id);
        tv_police_number = findViewById(R.id.tv_police_number);
        tv_car_name = findViewById(R.id.tv_car_name);
        tv_customer_name = findViewById(R.id.tv_customer_name);
        tv_mount = findViewById(R.id.tv_mount);


        if (getIntent().getExtras()!=null){
            strImg = getIntent().getExtras().getString("strImg");
            strId = getIntent().getExtras().getString("strId");
            strIdDataMobil = getIntent().getExtras().getString("strIdDatamobil");
            strIdUser = getIntent().getExtras().getString("strIdUser");
            strPoliceNumber = getIntent().getExtras().getString("strPoliceNumber");
            strCarName = getIntent().getExtras().getString("strCarName");
            strCustomerName = getIntent().getExtras().getString("strCustomerName");
            strMount = getIntent().getExtras().getDouble("strMount");
            pengeluaranMobil = new Gson().fromJson(getIntent().getExtras().getString("strPengeluaran"), PengeluaranMobil.class);
        }

        title.setText("Detail Pengeluaran");

        if (strImg!=null)
            Glide.with(DetailPengeluaranActivity.this).load(Constants.ASSETS+strImg).into(img);

        if (strId!=null)
            tv_id.setText(pengeluaranMobil.getId_datamobil());

        if (strPoliceNumber!=null)
            tv_police_number.setText(pengeluaranMobil.getPlatnomor());

        if (strCarName!=null)
            tv_car_name.setText(strCarName);

        if (strCustomerName!=null)
            tv_customer_name.setText(strCustomerName);

        if (strMount!=null)
            tv_mount.setText(CurrencyUtils.rupiah(Double.parseDouble(pengeluaranMobil.getHargabeli())));

        sp_jenis_pengeluaran = findViewById(R.id.sp_jenis_pengeluaran);
        et_nominal = findViewById(R.id.et_nominal);
        rv_pengeluaran = findViewById(R.id.rv_pengeluaran);
        progressBar = findViewById(R.id.progressBar);
        btn_save = findViewById(R.id.btn_save);
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (et_nominal.getText().toString().isEmpty()){
                    Toast.makeText(DetailPengeluaranActivity.this, "Nominal Kosong", Toast.LENGTH_SHORT).show();
                }else{
                    setPengeluaran();
                }
            }
        });
        btn_back = findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        btn_add = findViewById(R.id.btn_add);
        btn_add.setVisibility(View.GONE);

        getJenisPengeluaran();
        getDaftarPengeluaran();

    }

    private void setSp_jenis_pengeluaran(){
        String[]spBanks = new String[pengeluaranList.size()];

        for (int i=0; i<pengeluaranList.size();i++){
            spBanks[i] = pengeluaranList.get(i).getNama_pengeluaran();
        }

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_dropdown_item, spBanks);
        sp_jenis_pengeluaran.setAdapter(adapter);
        sp_jenis_pengeluaran.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                strIdPengeluaran = pengeluaranList.get(i).getId_pengeluaran();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });    }

    private void setPengeluaranDetailList(){

        System.out.println("PENGELUARAN SIZE : "+ pengeluaranDetailList.size());
        pengeluaranDetailAdapter = new PengeluaranDetailAdapter(pengeluaranDetailList, this, new PengeluaranDetailAdapter.PengeluaranListener() {
            @Override
            public void onRefundClick(Pengeluaran pengeluaran) {
                deletePengeluaran(pengeluaran);
            }
        });

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rv_pengeluaran.setLayoutManager(layoutManager);
        rv_pengeluaran.setAdapter(pengeluaranDetailAdapter);

    }

    private void getJenisPengeluaran() {
        progressBar.setVisibility(View.VISIBLE);
        RequestQueue queue = SingletonRequestQueue.getInstance(DetailPengeluaranActivity.this).getRequestQueue();

        VolleyLog.DEBUG = true;
        String uri = Constants.IP+"listjenispengeluaran";

        StringRequest getBank = new StringRequest(Request.Method.GET, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Gson gson = new Gson();
                try{
                    Type pengeluaranType = new TypeToken<ArrayList<Pengeluaran>>(){}.getType();
                    pengeluaranList = gson.fromJson(response, pengeluaranType);
                }catch (Exception e){

                }
                setSp_jenis_pengeluaran();
            }
        }, errorListener) {
            @Override
            public Priority getPriority() {
                return Priority.HIGH;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", Constants.CONTENT_TYPE);
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                String requestBody = null;

                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        queue.add(getBank);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request request) {
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void setPengeluaran() {

        RequestQueue queue = SingletonRequestQueue.getInstance(DetailPengeluaranActivity.this).getRequestQueue();
        progressBar.setVisibility(View.VISIBLE);
        VolleyLog.DEBUG = true;
        String uri = Constants.IP+"uploadpengeluaran";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                System.out.println("PENGELUARAN CREATE : "+response);
                try{

                    JSONObject jsonObject = new JSONObject(response);
                    Toast.makeText(DetailPengeluaranActivity.this,jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                }catch (Exception e){

                }


                pengeluaranDetailList = new ArrayList<>();
                getDaftarPengeluaran();
            }
        }, errorListener) {
            @Override
            public Priority getPriority() {
                return Priority.LOW;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("id_datamobil", pengeluaranMobil.getId_datamobil());
                params.put("idpengeluaran", strIdPengeluaran);
                params.put("nominal", et_nominal.getText().toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
                return headers;
            }

        };
        queue.add(stringRequest);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request request) {
                progressBar.setVisibility(View.GONE);
            }
        });
    }



    private void getDaftarPengeluaran() {
        progressBar.setVisibility(View.VISIBLE);
        RequestQueue queue = SingletonRequestQueue.getInstance(DetailPengeluaranActivity.this).getRequestQueue();

        VolleyLog.DEBUG = true;
        String uri = Constants.IP+"listpengeluaran/"+pengeluaranMobil.getId_datamobil();

        StringRequest getBank = new StringRequest(Request.Method.GET, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Gson gson = new Gson();
                try{
                    Type pengeluaranType = new TypeToken<ArrayList<Pengeluaran>>(){}.getType();
                    pengeluaranDetailList = gson.fromJson(response, pengeluaranType);
                }catch (Exception e){

                }
                setPengeluaranDetailList();
            }
        }, errorListener) {
            @Override
            public Priority getPriority() {
                return Priority.HIGH;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", Constants.CONTENT_TYPE);
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                String requestBody = null;

                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        queue.add(getBank);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request request) {
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void deletePengeluaran(Pengeluaran pengeluaran) {
        progressBar.setVisibility(View.VISIBLE);
        RequestQueue queue = SingletonRequestQueue.getInstance(DetailPengeluaranActivity.this).getRequestQueue();

        VolleyLog.DEBUG = true;
        String uri = Constants.IP+"deletepengeluaran/"+pengeluaran.getId_pengeluaran();

        StringRequest getBank = new StringRequest(Request.Method.GET, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                pengeluaranDetailList = new ArrayList<>();
                getDaftarPengeluaran();
            }
        }, errorListener) {
            @Override
            public Priority getPriority() {
                return Priority.HIGH;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", Constants.CONTENT_TYPE);
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                String requestBody = null;

                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        queue.add(getBank);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request request) {
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    Response.ErrorListener errorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {

            if (error instanceof NetworkError) {
                Toast.makeText(DetailPengeluaranActivity.this, "No network available", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(DetailPengeluaranActivity.this, error.toString(), Toast.LENGTH_LONG).show();
            }
        }
    };

}
