package com.oemartech.sis.activity.pembelian;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.oemartech.sis.R;
import com.oemartech.sis.activity.RegisterActivity;
import com.oemartech.sis.activity.lain.ProfilActivity;
import com.oemartech.sis.base.BaseActivity;
import com.oemartech.sis.model.GlobalVar;
import com.oemartech.sis.model.MasterMobil;
import com.oemartech.sis.model.MasterPabrik;
import com.oemartech.sis.model.MasterShowroom;
import com.oemartech.sis.model.MasterVarian;
import com.oemartech.sis.resful.SingletonRequestQueue;
import com.oemartech.sis.resful.VolleyMultipartRequest;
import com.oemartech.sis.utils.Constants;
import com.oemartech.sis.utils.CurrencyEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Sevima on 10/1/2018.
 */

public class InputPembelianActivity extends BaseActivity {
    Spinner spPabrik,spMobil,spWarna,spBahanBakar,spVarian,spLokasi;
    EditText etPlatNomor;
    CurrencyEditText etHargaBeli,etHargaJual;
    EditText etKondisi,etTahun,etCC;
    TextView tvFile,title;
    ImageView back,add;
    RecyclerView rvCarPictures;
    Button btnExpSTNK;
    ProgressBar progressBar;

    List<Bitmap> bFile = new ArrayList<>();
    LinearLayout fileNameContainer;
    TextView tvFilename;
    Button btSimpan;
    Button btFile;
    private RequestQueue rQueue;

    int PICK_IMAGE_MULTIPLE = 3;
    ArrayList<Uri> mArrayUri;
    String imageEncoded;
    List<String> imagesEncodedList;
    String imgDecodableString;
    Map<Integer, String> map ;
    static Map<Integer, String> treeMap;
    private int mYear, mMonth, mDay;
    List<MasterPabrik> masterPabrikList = new ArrayList<>();
    List<MasterMobil> masterMobilList = new ArrayList<>();
    List<MasterVarian> masterVarianList = new ArrayList<>();
    List<MasterShowroom> masterShowroomList = new ArrayList<>();

    String paramidPabrik;
    String paramIdCar;
    String paramIdVarian;
    String paramLokasi;
    String paramPlat;
    String paramWarna;
    String paramBahanBakar;
    String paramHargaBeli;
    String paramHargaJual;
    String paramKondisi;
    String paramTahun;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_pembelian);
        requestMultiplePermissions();
        title = findViewById(R.id.title);
        title.setText("Pembelian Mobil");
        back = findViewById(R.id.btn_back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        rvCarPictures = findViewById(R.id.rvCarPictures);
        btnExpSTNK = findViewById(R.id.btn_date);
        btnExpSTNK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tanggal_mulai();
            }
        });

        add = findViewById(R.id.btn_add);
        add.setVisibility(View.GONE);

        progressBar = findViewById(R.id.progressBar);
        spPabrik = findViewById(R.id.spPabrik);
        spMobil = findViewById(R.id.spMobil);
        spVarian = findViewById(R.id.spVarian);
        spWarna = findViewById(R.id.spWarna);
        spBahanBakar = findViewById(R.id.spBahanBakar);
        spLokasi = findViewById(R.id.spLokasi);

        etPlatNomor = findViewById(R.id.etPlatNomor);
        etKondisi = findViewById(R.id.etKondisi);
        etHargaBeli = findViewById(R.id.etBeli);
        etHargaJual = findViewById(R.id.etJual);
        etTahun = findViewById(R.id.etTahun);
        etCC = findViewById(R.id.etCC);

        tvFilename = findViewById(R.id.tvFile);
        fileNameContainer = findViewById(R.id.fileNameContainer);
        btFile = findViewById(R.id.btn_unggah);

        getFactoryRequest();
        ColorList();
        bahanBakarList();
        getShowroomRequest();

       btFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                galleryIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(galleryIntent, PICK_IMAGE_MULTIPLE);
            }
        });

        btSimpan = findViewById(R.id.btn_simpan);
        btSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validasi();
            }
        });
    }

    public void tanggal_mulai(){
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(InputPembelianActivity.this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        btnExpSTNK.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);


                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    public void validasi(){
        if (etPlatNomor.getText().toString().isEmpty()){
            etPlatNomor.setError("Harus diisi");
        }else if (etHargaBeli.getText().toString().isEmpty()){
            etHargaBeli.setError("Harus diisi");
        }else if (etHargaJual.getText().toString().isEmpty()){
            etHargaJual.setError("Harus diisi");
        }else if (etKondisi.getText().toString().isEmpty()){
            etKondisi.setError("Harus diisi");
        }else if (etTahun.getText().toString().isEmpty()){
            etTahun.setError("Harus diisi");
        }else{
            String strHargaJual =  etHargaJual.getText().toString().replace(",","");
            String strHargaBeli =  etHargaJual.getText().toString().replace(",","");

            paramPlat = etPlatNomor.getText().toString();
            paramHargaBeli = strHargaBeli;
            paramHargaJual = strHargaJual;
            paramKondisi = etKondisi.getText().toString();
            paramTahun = etTahun.getText().toString();

            entry();
        }
    }



    Response.ErrorListener errorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            if (error instanceof NetworkError) {
                Toast.makeText(getApplicationContext(), "No network available", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_LONG).show();
            }
        }
    };


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);


//        try {

            if(requestCode == PICK_IMAGE_MULTIPLE && resultCode == RESULT_OK
                    && null != data) {

                String[] filePathColumn = { MediaStore.Images.Media.DATA };
                imagesEncodedList = new ArrayList<String>();
                if(data.getData()!=null){

                    Uri mImageUri=data.getData();

                    // Get the cursor
                    Cursor cursor = getContentResolver().query(mImageUri,
                            filePathColumn, null, null, null);
                    // Move to first row
                    cursor.moveToFirst();
                    Toast.makeText(this, "PICK : "+filePathColumn.length, Toast.LENGTH_SHORT).show();
                    bFile = new ArrayList<>();
                    List<String> fileName = new ArrayList<>();
                    fileNameContainer.removeAllViewsInLayout();
                    try {
                        fileName.add(getFileName(mImageUri));

                        bFile.add(0,MediaStore.Images.Media.getBitmap(this.getContentResolver(), mImageUri));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    setFileName(fileName);
                    cursor.close();


                } else {

                    if (data.getClipData() != null) {
                        ClipData mClipData = data.getClipData();
                         mArrayUri = new ArrayList<Uri>();
                        for (int i = 0; i < mClipData.getItemCount(); i++) {

                            ClipData.Item item = mClipData.getItemAt(i);
                            Uri uri = item.getUri();
                            mArrayUri.add(uri);
                            // Get the cursor
                            Cursor cursor = getContentResolver().query(uri, filePathColumn, null, null, null);
                            // Move to first row
                            cursor.moveToFirst();

                            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                            imageEncoded  = cursor.getString(columnIndex);
                            imagesEncodedList.add(imageEncoded);


                            cursor.close();

                        }


                        fileNameContainer.removeAllViewsInLayout();
                        List<String> fileName = new ArrayList<>(mArrayUri.size());
                        bFile = new ArrayList<>();
                        for (int j=0; j<mArrayUri.size();j++) {
                            try {
                                fileName.add(getFileName(mArrayUri.get(j)));
                                bFile.add(j,MediaStore.Images.Media.getBitmap(this.getContentResolver(), mArrayUri.get(j)));
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }

                        setFileName(fileName);
                        Log.v("LOG_TAG", "Selected Images" + mArrayUri.size());
                    }
                }
            } else {
                System.out.println("Ada sesuatu yang salah");
            }

//        } catch (Exception e) {
//            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
//                    .show();
//
//        }
    }

    private void setFileName(List<String> fileName){
        for (int i= 0; i< fileName.size();i++){
            TextView tv = new TextView(getApplicationContext());

            ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT, // Width of TextView
                    ViewGroup.LayoutParams.WRAP_CONTENT); // Height of TextView

            tv.setId(i);
            tv.setLayoutParams(lp);
            tv.setText(fileName.get(i));
            tv.setTextColor(getResources().getColor(R.color.blue_500));
            fileNameContainer.addView(tv);
        }

    }

    public String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }


    public void ColorList(){
        final String[] warna={"Putih","Hitam","Merah","Kuning"};
        final ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, warna);
        spWarna.setAdapter(adapter);
        spWarna.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                paramWarna = adapter.getItem(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public void bahanBakarList(){
        final String[] bb={"Bensin","Diesel","Listrik"};
        final ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, bb);
        spBahanBakar.setAdapter(adapter);
        spBahanBakar.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                paramBahanBakar = adapter.getItem(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }


    public void factoryList(){

        List<String> value = new ArrayList<>();
        for (int i = 0; i<masterPabrikList.size();i++){
            value.add(masterPabrikList.get(i).getNama_Pabrikan());
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_dropdown_item, value);
        spPabrik.setAdapter(adapter);
        spPabrik.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                paramidPabrik = String.valueOf(masterPabrikList.get(i).getID_Pabrikan());
               getCarsRequest(view.getContext(),paramidPabrik);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public void CarList(){
        List<String> value = new ArrayList<>();
        for (int i = 0; i<masterMobilList.size();i++){
            value.add(masterMobilList.get(i).getNamajenismobil());
        }
        final ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, value);
        spMobil.setAdapter(adapter);
        spMobil.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                paramIdCar = String.valueOf(masterMobilList.get(i).getIdjenismobil());
                getVarianRequest(paramIdCar);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public void varianList(){
        List<String> value = new ArrayList<>();
        for (int i = 0; i<masterVarianList.size();i++){
            value.add(masterVarianList.get(i).getNama_varian());
        }
        final ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, value);
        spVarian.setAdapter(adapter);
        spVarian.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                paramIdVarian = String.valueOf(masterVarianList.get(i).getId_varian());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public void showroomList(){
        List<String> value = new ArrayList<>();
        for (int i = 0; i<masterShowroomList.size();i++){
            value.add(masterShowroomList.get(i).getNamashowroom());
        }
        final ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, value);
        spLokasi.setAdapter(adapter);
        spLokasi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                paramLokasi = String.valueOf(masterShowroomList.get(i).getIdshowroom());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }



    public void getCarsRequest(final Context context, String id) {
        final Map<Integer, String> map = new IdentityHashMap<Integer, String>();


        RequestQueue queue = SingletonRequestQueue.getInstance(context).getRequestQueue();

        VolleyLog.DEBUG = true;
        String uri = "http://showroom.theidealstore.id/public/api/getjenismobilby/"+id;

        StringRequest stringRequestPOSTJSON = new StringRequest(Request.Method.GET, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Gson gson = new Gson();
                try{

                    Type masterMobilListType = new TypeToken<ArrayList<MasterMobil>>(){}.getType();
                    masterMobilList = gson.fromJson(response, masterMobilListType);
                }catch (Exception e){

                }

                    CarList();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof NetworkError) {
                    Toast.makeText(context, "No network available", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(context, error.toString(), Toast.LENGTH_LONG).show();
                }
            }
        }) {
            @Override
            public Request.Priority getPriority() {
                return Request.Priority.HIGH;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", Constants.CONTENT_TYPE);
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                String requestBody = null;


                return requestBody == null ? null : requestBody.getBytes(StandardCharsets.UTF_8);
            }
        };
        queue.add(stringRequestPOSTJSON);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request request) {
            }
        });

    }

    public void getFactoryRequest() {
        final Map<Integer, String> map = new IdentityHashMap<Integer, String>();


        RequestQueue queue = SingletonRequestQueue.getInstance(InputPembelianActivity.this).getRequestQueue();

        VolleyLog.DEBUG = true;
        String uri = "http://showroom.theidealstore.id/public/api/masterpabrikan";

        StringRequest stringRequestPOSTJSON = new StringRequest(Request.Method.GET, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Gson gson = new Gson();
                try{

                    Type masterPabrikListType = new TypeToken<ArrayList<MasterPabrik>>(){}.getType();
                    masterPabrikList = gson.fromJson(response, masterPabrikListType);
                }catch (Exception e){

                }

                factoryList();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof NetworkError) {
                    Toast.makeText(InputPembelianActivity.this, "No network available", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(InputPembelianActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                }
            }
        }) {
            @Override
            public Request.Priority getPriority() {
                return Request.Priority.HIGH;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", Constants.CONTENT_TYPE);
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                String requestBody = null;


                return requestBody == null ? null : requestBody.getBytes(StandardCharsets.UTF_8);
            }
        };
        queue.add(stringRequestPOSTJSON);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request request) {
            }
        });

    }


    public void getVarianRequest(String id) {
        final Map<Integer, String> map = new IdentityHashMap<Integer, String>();


        RequestQueue queue = SingletonRequestQueue.getInstance(InputPembelianActivity.this).getRequestQueue();

        VolleyLog.DEBUG = true;
        String uri = "http://showroom.theidealstore.id/public/api/getvarianmobilby/"+id;

        StringRequest stringRequestPOSTJSON = new StringRequest(Request.Method.GET, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Gson gson = new Gson();
                try{

                    Type masterVarianListType = new TypeToken<ArrayList<MasterVarian>>(){}.getType();
                    masterVarianList = gson.fromJson(response, masterVarianListType);
                }catch (Exception e){

                }

                varianList();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof NetworkError) {
                    Toast.makeText(InputPembelianActivity.this, "No network available", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(InputPembelianActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                }
            }
        }) {
            @Override
            public Request.Priority getPriority() {
                return Request.Priority.HIGH;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", Constants.CONTENT_TYPE);
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                String requestBody = null;


                return requestBody == null ? null : requestBody.getBytes(StandardCharsets.UTF_8);
            }
        };
        queue.add(stringRequestPOSTJSON);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request request) {
            }
        });

    }


    public void getShowroomRequest() {
        final Map<Integer, String> map = new IdentityHashMap<Integer, String>();


        RequestQueue queue = SingletonRequestQueue.getInstance(InputPembelianActivity.this).getRequestQueue();

        VolleyLog.DEBUG = true;
        String uri = "http://showroom.theidealstore.id/public/api/listshowroom";

        StringRequest stringRequestPOSTJSON = new StringRequest(Request.Method.GET, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Gson gson = new Gson();
                try{

                    Type showroomListType = new TypeToken<ArrayList<MasterShowroom>>(){}.getType();
                    masterShowroomList = gson.fromJson(response, showroomListType);
                }catch (Exception e){

                }

                showroomList();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof NetworkError) {
                    Toast.makeText(InputPembelianActivity.this, "No network available", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(InputPembelianActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                }
            }
        }) {
            @Override
            public Request.Priority getPriority() {
                return Request.Priority.HIGH;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", Constants.CONTENT_TYPE);
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                String requestBody = null;


                return requestBody == null ? null : requestBody.getBytes(StandardCharsets.UTF_8);
            }
        };
        queue.add(stringRequestPOSTJSON);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request request) {
            }
        });

    }


    private void entry(){
        progressBar.setVisibility(View.VISIBLE);
        String uriUploadPhoto = Constants.IP+"uploadmobildijual";
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, uriUploadPhoto,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        progressBar.setVisibility(View.GONE);
                        rQueue.getCache().clear();
                        Toast.makeText(InputPembelianActivity.this, new String(response.data), Toast.LENGTH_SHORT).show();
                        finish();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("id_pabrikan", paramidPabrik);
                params.put("id_jenismobil", paramIdCar);
                params.put("id_varian", paramIdVarian);
                params.put("idshowroom", paramLokasi);
                params.put("bahanbakar", paramBahanBakar);
                params.put("platnomor", paramPlat);
                params.put("warna", paramWarna);
                params.put("hargabeli", paramHargaBeli);
                params.put("hargajual", paramHargaJual);
                params.put("kondisi", paramKondisi);
                params.put("tahun", paramTahun);
                params.put("expstnk", btnExpSTNK.getText().toString());
                params.put("jumlahgambar",String.valueOf(bFile.size()));
//                params.put("cc", etCC.getText().toString());
                return params;
            }

            /*
             *pass files using below method
             * */
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                long imagename = System.currentTimeMillis();
                for (int i = 0; i< bFile.size(); i++){
                    params.put("file["+i+"]", new DataPart(imagename + ".png", getFileDataFromDrawable(bFile.get(i))));
                }
                Log.i("Param Img : ", params.toString());
                return params;
            }
        };


        System.out.println("PARAM : "+volleyMultipartRequest.getPostBodyContentType());
        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        rQueue = Volley.newRequestQueue(InputPembelianActivity.this);
        rQueue.add(volleyMultipartRequest);
    }

    public byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 80, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    private void  requestMultiplePermissions(){
        Dexter.withActivity(this)
                .withPermissions(

                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            Toast.makeText(getApplicationContext(), "All permissions are granted by user!", Toast.LENGTH_SHORT).show();
                        }

                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings

                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        Toast.makeText(getApplicationContext(), "Some Error! ", Toast.LENGTH_SHORT).show();
                    }
                })
                .onSameThread()
                .check();
    }


}
