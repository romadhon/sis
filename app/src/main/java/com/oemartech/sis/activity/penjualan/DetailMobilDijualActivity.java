package com.oemartech.sis.activity.penjualan;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.oemartech.sis.R;
import com.oemartech.sis.adapter.CarPicturesAdapter;
import com.oemartech.sis.base.BaseActivity;
import com.oemartech.sis.resful.SingletonRequestQueue;
import com.oemartech.sis.utils.Constants;
import com.oemartech.sis.utils.CurrencyUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DetailMobilDijualActivity extends BaseActivity {
    ImageView imgcar,imgcarEdit,btn_back,btnEdit;
    TextView jmlImage;
    TextView txtname,txtprice,txtpolice,txtyear,txttransmission,txtcondition,txttype,txtcolor, txtbahanbakar;
    EditText etName,etPrice,etPolice,etYear,etTransmission,etCondition,etType,etColor, etBahanbakar;
    Button btnDelete,btnSave;
    ImageView add_image;
    LinearLayout linearoption;
    RecyclerView rvCarPictures;
    ProgressBar progressBar,progressBarImg;

    CarPicturesAdapter carPicturesAdapter;
    Bitmap bFile;
    private RequestQueue rQueue;
    private static final int SELECT_PICTURE = 1;

    double paramHargaBeli;
    int paramidjenismobil;
    int paramidpabrikan;
    int paramidvarian;
    int paramidshowroom;
    String paramexpstnk;
    String parambahanbakar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_mobil_dijual);
        setView();
    }

    private void setView(){
        rvCarPictures = findViewById(R.id.rvCarPictures);

        imgcar =  findViewById(R.id.imgCar);
        imgcarEdit =  findViewById(R.id.imgCarEdit);
        jmlImage =  findViewById(R.id.jmlImage);
        txtname =  findViewById(R.id.txtName);
        txtprice =  findViewById(R.id.txtPrice);
        txtpolice =  findViewById(R.id.txtPolice);
        txtyear =   findViewById(R.id.txtYear);
        txtcondition =   findViewById(R.id.txtCondition);
        txttransmission =   findViewById(R.id.txtTransmission);
        txttype =   findViewById(R.id.txtType);
        txtcolor =   findViewById(R.id.txtColor);
        txtbahanbakar =  findViewById(R.id.txtBahanbakar);

        etName =  findViewById(R.id.etName);
        etPrice =  findViewById(R.id.etPrice);
        etPolice =  findViewById(R.id.etPolice);
        etYear =   findViewById(R.id.etYear);
        etCondition =   findViewById(R.id.etCondition);
        etTransmission =   findViewById(R.id.etTransmission);
        etType =   findViewById(R.id.etType);
        etColor =   findViewById(R.id.etColor);
        etBahanbakar =  findViewById(R.id.etBahanbakar);

        linearoption =  findViewById(R.id.linearoption);
        btnDelete =  findViewById(R.id.btn_delete);
        btnEdit =  findViewById(R.id.btn_edit);
        add_image =  findViewById(R.id.add_image);
        btnSave =  findViewById(R.id.btn_save);
        btnSave.setVisibility(View.GONE);
        btn_back = findViewById(R.id.btn_back);

        progressBar =  findViewById(R.id.progressBar);
        progressBarImg =  findViewById(R.id.progressBarImg);

        if (getIntent()!=null){
            paramHargaBeli = getIntent().getExtras().getDouble("buy");
            paramidjenismobil = getIntent().getExtras().getInt("idjenismobil");
            paramidpabrikan = getIntent().getExtras().getInt("idpabrikan");
            paramidvarian = getIntent().getExtras().getInt("idvarian");
            paramidshowroom = getIntent().getExtras().getInt("idshowroom");
            paramexpstnk = getIntent().getExtras().getString("expstnk");
            parambahanbakar = getIntent().getExtras().getString("bahanbakar");


            txtname.setText(getIntent().getExtras().getString("name"));
            txtprice.setText(CurrencyUtils.rupiah(getIntent().getExtras().getDouble("price")));
            txtpolice.setText(getIntent().getExtras().getString("police"));
            txttype.setText(getIntent().getExtras().getString("type"));
            txtcolor.setText(getIntent().getExtras().getString("color"));
            txttransmission.setText(getIntent().getExtras().getString("transmission"));
            txtyear.setText(getIntent().getExtras().getString("year"));
            txtcondition.setText(getIntent().getExtras().getString("condition"));
            txtbahanbakar.setText(parambahanbakar);

        }
        detail_request(String.valueOf(getIntent().getExtras().getInt("id")));
        goneEditText();
        visibleTextView();

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteCar();
            }
        });

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnDelete.setVisibility(View.GONE);
                btnSave.setVisibility(View.VISIBLE);

                etName.setText(txtname.getText());
                etPrice.setText(String.format ("%.0f", getIntent().getExtras().getDouble("price")));
                etPolice.setText(txtpolice.getText());
                etYear.setText(txtyear.getText());
                etCondition.setText(txtcondition.getText());
                etTransmission.setText(txttransmission.getText());
                etType.setText(txttype.getText());
                etColor.setText(txtcolor.getText());
                etBahanbakar.setText(parambahanbakar);

                visibleEditText();
                goneTextView();
            }
        });


        add_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                galleryIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                startActivityForResult(galleryIntent, 1);
            }
        });


        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                add_image.setVisibility(View.GONE);
                imgcarEdit.setVisibility(View.GONE);
                edit_request(getIntent().getExtras().getInt("id"));
            }
        });

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    public void visibleEditText(){
//        etName.setVisibility(View.VISIBLE);
        etPrice.setVisibility(View.VISIBLE);
        etPolice.setVisibility(View.VISIBLE);
        etYear.setVisibility(View.VISIBLE);
        etCondition.setVisibility(View.VISIBLE);
//        etTransmission.setVisibility(View.VISIBLE);
//        etType.setVisibility(View.VISIBLE);
        etColor.setVisibility(View.VISIBLE);
        etBahanbakar.setVisibility(View.VISIBLE);
    }

    public void visibleTextView(){
        txtname.setVisibility(View.VISIBLE);
        txtprice.setVisibility(View.VISIBLE);
        txtpolice.setVisibility(View.VISIBLE);
        txtyear.setVisibility(View.VISIBLE);
        txtcondition.setVisibility(View.VISIBLE);
        txttransmission.setVisibility(View.VISIBLE);
        txttype.setVisibility(View.VISIBLE);
        txtcolor.setVisibility(View.VISIBLE);
        txtbahanbakar.setVisibility(View.VISIBLE);
    }

    public void goneEditText(){
        etName.setVisibility(View.GONE);
        etPrice.setVisibility(View.GONE);
        etPolice.setVisibility(View.GONE);
        etYear.setVisibility(View.GONE);
        etCondition.setVisibility(View.GONE);
        etTransmission.setVisibility(View.GONE);
        etType.setVisibility(View.GONE);
        etColor.setVisibility(View.GONE);
        etBahanbakar.setVisibility(View.GONE);
    }

    public void goneTextView(){
//        txtname.setVisibility(View.GONE);
        txtprice.setVisibility(View.GONE);
        txtpolice.setVisibility(View.GONE);
        txtyear.setVisibility(View.GONE);
        txtcondition.setVisibility(View.GONE);
        txttransmission.setVisibility(View.GONE);
//        txttype.setVisibility(View.GONE);
        txtcolor.setVisibility(View.GONE);
        txtbahanbakar.setVisibility(View.GONE);
    }

    public void setRecyclerview(List<String> dataList){
        jmlImage.setVisibility(View.VISIBLE);
        jmlImage.setText(String.valueOf(dataList.size())+" Gambar");
        carPicturesAdapter = new CarPicturesAdapter(dataList, DetailMobilDijualActivity.this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(DetailMobilDijualActivity.this, LinearLayoutManager.HORIZONTAL, false);
        rvCarPictures.setLayoutManager(layoutManager);
        rvCarPictures.setAdapter(carPicturesAdapter);
    }


    public void deleteCar(){
        new AlertDialog.Builder(DetailMobilDijualActivity.this)
                .setMessage("Delete Car ?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        delete_request(String.valueOf(getIntent().getExtras().getInt("id")));
                    }
                })

                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                })
                .show();
    }

    private void detail_request(final String id) {

        RequestQueue queue = SingletonRequestQueue.getInstance(this).getRequestQueue();
        progressBarImg.setVisibility(View.VISIBLE);
        VolleyLog.DEBUG = true;
        String uriHold = "http://showroom.theidealstore.id/public/api/getmobildijual/"+id;

        final StringRequest unbooking = new StringRequest(Request.Method.GET, uriHold, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                VolleyLog.wtf(response);
                progressBarImg.setVisibility(View.GONE);

                List<String> allImg = new ArrayList<String>();

                try {
                    JSONObject jsonDetailCar = new JSONObject(response);
                    JSONArray cast = jsonDetailCar.getJSONArray("galeri");
                    for (int i=0; i<cast.length(); i++) {

                        JSONObject jsonUrl = new JSONObject(cast.getString(i));

                        allImg.add(jsonUrl.getString("url"));
                    }

                    setRecyclerview(allImg);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, errorListener) {
            @Override
            public Priority getPriority() {
                return Priority.LOW;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();


                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
                return headers;
            }

        };

        queue.add(unbooking);


        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request request) {

                progressBarImg.setVisibility(View.GONE);
            }
        });
    }

    private void delete_request(final String id) {

        RequestQueue queue = SingletonRequestQueue.getInstance(DetailMobilDijualActivity.this).getRequestQueue();
        progressBar.setVisibility(View.VISIBLE);
        VolleyLog.DEBUG = true;
        String uriHold = Constants.IP+ "deletemobildijual/"+id;

        final StringRequest unbooking = new StringRequest(Request.Method.GET, uriHold, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                VolleyLog.wtf(response);
                progressBar.setVisibility(View.GONE);
                Toast.makeText(DetailMobilDijualActivity.this, response, Toast.LENGTH_SHORT).show();
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_OK,returnIntent);
                finish();
            }
        }, errorListener) {
            @Override
            public Priority getPriority() {
                return Priority.LOW;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();


                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
                return headers;
            }

        };

        queue.add(unbooking);


        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request request) {
                System.out.println("PARAM : "+request);
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    public void edit_request(final int id){
        RequestQueue queue = SingletonRequestQueue.getInstance(this).getRequestQueue();
        progressBar.setVisibility(View.VISIBLE);
        VolleyLog.DEBUG = true;
        String uri = Constants.IP+"editmobildijual";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                VolleyLog.wtf(response);
                progressBar.setVisibility(View.GONE);
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_OK,returnIntent);
                finish();
            }
        }, errorListener) {
            @Override
            public Priority getPriority() {
                return Priority.LOW;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("id_datamobil", String.valueOf(id));
                params.put("id_pabrikan", String.valueOf(paramidpabrikan));
                params.put("id_jenismobil", String.valueOf(paramidjenismobil));
                params.put("idshowroom", String.valueOf(paramidshowroom));
                params.put("id_varian", String.valueOf(paramidvarian));
                params.put("platnomor", etPolice.getText().toString());
                params.put("warna", etColor.getText().toString());
                params.put("hargabeli", String.format ("%.0f", paramHargaBeli));
                params.put("hargajual", etPrice.getText().toString().replace(",",""));
                params.put("tahun", etYear.getText().toString());
                params.put("expstnk", paramexpstnk);
                params.put("bahanbakar", etBahanbakar.getText().toString());
                params.put("kondisi", etCondition.getText().toString());

                System.out.println("PARAM : "+params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
                return headers;
            }

        };
        queue.add(stringRequest);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request request) {
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    Response.ErrorListener errorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            progressBar.setVisibility(View.GONE);
            if (error instanceof NetworkError) {
                Toast.makeText(DetailMobilDijualActivity.this, "No network available", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(DetailMobilDijualActivity.this, error.toString(), Toast.LENGTH_LONG).show();
            }
        }
    };
}
