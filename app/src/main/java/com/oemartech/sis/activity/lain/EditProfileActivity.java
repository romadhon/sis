package com.oemartech.sis.activity.lain;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.oemartech.sis.R;
import com.oemartech.sis.base.BaseActivity;
import com.oemartech.sis.model.GlobalVar;
import com.oemartech.sis.resful.SingletonRequestQueue;
import com.oemartech.sis.resful.VolleyMultipartRequest;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EditProfileActivity extends BaseActivity implements View.OnClickListener {

    TextView title;
    ImageView back;
    ImageView add;
    ImageView imgProfile;
    ImageView btnChoosePicture;
    EditText etNama;
    EditText etEmail;
    EditText etHp;
    EditText etAlamat;
    EditText etPassword;
    Button simpan;
    ProgressDialog pDialog;
    private RequestQueue rQueue;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        setView();

    }

    private void setView(){
        title = findViewById(R.id.title);
        add = findViewById(R.id.btn_add);
        add.setVisibility(View.GONE);
        imgProfile = findViewById(R.id.imgProfile);
        btnChoosePicture = findViewById(R.id.choose_img);
        btnChoosePicture.setOnClickListener(this);

        etNama = findViewById(R.id.etNama);
        etEmail = findViewById(R.id.etEmail);
        etHp = findViewById(R.id.etHp);
        etAlamat = findViewById(R.id.etAlamat);
        etPassword = findViewById(R.id.etPassword);
        etPassword.setOnClickListener(this);

        if (getIntent().getExtras()!=null){
            etNama.setText(getIntent().getExtras().getString("nama"));
            etEmail.setText(getIntent().getExtras().getString("email"));
            etHp.setText(getIntent().getExtras().getString("hp"));
            etAlamat.setText(getIntent().getExtras().getString("alamat"));
        }

        simpan = findViewById(R.id.btn_simpan);
        simpan.setOnClickListener(this);
        back = findViewById(R.id.btn_back);
        back.setOnClickListener(this);
    }

    private void validasi(){
        if (etNama.getText().toString().isEmpty()){
            etNama.setError("harus diisi");
        }else if (etEmail.getText().toString().isEmpty()){
            etEmail.setError("harus diisi");
        }else if (etHp.getText().toString().isEmpty()){
            etHp.setError("harus diisi");
        }else if (etAlamat.getText().toString().isEmpty()){
            etAlamat.setError("harus diisi");
        } else {

        }
    }

    @Override
    public void onClick(View view) {
        if (view == btnChoosePicture){
            Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(galleryIntent, 1);
        }else if(view == etPassword){
            Toast.makeText(this, "Ganti Password", Toast.LENGTH_SHORT).show();
        }else if(view == simpan){
            validasi();
        }else if(view==back){
            onBackPressed();
        }
    }

    private void updateProfileRequest() {
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        pDialog.setMessage("wait ...");
        showDialog();

        RequestQueue queue = SingletonRequestQueue.getInstance(getApplicationContext()).getRequestQueue();

        VolleyLog.DEBUG = true;
        String uri = "http://showroom.theidealstore.id/public/api/updateuser";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                GlobalVar.getInstance().setNama(etNama.getText().toString());
                GlobalVar.getInstance().setEmail(etEmail.getText().toString());
                GlobalVar.getInstance().setNohp(etHp.getText().toString());
                GlobalVar.getInstance().setAlamat(etAlamat.getText().toString());

            }
        }, errorListener) {
            @Override
            public Priority getPriority() {
                return Priority.LOW;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("name", etNama.getText().toString());
                params.put("password", GlobalVar.getInstance().getPassword());
                params.put("email", etEmail.getText().toString());
                params.put("nohp", etHp.getText().toString());
                params.put("alamat", etAlamat.getText().toString());
                params.put("role", GlobalVar.getInstance().getRole());
                params.put("id", GlobalVar.getInstance().getId());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
                return headers;
            }

        };


        queue.add(stringRequest);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request request) {
                hideDialog();
            }
        });
    }

    Response.ErrorListener errorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            hideDialog();
            if (error instanceof NetworkError) {
                Toast.makeText(getApplicationContext(), "No network available", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_LONG).show();
            }
        }
    };

    private void uploadImage(final Bitmap bitmap){
        String uriUploadPhoto = "http://showroom.theidealstore.id/public/api/updateprofpict";
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, uriUploadPhoto,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        Log.d("ressssssoo",new String(response.data));
                        rQueue.getCache().clear();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * here we have only one parameter with the image
             * which is tags
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("id", GlobalVar.getInstance().getId());
                return params;
            }

            /*
             *pass files using below method
             * */
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                long imagename = System.currentTimeMillis();
                params.put("file", new DataPart(imagename + ".png", getFileDataFromDrawable(bitmap)));
                return params;
            }
        };


        System.out.println("PARAM : "+volleyMultipartRequest.getPostBodyContentType());
        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        rQueue = Volley.newRequestQueue(EditProfileActivity.this);
        rQueue.add(volleyMultipartRequest);
    }

    public byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 80, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    private void  requestMultiplePermissions(){
        Dexter.withActivity(this)
                .withPermissions(

                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            Toast.makeText(getApplicationContext(), "All permissions are granted by user!", Toast.LENGTH_SHORT).show();
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings

                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        Toast.makeText(getApplicationContext(), "Some Error! ", Toast.LENGTH_SHORT).show();
                    }
                })
                .onSameThread()
                .check();
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
