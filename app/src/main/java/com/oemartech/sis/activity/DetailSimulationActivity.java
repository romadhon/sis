package com.oemartech.sis.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.piasy.biv.BigImageViewer;
import com.github.piasy.biv.loader.glide.GlideCustomImageLoader;
import com.oemartech.sis.R;
import com.oemartech.sis.adapter.GalleryAdapter;
import com.oemartech.sis.base.BaseActivity;
import com.oemartech.sis.utils.Constants;
import com.oemartech.sis.utils.CurrencyUtils;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class DetailSimulationActivity extends BaseActivity {
    String url;
    TextView tvHargaMobil;
    TextView tvDP;
    TextView tvPH;
    TextView tvProvisi;
    TextView tvTotalHutang;
    TextView tvTenor;
    TextView tvBunga;
    TextView tvAngsuran;
    TextView tvTotalBayarPertama;
    ImageView btn_back;

    double otr;
    double tdp;
    double phmurni;
    double provisi;
    double total_hutang;
    int tenor;
    double bunga;
    double angsuran;
    double dp;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simulation_detail);
        if (getIntent().getExtras()!=null){
            otr = getIntent().getExtras().getDouble("otr");
            tdp = getIntent().getExtras().getDouble("tdp");
            phmurni = getIntent().getExtras().getDouble("phmurni");
            provisi = getIntent().getExtras().getDouble("provisi");
            total_hutang = getIntent().getExtras().getDouble("total_hutang");
            tenor = getIntent().getExtras().getInt("tenor");
            bunga = getIntent().getExtras().getDouble("bunga");
            angsuran = getIntent().getExtras().getDouble("angsuran");
            dp = getIntent().getExtras().getDouble("dp");
        }

        tvHargaMobil = findViewById(R.id.tv_harga_mobil);
        tvHargaMobil.setText(CurrencyUtils.rupiah(otr));
        tvDP = findViewById(R.id.tv_tdp);
        tvDP.setText(CurrencyUtils.rupiah(tdp));
        tvPH = findViewById(R.id.tv_ph);
        tvPH.setText(CurrencyUtils.rupiah(phmurni));
        tvProvisi = findViewById(R.id.tv_provisi);
        tvProvisi.setText(CurrencyUtils.rupiah(provisi));
        tvTotalHutang = findViewById(R.id.tv_total_hutang);
        tvTotalHutang.setText(CurrencyUtils.rupiah(total_hutang));
        tvTenor = findViewById(R.id.tv_tenor);
        tvTenor.setText(tenor+" Bulan");
        tvBunga = findViewById(R.id.tv_bunga);
        tvBunga.setText(bunga+" %");
        tvAngsuran = findViewById(R.id.tv_angsuran);
        tvAngsuran.setText(CurrencyUtils.rupiah(angsuran));
        tvTotalBayarPertama = findViewById(R.id.tv_dp);
        tvTotalBayarPertama.setText(CurrencyUtils.rupiah(dp));


        btn_back = findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }


}
