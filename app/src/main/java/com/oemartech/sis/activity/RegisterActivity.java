package com.oemartech.sis.activity;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.oemartech.sis.R;
import com.oemartech.sis.activity.admin.PegawaiActivity;
import com.oemartech.sis.activity.pembelian.AddMemberActivity;
import com.oemartech.sis.base.BaseActivity;
import com.oemartech.sis.model.GlobalVar;
import com.oemartech.sis.resful.SingletonRequestQueue;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RegisterActivity extends BaseActivity {

    TextView title;
    ImageView btn_add;
    ImageView btn_back;
    TextView txtDate;
    EditText etName,etPassword,etConfirmPassword;
    Spinner spRole;
    Button btn_save;
ProgressBar progressBar;
    String role,birthday;
    private int mYear, mMonth, mDay;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        title = findViewById(R.id.title);
        title.setText("Register");

        etName = findViewById(R.id.etName);
        etPassword = findViewById(R.id.etPassword);
        etConfirmPassword = findViewById(R.id.etConfirmPassword);
        txtDate =  findViewById(R.id.btn_date);
        txtDate.setText("- Choose Date -");
        txtDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             tanggal_mulai();
            }
        });

        final List<String> categories = new ArrayList<String>();
        categories.add("Owner");
        categories.add("Operator");
        categories.add("Marketing");

        spRole = findViewById(R.id.spRole);
        spRole.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (String.valueOf(adapterView.getItemAtPosition(i)).equals(categories.get(0))){
                    role = String.valueOf(1);
                }else if (String.valueOf(adapterView.getItemAtPosition(i)).equals(categories.get(0))) {
                    role = String.valueOf(2);
                }else {
                    role = String.valueOf(3);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spRole.setAdapter(dataAdapter);

        btn_add = findViewById(R.id.btn_add);
        btn_add.setVisibility(View.GONE);

        btn_back = findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        btn_save = findViewById(R.id.btn_save);
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validation();
            }
        });

        progressBar = findViewById(R.id.progressBar);
    }

    public void tanggal_mulai(){
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(RegisterActivity.this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        txtDate.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                        birthday = txtDate.getText().toString();

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    public void validation(){
        if (etName.getText().toString().isEmpty()){
            etName.setError("Please Entry Name");
        } else if (etPassword.getText().toString().isEmpty()){
            etPassword.setError("Please Entry Password");
        } else if (etConfirmPassword.getText().toString().isEmpty()){
            etConfirmPassword.setError("Please Entry Confirm Password");
        }else if (!etPassword.getText().toString().equals(etConfirmPassword.getText().toString())){
            Toast.makeText(this, "Password and Confirm Password doesn't match", Toast.LENGTH_SHORT).show();
        } else {
            register(etName.getText().toString(),etPassword.getText().toString(), role, birthday );
        }
    }

    public void register(final String name, final String password, final String role, final String birthday){
        RequestQueue queue = SingletonRequestQueue.getInstance(RegisterActivity.this).getRequestQueue();
        progressBar.setVisibility(View.VISIBLE);
        VolleyLog.DEBUG = true;
        String uri = "http://showroom.theidealstore.id/public/api/register";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                VolleyLog.wtf(response);
                progressBar.setVisibility(View.GONE);
                Toast.makeText(RegisterActivity.this, response, Toast.LENGTH_SHORT).show();
                ((PegawaiActivity) GlobalVar.getInstance().getActivity()).staff_request();
                finish();
            }
        }, errorListener) {
            @Override
            public Priority getPriority() {
                return Priority.LOW;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("name", name);
                params.put("password", password);
                params.put("role", role);
                params.put("birthday", birthday);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
                return headers;
            }

        };
        queue.add(stringRequest);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request request) {
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    Response.ErrorListener errorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            progressBar.setVisibility(View.GONE);
            if (error instanceof NetworkError) {
                Toast.makeText(RegisterActivity.this, "No network available", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(RegisterActivity.this, error.toString(), Toast.LENGTH_LONG).show();
            }
        }
    };
}
