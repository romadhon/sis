package com.oemartech.sis.activity.penjualan;

import android.app.Dialog;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.JsonObject;
import com.oemartech.sis.R;
import com.oemartech.sis.base.BaseActivity;
import com.oemartech.sis.fragment.AddCustimerFragment;
import com.oemartech.sis.model.GlobalVar;
import com.oemartech.sis.model.MobilDijual;
import com.oemartech.sis.resful.SingletonRequestQueue;
import com.oemartech.sis.utils.Constants;
import com.oemartech.sis.utils.CurrencyEditText;
import com.oemartech.sis.utils.CurrencyUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AddPurchaseActivity extends BaseActivity implements View.OnClickListener {
    public static final int action_purchase = 10002;
    public static final int action_purchase_result = 10003;
    LinearLayout bodyDetail,linearDp;
    TextView title;
    TextView tvCarName,tvYear;
    TextView tvName,tvPhone,tvAddress,tvPrice,tvTotal,tvDp;
    CurrencyEditText etDiscount;
    Button btnChooseCustomer,btnPurchase,btnDetail;
    ImageView btnBack, btnAdd;
    CheckBox cbIsCredit;
    EditText etSearchCustomer,etLeasing;
    ListView customerList;

    ProgressBar progressBar;
    AlertDialog dialog;
    LayoutInflater inflater;
    View dialogView;
    FragmentManager manager;
    ArrayList<String> customerName;
    ArrayList<Integer> customerId;
    String paramidcustomer;
    String paramiduser;
    String paramiddatamobil;
    double paramPrice;
    double paramTotal = 0;
    double dp = 0;
    int paramiscredit;
    int bookingStatus;
    int holdStatus;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_purchase);
        setView();
    }

    private void setView(){
        title = findViewById(R.id.title);
        bodyDetail = findViewById(R.id.bodydetail);
        linearDp = findViewById(R.id.linearDp);
        tvCarName = findViewById(R.id.tvCarName);
        tvYear = findViewById(R.id.tvYear);
        tvName = findViewById(R.id.tvName);
        tvPhone = findViewById(R.id.tvPhone);
        tvAddress = findViewById(R.id.tvAddress);
        tvPrice = findViewById(R.id.tvPrice);
        tvDp = findViewById(R.id.tvDp);
        tvTotal = findViewById(R.id.tvTotal);
        etDiscount = findViewById(R.id.etDiscount);
        etLeasing = findViewById(R.id.etLeasing);
        btnChooseCustomer = findViewById(R.id.btn_choose_customer);
        btnChooseCustomer.setOnClickListener(this);
        btnPurchase = findViewById(R.id.btn_purchase);
        btnPurchase.setOnClickListener(this);
        btnDetail = findViewById(R.id.btn_detail);
        btnDetail.setOnClickListener(this);
        cbIsCredit = findViewById(R.id.cb_iscredit);
        btnBack = findViewById(R.id.btn_back);
        btnBack.setOnClickListener(this);
        btnAdd = findViewById(R.id.btn_add);
        btnAdd.setVisibility(View.GONE);
        progressBar = findViewById(R.id.progressBar);

        title.setText("Purchase Order");
        if (getIntent().getExtras() !=null ){
            bookingStatus = getIntent().getIntExtra("booking",100);
            holdStatus = getIntent().getIntExtra("hold",200);
            paramiddatamobil = String.valueOf(getIntent().getExtras().getInt("id"));
            paramPrice = getIntent().getExtras().getDouble("price");
            tvCarName.setText(getIntent().getExtras().getString("name"));
            tvYear.setText(getIntent().getExtras().getString("year"));
            tvPrice.setText(CurrencyUtils.rupiah(getIntent().getExtras().getDouble("price")));
            tvTotal.setText(CurrencyUtils.rupiah(getIntent().getExtras().getDouble("price")));


            if (bookingStatus ==1){
                btnChooseCustomer.setVisibility(View.GONE);
                linearDp.setVisibility(View.VISIBLE);
                requestBooked(paramiddatamobil,getIntent().getExtras().getDouble("price"));
            }

        }

        etLeasing.setEnabled(false);
        cbIsCredit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((CheckBox) view).isChecked()) {
                    paramiscredit = 1;
                    etLeasing.setEnabled(true);
                } else {
                    paramiscredit = 0;
                    etLeasing.setText("");
                    etLeasing.setEnabled(false);
                }
            }
        });

        etDiscount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!etDiscount.getText().toString().isEmpty()){
                    String strDisc =  etDiscount.getText().toString().replace(",","");
                    if (!strDisc.isEmpty()){
                        if (bookingStatus == 1 ) {
                            paramTotal = (paramPrice - dp) - Double.parseDouble(strDisc);
                        }else {
                            if (!strDisc.trim().isEmpty()){
                                    paramTotal = paramPrice - Double.parseDouble(strDisc);
                                    tvTotal.setText(CurrencyUtils.rupiah( paramTotal) );
                            }
                        }

                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        customer_request(false);
    }

    @Override
    public void onClick(View view) {
         if (view == btnChooseCustomer) {
             setChooseCustomer();
        } else if (view == btnPurchase) {
             if (!etDiscount.getText().toString().isEmpty())
             paramTotal = paramPrice - Double.parseDouble(etDiscount.getText().toString().replace(",",""))- dp;

             if (null == paramidcustomer){
                 Toast.makeText(this, "Choose Customer Please", Toast.LENGTH_SHORT).show();
             }else if (paramiscredit == 1){
                 if (etLeasing.getText().toString().isEmpty()){
                     etLeasing.setError("Please Entry ! ");
                 }else{
                     requestPurchase();
                 }
             } else {
                 requestPurchase();
             }

        } else if (view == btnDetail) {
            car_detail();
        } else if (view == btnBack) {
            onBackPressed();
        }
    }

    public void car_detail(){
        final Dialog dialog = new Dialog(AddPurchaseActivity.this);
        dialog.setContentView(R.layout.car_sell_detail);
        dialog.setTitle("Car Detail");

        TextView tvNama = dialog.findViewById(R.id.tvNama);
        TextView tvPlatNomor = dialog.findViewById(R.id.tvPlatNomor);
        TextView tvWarna = dialog.findViewById(R.id.tvWarna);
        TextView tvHarga = dialog.findViewById(R.id.tvHarga);
        TextView tvTipe = dialog.findViewById(R.id.tvTipe);
        TextView tvTransmisi = dialog.findViewById(R.id.tv_transmisi);
        TextView tvTahun = dialog.findViewById(R.id.tvTahun);
        TextView tvKondisi = dialog.findViewById(R.id.tvKondisi);

        if (getIntent().getExtras()!=null){
            tvNama.setText(tvCarName.getText().toString());
            tvPlatNomor.setText(getIntent().getExtras().getString("plat"));
            tvWarna.setText(getIntent().getExtras().getString("color"));
            tvHarga.setText(tvPrice.getText().toString());
            tvTipe.setText(getIntent().getExtras().getString("type"));
            tvTransmisi.setText(getIntent().getExtras().getString("transmission"));
            tvTahun.setText(tvYear.getText().toString());
            tvKondisi.setText(getIntent().getExtras().getString("condition"));
        }


        Button dialogButton = dialog.findViewById(R.id.btn_simpan);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        Button btnBack = dialog.findViewById(R.id.btn_back);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void setChooseCustomer(){
        manager = getFragmentManager();
        android.app.Fragment frag = manager.findFragmentByTag("customer");
        if (frag != null) {
            manager.beginTransaction().remove(frag).commit();
        }

        // custom dialog
        dialog = new AlertDialog.Builder(AddPurchaseActivity.this).create();
        inflater = getLayoutInflater();
        dialogView = inflater.inflate(R.layout.choose_customer, null);
        dialog.setView(dialogView);
        dialog.setTitle("Customer List");

        etSearchCustomer    = dialogView.findViewById(R.id.etSearchName);

        Button btnAddNewCustomer = dialogView.findViewById(R.id.btn_add_customer);
        btnAddNewCustomer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddCustimerFragment addCustomer = new AddCustimerFragment();
                Bundle bundle = new Bundle();
                bundle.putBoolean("iscreate",true);
                bundle.putString("from","buy");
                addCustomer.setArguments(bundle);
                addCustomer.show(manager, "customer");
                dialog.dismiss();
            }
        });

        etSearchCustomer.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        customerList    = dialogView.findViewById(R.id.customerList);

        final StableArrayAdapter adapter = new StableArrayAdapter(this,
                android.R.layout.simple_list_item_1, customerName);
        customerList.setAdapter(adapter);
        customerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, final View v, int i, long l) {
                requestCustomerDetail(String.valueOf(customerId.get(i)),true);

            }
        });

        dialog.show();
    }

    public void customer_request(final boolean isNewCustomer){
        RequestQueue queue = SingletonRequestQueue.getInstance(AddPurchaseActivity.this).getRequestQueue();
        progressBar.setVisibility(View.VISIBLE);
        btnChooseCustomer.setEnabled(false);
        VolleyLog.DEBUG = true;
        String uri = Constants.IP+"customers";

        StringRequest stringRequestPOSTJSON = new StringRequest(Request.Method.GET, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressBar.setVisibility(View.GONE);
                btnChooseCustomer.setEnabled(true);
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    customerName = new ArrayList<>();
                    customerId = new ArrayList<>();
                    for (int i = 0; i< jsonArray.length(); i++){
                        customerName.add(jsonArray.getJSONObject(i).getString("namacust"));
                        customerId.add(jsonArray.getJSONObject(i).getInt("idcust"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (isNewCustomer)
                    setChooseCustomer();

            }
        }, errorListener) {
            @Override
            public Request.Priority getPriority() {
                return Request.Priority.HIGH;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", Constants.CONTENT_TYPE);
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                String requestBody = null;


                return requestBody == null ? null : requestBody.getBytes(StandardCharsets.UTF_8);
            }
        };
        queue.add(stringRequestPOSTJSON);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request request) {
                progressBar.setVisibility(View.GONE);
                btnChooseCustomer.setEnabled(true);
            }
        });
    }

    private void requestCustomerDetail(final String idcustomer, final boolean useDialog) {

        RequestQueue queue = SingletonRequestQueue.getInstance(AddPurchaseActivity.this).getRequestQueue();

        VolleyLog.DEBUG = true;
        String uriCustomer = Constants.IP+"getcustid/"+idcustomer;

        StringRequest customer = new StringRequest(Request.Method.GET, uriCustomer, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (useDialog)
                dialog.dismiss();

                VolleyLog.wtf(response);
                bodyDetail.setVisibility(View.VISIBLE);
                btnPurchase.setEnabled(true);
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    paramidcustomer = jsonArray.getJSONObject(0).getString("idcust");
                    paramiduser = paramidcustomer;
                    tvName.setText(jsonArray.getJSONObject(0).getString("namacust"));
                    tvPhone.setText(jsonArray.getJSONObject(0).getString("telpcust"));
                    tvAddress.setText(jsonArray.getJSONObject(0).getString("alamatcust"));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, errorListener) {
            @Override
            public Priority getPriority() {
                return Priority.LOW;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
                return headers;
            }

        };
        queue.add(customer);

        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request request) {
                if (useDialog)
                dialog.dismiss();
                btnPurchase.setEnabled(true);
            }
        });
    }

    private void requestPurchase() {

        RequestQueue queue = SingletonRequestQueue.getInstance(AddPurchaseActivity.this).getRequestQueue();

        VolleyLog.DEBUG = true;
        String uriPurchase = Constants.IP+"penjualan";

        final StringRequest buy = new StringRequest(Request.Method.POST, uriPurchase, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                VolleyLog.wtf(response);
                Toast.makeText(AddPurchaseActivity.this, response, Toast.LENGTH_SHORT).show();
                Intent returnIntent = new Intent();
                setResult(action_purchase_result, returnIntent);
                finish();
            }
        }, errorListener) {
            @Override
            public Priority getPriority() {
                return Priority.LOW;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                String strDisc =  etDiscount.getText().toString().replace(",","");
                params.put("iddatamobil", paramiddatamobil);
                params.put("iduser", GlobalVar.getInstance().getId());
                params.put("idshowroom", "1");
                params.put("idcustomer", paramidcustomer);
                params.put("hargaawal", String.format ("%.0f", paramPrice));
                params.put("potongan", strDisc);
                params.put("hargaakhir", String.format ("%.0f", paramTotal));
                params.put("iskredit", String.valueOf(paramiscredit));
                params.put("nama_leasing", etLeasing.getText().toString());

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
                return headers;
            }

        };

        queue.add(buy);


        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request request) {
                System.out.println("PARAM : "+request);
            }
        });
    }

    private void requestBooked(final String iddatamobil, final double price) {

        RequestQueue queue = SingletonRequestQueue.getInstance(AddPurchaseActivity.this).getRequestQueue();

        VolleyLog.DEBUG = true;
        String uriBooking = "http://showroom.theidealstore.id/public/api/selectbooked";

        final StringRequest booking = new StringRequest(Request.Method.POST, uriBooking, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                VolleyLog.wtf(response);
                try {
                    JSONObject jsonBooked = new JSONObject(response);
                    dp = jsonBooked.getDouble("dp");
                    tvDp.setText(CurrencyUtils.rupiah(dp));
                    tvTotal.setText(CurrencyUtils.rupiah(price - dp));
                    requestCustomerDetail(jsonBooked.getString("idcustomer"),false);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, errorListener) {
            @Override
            public Priority getPriority() {
                return Priority.LOW;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                params.put("iddatamobil", iddatamobil);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
                return headers;
            }

        };

        queue.add(booking);


        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request request) {
                System.out.println("PARAM : "+request);
            }
        });
    }

    Response.ErrorListener errorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            if (error instanceof NetworkError) {
                Toast.makeText(AddPurchaseActivity.this, "No network available", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(AddPurchaseActivity.this, error.toString(), Toast.LENGTH_LONG).show();
            }
        }
    };

    private class StableArrayAdapter extends ArrayAdapter<String> {

        HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();

        public StableArrayAdapter(Context context, int textViewResourceId,
                                  List<String> objects) {
            super(context, textViewResourceId, objects);
            for (int i = 0; i < objects.size(); ++i) {
                mIdMap.put(objects.get(i), i);
            }
        }

        @Override
        public long getItemId(int position) {
            String item = getItem(position);
            return mIdMap.get(item);
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

    }

    public void refreshCustomerList(){
        customer_request(true);
    }
}
