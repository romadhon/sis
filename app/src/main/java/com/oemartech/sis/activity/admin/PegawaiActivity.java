package com.oemartech.sis.activity.admin;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.oemartech.sis.MainActivity;
import com.oemartech.sis.R;
import com.oemartech.sis.activity.RegisterActivity;
import com.oemartech.sis.adapter.StaffAdapter;
import com.oemartech.sis.base.BaseActivity;
import com.oemartech.sis.model.GlobalVar;
import com.oemartech.sis.model.Staff;
import com.oemartech.sis.resful.SingletonRequestQueue;
import com.oemartech.sis.utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Sevima on 9/28/2018.
 */

public class PegawaiActivity extends BaseActivity {
    ImageView btn_back;
    TextView title;
    ImageView btn_add;
    RecyclerView recyclerView;
    Context context;
    StaffAdapter staffAdapter;
    ArrayList<Staff> staffs = new ArrayList<>();
    String role;
    String paramRole;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_member);
        GlobalVar.getInstance().setActivity(PegawaiActivity.this);
        context = this;
        title = (TextView) findViewById(R.id.title);
        title.setText("Users");

        btn_add = (ImageView) findViewById(R.id.btn_add);
        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                add_pegawai();
                Intent intent = new Intent(PegawaiActivity.this, RegisterActivity.class);
                startActivity(intent);
            }

        });

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        staff_request();

        btn_back = (ImageView) findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    public void MemberList(){
        staffAdapter = new StaffAdapter(staffs, this, new StaffAdapter.StaffAdapterListener() {
            @Override
            public void onDeleteClick(int id) {

            }

            @Override
            public void onEditClick(int id, String nama, String password, String role) {

            }
        });
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(staffAdapter);
    }

    public void add_pegawai(){
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.entry_pegawai);
        dialog.setTitle("Tambah Pegawai");

        final EditText etnama = (EditText) dialog.findViewById(R.id.etNama);
        final EditText etpassword = (EditText) dialog.findViewById(R.id.etPassword);
        final Spinner spRole = (Spinner) dialog.findViewById(R.id.sRole);
        final String[] warna={"Marketing","Admin","PO","SO"};
        final ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, warna);
        spRole.setAdapter(adapter);
        spRole.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (adapter.getItem(i).equals("Marketing")){
                    role = "1";
                }else if(adapter.getItem(i).equals("Admin")){
                    role = "2";
                }else if(adapter.getItem(i).equals("PO")){
                    role = "3";
                }else if(adapter.getItem(i).equals("SO")){
                    role = "4";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        Button dialogButton = (Button) dialog.findViewById(R.id.btn_simpan);
        // if button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                add_pegawai_request(etnama.getText().toString(),etpassword.getText().toString(),role);
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void staff_request(){
        RequestQueue queue = SingletonRequestQueue.getInstance(context).getRequestQueue();

        VolleyLog.DEBUG = true;
        String uri = "http://showroom.theidealstore.id/public/api/users";

        StringRequest stringRequestPOSTJSON = new StringRequest(Request.Method.GET, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                staffs =  new ArrayList<>();
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0; i< jsonArray.length(); i++){
                        int id = jsonArray.getJSONObject(i).getInt("id");
                        String name = jsonArray.getJSONObject(i).getString("name");
                        String pass = jsonArray.getJSONObject(i).getString("password");
                        String bagian = jsonArray.getJSONObject(i).getString("role");
                        staffs.add(new Staff(id, name, pass,bagian));
                    }
                    MemberList();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("RESPONSE : "+response);
            }
        }, errorListener) {
            @Override
            public Priority getPriority() {
                return Priority.HIGH;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", Constants.CONTENT_TYPE);
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                String requestBody = null;


                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        queue.add(stringRequestPOSTJSON);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request request) {
            }
        });
    }


    public void add_pegawai_request(final String nama, final String password, final String role){
        RequestQueue queue = SingletonRequestQueue.getInstance(context).getRequestQueue();

        VolleyLog.DEBUG = true;
        String uri = "http://showroom.theidealstore.id/public/api/register";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                VolleyLog.wtf(response);
                staff_request();
            }
        }, errorListener) {
            @Override
            public Priority getPriority() {
                return Priority.LOW;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                params.put("name", nama);
                params.put("password", password);
                params.put("role", role);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
                return headers;
            }

        };
        queue.add(stringRequest);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request request) {
            }
        });
    }

    Response.ErrorListener errorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            if (error instanceof NetworkError) {
                Toast.makeText(context, "No network available", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(context, error.toString(), Toast.LENGTH_LONG).show();
            }
        }
    };
}
