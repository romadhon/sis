package com.oemartech.sis.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.github.piasy.biv.BigImageViewer;
import com.github.piasy.biv.loader.glide.GlideCustomImageLoader;
import com.github.piasy.biv.view.BigImageView;
import com.oemartech.sis.R;
import com.oemartech.sis.adapter.GalleryAdapter;
import com.oemartech.sis.base.BaseActivity;
import com.oemartech.sis.model.SampleCustomImageSizeModel;
import com.oemartech.sis.utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class GalleryActivity extends BaseActivity {
    String url;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BigImageViewer.initialize(GlideCustomImageLoader.with(getApplicationContext()));

        setContentView(R.layout.activity_gallery);
        if (getIntent().getExtras()!=null){

            url = getIntent().getExtras().getString("url");
            Log.v("ULR : ",url);
        }
        configRecycler();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        BigImageViewer.imageLoader().cancelAll();
    }

    private void configRecycler() {
        final List<String> imageUrlsList = new ArrayList<>();
        try {
            JSONArray jsonUrl = new JSONArray(url);
            for (int i = 0; i<jsonUrl.length();i++){
                imageUrlsList.add(Constants.ASSETS+jsonUrl.getString(i));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RecyclerView recycler = findViewById(R.id.recycler_view);
        recycler.setLayoutManager(
                new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        recycler.setAdapter(new GalleryAdapter(imageUrlsList));

        LinearSnapHelper snapHelper = new LinearSnapHelper();
        snapHelper.attachToRecyclerView(recycler);
    }
}
