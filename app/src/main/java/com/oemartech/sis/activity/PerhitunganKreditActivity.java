package com.oemartech.sis.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.oemartech.sis.R;
import com.oemartech.sis.model.SettingCreditCount;
import com.oemartech.sis.utils.CurrencyEditText;
import com.oemartech.sis.utils.CurrencyUtils;
import com.oemartech.sis.utils.SharedPreferenceUtils;

import java.lang.ref.WeakReference;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Set;

/**
 * Created by Sevima on 9/28/2018.
 */

public class  PerhitunganKreditActivity extends AppCompatActivity {

    public static int setProvisi = 5;
    public static int setBunga = 30;
    public static float setAsuransi = (float) 1.5;
    public static float setCreditProtection = (float) 1.5;
    public static String adminCost = "150,000";

    public static int tenor1Tahun = 12;
    public static int tenor2Tahun = 24;
    public static int tenor3Tahun = 36;
    public static int tenor4Tahun = 48;
    public static int tenor5Tahun = 60;


    CurrencyEditText etOTR;
    EditText etTDP;
    Button btnSimulasi;

    LinearLayout ll_resutl_simulation;
    RelativeLayout rl_1year;
    RelativeLayout rl_2year;
    RelativeLayout rl_3year;
    RelativeLayout rl_4year;
    RelativeLayout rl_5year;

    TextView tv_nominal_dp_1tahun;
    TextView tv_nominal_dp_2tahun;
    TextView tv_nominal_dp_3tahun;
    TextView tv_nominal_dp_4tahun;
    TextView tv_nominal_dp_5tahun;

    TextView tv_nominal_angsuran_1tahun;
    TextView tv_nominal_angsuran_2tahun;
    TextView tv_nominal_angsuran_3tahun;
    TextView tv_nominal_angsuran_4tahun;
    TextView tv_nominal_angsuran_5tahun;

    private  ImageView back,btn_add;
    private TextView title;
    private String current = "";
    private WeakReference<EditText> editTextWeakReference;
    double tdpPersen;
    double otr;
    double tdp;

    double ph_murni;
    double provisi;
    double totalHutang;
    double angsuran;
    double pembulatanAngsuran;
    double asuransi;
    double creditProtection;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perhitungan_kredit);

        SettingCreditCount settingCreditCount = SharedPreferenceUtils.getCountCreditSetting(this);
        if (settingCreditCount.getProvisi()==0 && settingCreditCount.getBunga()==0 && settingCreditCount.getAsuransi()==0.0 &&
                settingCreditCount.getCredit_protection()==0.0 &&settingCreditCount.getAdmin_cost().equals("0")){
            SharedPreferenceUtils.seveCountCreditSetting(this,
                    new SettingCreditCount(
                            setProvisi,
                            setBunga,
                            setAsuransi,
                            setCreditProtection,
                            adminCost));
        }


        title = (TextView) findViewById(R.id.title);
        title.setText("Simulasi Kredit");

        etOTR = findViewById(R.id.et_otr);
        etTDP = findViewById(R.id.et_tdp);
        ll_resutl_simulation = findViewById(R.id.ll_resutl_simulation);

        btnSimulasi = findViewById(R.id.btn_simulation);
        btnSimulasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (etOTR.getText().toString().isEmpty()){
                    Toast.makeText(PerhitunganKreditActivity.this, "Silakan masukkan Harga Mobil", Toast.LENGTH_SHORT).show();
                } else if (etTDP.getText().toString().isEmpty()){
                    Toast.makeText(PerhitunganKreditActivity.this, "Silakan masukkan TDP", Toast.LENGTH_SHORT).show();
                } else {
                    generateSimulation();
                }

            }
        });

        back = (ImageView) findViewById(R.id.btn_back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        btn_add = (ImageView) findViewById(R.id.btn_add);
        btn_add.setImageDrawable(getResources().getDrawable(R.drawable.ic_settings_white_24dp));
        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PerhitunganKreditActivity.this, PengaturanPerhitunganKreditActivity.class);
                startActivity(intent);
            }
        });

    }


    private void generateSimulation(){
        SettingCreditCount settingCreditCount = SharedPreferenceUtils.getCountCreditSetting(this);
        String strOtr =  etOTR.getText().toString().replace(",","");

        otr = Double.parseDouble(strOtr);
        tdpPersen = Double.parseDouble(etTDP.getText().toString());
        tdp = (otr*tdpPersen)/100;

        ph_murni = otr - tdp;
        provisi = (ph_murni * settingCreditCount.getProvisi()) / 100;
        totalHutang = ph_murni + provisi;

        asuransi = (settingCreditCount.getAsuransi()*totalHutang)/100;
        creditProtection = (settingCreditCount.getCredit_protection()*totalHutang)/100;
        ll_resutl_simulation.setVisibility(View.VISIBLE);

        setTenor1Tahun(settingCreditCount);
        setTenor2Tahun(settingCreditCount);
        setTenor3Tahun(settingCreditCount);
        setTenor4Tahun(settingCreditCount);
        setTenor5Tahun(settingCreditCount);
    }


    private void setTenor1Tahun(SettingCreditCount settingCreditCount){
        String strAC =  settingCreditCount.getAdmin_cost().replace(",","");
        final double angsuran1tahun = (((totalHutang * settingCreditCount.getBunga()) / 100)+ totalHutang) / tenor1Tahun;
        final double totalPembayaranPertama = tdp+angsuran1tahun;
        tv_nominal_dp_1tahun = findViewById(R.id.tv_nominal_dp_1tahun);
        tv_nominal_angsuran_1tahun = findViewById(R.id.tv_nominal_angsuran_1tahun);
        tv_nominal_dp_1tahun.setText(CurrencyUtils.rupiah(totalPembayaranPertama));
        tv_nominal_angsuran_1tahun.setText(CurrencyUtils.rupiah(angsuran1tahun));

        rl_1year = findViewById(R.id.rl_1year);
        rl_1year.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PerhitunganKreditActivity.this, DetailSimulationActivity.class);
                intent.putExtra("otr", otr);
                intent.putExtra("tdp", tdp);
                intent.putExtra("phmurni", ph_murni);
                intent.putExtra("provisi", provisi);
                intent.putExtra("total_hutang", totalHutang);
                intent.putExtra("tenor", tenor1Tahun);
                intent.putExtra("bunga", tdpPersen);
                intent.putExtra("angsuran", angsuran1tahun);
                intent.putExtra("dp", totalPembayaranPertama);
                startActivity(intent);
            }
        });
    }

    private void setTenor2Tahun(SettingCreditCount settingCreditCount){
        String strAC =  settingCreditCount.getAdmin_cost().replace(",","");
        final double angsuran2tahun = (((totalHutang * settingCreditCount.getBunga()) / 100)+ totalHutang) / tenor2Tahun;
        final double totalPembayaranPertama = tdp+angsuran2tahun;
        tv_nominal_dp_2tahun = findViewById(R.id.tv_nominal_dp_2tahun);
        tv_nominal_angsuran_2tahun = findViewById(R.id.tv_nominal_angsuran_2tahun);
        tv_nominal_dp_2tahun.setText(CurrencyUtils.rupiah(totalPembayaranPertama));
        tv_nominal_angsuran_2tahun.setText(CurrencyUtils.rupiah(angsuran2tahun));

        rl_2year = findViewById(R.id.rl_2year);
        rl_2year.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PerhitunganKreditActivity.this, DetailSimulationActivity.class);
                intent.putExtra("otr", otr);
                intent.putExtra("tdp", tdp);
                intent.putExtra("phmurni", ph_murni);
                intent.putExtra("provisi", provisi);
                intent.putExtra("total_hutang", totalHutang);
                intent.putExtra("tenor", tenor2Tahun);
                intent.putExtra("bunga", tdpPersen);
                intent.putExtra("angsuran", angsuran2tahun);
                intent.putExtra("dp", totalPembayaranPertama);
                startActivity(intent);
            }
        });
    }

    private void setTenor3Tahun(SettingCreditCount settingCreditCount){
        String strAC =  settingCreditCount.getAdmin_cost().replace(",","");
        final double angsuran3tahun = (((totalHutang * settingCreditCount.getBunga()) / 100)+ totalHutang) / tenor3Tahun;
        final double totalPembayaranPertama = tdp+angsuran3tahun;
        tv_nominal_dp_3tahun = findViewById(R.id.tv_nominal_dp_3tahun);
        tv_nominal_angsuran_3tahun = findViewById(R.id.tv_nominal_angsuran_3tahun);
        tv_nominal_dp_3tahun.setText(CurrencyUtils.rupiah(totalPembayaranPertama));
        tv_nominal_angsuran_3tahun.setText(CurrencyUtils.rupiah(angsuran3tahun));

        rl_3year = findViewById(R.id.rl_3year);
        rl_3year.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PerhitunganKreditActivity.this, DetailSimulationActivity.class);
                intent.putExtra("otr", otr);
                intent.putExtra("tdp", tdp);
                intent.putExtra("phmurni", ph_murni);
                intent.putExtra("provisi", provisi);
                intent.putExtra("total_hutang", totalHutang);
                intent.putExtra("tenor", tenor3Tahun);
                intent.putExtra("bunga", tdpPersen);
                intent.putExtra("angsuran", angsuran3tahun);
                intent.putExtra("dp", totalPembayaranPertama);
                startActivity(intent);
            }
        });
    }

    private void setTenor4Tahun(SettingCreditCount settingCreditCount){
        String strAC =  settingCreditCount.getAdmin_cost().replace(",","");
        final double angsuran4tahun = (((totalHutang * settingCreditCount.getBunga()) / 100)+ totalHutang) / tenor4Tahun;
        final double totalPembayaranPertama = tdp+angsuran4tahun;
        tv_nominal_dp_4tahun = findViewById(R.id.tv_nominal_dp_4tahun);
        tv_nominal_angsuran_4tahun = findViewById(R.id.tv_nominal_angsuran_4tahun);
        tv_nominal_dp_4tahun.setText(CurrencyUtils.rupiah(totalPembayaranPertama));
        tv_nominal_angsuran_4tahun.setText(CurrencyUtils.rupiah(angsuran4tahun));

        rl_4year = findViewById(R.id.rl_4year);
        rl_4year.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PerhitunganKreditActivity.this, DetailSimulationActivity.class);
                intent.putExtra("otr", otr);
                intent.putExtra("tdp", tdp);
                intent.putExtra("phmurni", ph_murni);
                intent.putExtra("provisi", provisi);
                intent.putExtra("total_hutang", totalHutang);
                intent.putExtra("tenor", tenor4Tahun);
                intent.putExtra("bunga", tdpPersen);
                intent.putExtra("angsuran", angsuran4tahun);
                intent.putExtra("dp", totalPembayaranPertama);
                startActivity(intent);
            }
        });
    }

    private void setTenor5Tahun(SettingCreditCount settingCreditCount){
        String strAC =  settingCreditCount.getAdmin_cost().replace(",","");
        final double angsuran5tahun = (((totalHutang * settingCreditCount.getBunga()) / 100)+ totalHutang) / tenor5Tahun;
        final double totalPembayaranPertama = tdp+angsuran5tahun;
        tv_nominal_dp_5tahun = findViewById(R.id.tv_nominal_dp_5tahun);
        tv_nominal_angsuran_5tahun = findViewById(R.id.tv_nominal_angsuran_5tahun);
        tv_nominal_dp_5tahun.setText(CurrencyUtils.rupiah(totalPembayaranPertama));
        tv_nominal_angsuran_5tahun.setText(CurrencyUtils.rupiah(angsuran5tahun));

        rl_5year = findViewById(R.id.rl_5year);
        rl_5year.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PerhitunganKreditActivity.this, DetailSimulationActivity.class);
                intent.putExtra("otr", otr);
                intent.putExtra("tdp", tdp);
                intent.putExtra("phmurni", ph_murni);
                intent.putExtra("provisi", provisi);
                intent.putExtra("total_hutang", totalHutang);
                intent.putExtra("tenor", tenor5Tahun);
                intent.putExtra("bunga", tdpPersen);
                intent.putExtra("angsuran", angsuran5tahun);
                intent.putExtra("dp", totalPembayaranPertama);
                startActivity(intent);
            }
        });
    }

}
