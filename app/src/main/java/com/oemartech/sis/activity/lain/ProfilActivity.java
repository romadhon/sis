package com.oemartech.sis.activity.lain;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.oemartech.sis.MainActivity;
import com.oemartech.sis.R;
import com.oemartech.sis.activity.LoginActivity;
import com.oemartech.sis.base.BaseActivity;
import com.oemartech.sis.helper.Images;
import com.oemartech.sis.model.GlobalVar;
import com.oemartech.sis.resful.SingletonRequestQueue;
import com.oemartech.sis.resful.VolleyMultipartRequest;
import com.oemartech.sis.utils.Constants;
import com.oemartech.sis.utils.ValidationUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileDescriptor;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProfilActivity extends BaseActivity implements View.OnClickListener {
    private static final int SELECT_PICTURE = 1;
    TextView title,tvNama,tvEmail,tvAlamat,tvHp;
    EditText etNama,etEmail,etAlamat,etHp;
    ImageView imgProfile;
    ImageView btnBack,btnEdit,btnChooseImg;
    Button btn_simpan, btn_register;
    ProgressDialog pDialog;
    boolean isEdit,isChangePicture = false;
    private Bitmap bitmap;
    String imgDecodableString;
    String selectedImagePath;
    private RequestQueue rQueue;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        setView();
    }

    public void setView(){
        title = findViewById(R.id.title);

        tvNama = findViewById(R.id.tvNama);
        tvEmail = findViewById(R.id.tvEmail);
        tvAlamat = findViewById(R.id.tvAlamat);
        tvHp = findViewById(R.id.tvHp);

        requestMultiplePermissions();
        imgProfile = findViewById(R.id.imgProfile);
//        Glide.with(ProfilActivity.this).load(Constants.ASSETS+GlobalVar.getInstance().getId()+".png").placeholder(getResources().getDrawable(R.drawable.ic_account_circle_black_24dp)).into(imgProfile);
        Glide.with(ProfilActivity.this)
                .load(Constants.ASSETS+GlobalVar.getInstance().getImageUrl())
                .centerCrop()
                .placeholder(R.drawable.ic_photo_black_24dp)
                .into(imgProfile);
        tvNama.setText(GlobalVar.getInstance().getNama().equals("null")?"-":GlobalVar.getInstance().getNama());
        tvEmail.setText(GlobalVar.getInstance().getEmail().equals("null")?"-":GlobalVar.getInstance().getEmail());
        tvAlamat.setText(GlobalVar.getInstance().getAlamat().equals("null")?"-":GlobalVar.getInstance().getAlamat());
        tvHp.setText(GlobalVar.getInstance().getNohp().equals("null")?"-":GlobalVar.getInstance().getNohp());


        etNama = findViewById(R.id.etNama);
        etEmail = findViewById(R.id.etEmail);
        etAlamat = findViewById(R.id.etAlamat);
        etHp = findViewById(R.id.etHp);

        btnBack = findViewById(R.id.btn_back);
        btnBack.setOnClickListener(this);
        btnEdit = findViewById(R.id.btn_add);
        btnEdit.setOnClickListener(this);
        btnEdit.setImageDrawable(getResources().getDrawable(R.drawable.ic_edit_black_24dp));
        btnChooseImg = findViewById(R.id.choose_img);
        btnChooseImg.setOnClickListener(this);
        btn_simpan = findViewById(R.id.btn_simpan);
        btn_simpan.setOnClickListener(this);

        title.setText("Profil");
        goneEt();
        visibileTv();
        btn_simpan.setVisibility(View.GONE);
    }

    public void goneTv(){
        tvNama.setVisibility(View.GONE);
        tvEmail.setVisibility(View.GONE);
        tvHp.setVisibility(View.GONE);
        tvAlamat.setVisibility(View.GONE);
    }

    public void goneEt(){
        btnChooseImg.setVisibility(View.GONE);
        etNama.setVisibility(View.GONE);
        etEmail.setVisibility(View.GONE);
        etHp.setVisibility(View.GONE);
        etAlamat.setVisibility(View.GONE);
    }

    public void visibileTv(){
        tvNama.setVisibility(View.VISIBLE);
        tvEmail.setVisibility(View.VISIBLE);
        tvHp.setVisibility(View.VISIBLE);
        tvAlamat.setVisibility(View.VISIBLE);
    }

    public void visibileEt(){
        btnChooseImg.setVisibility(View.VISIBLE);
        etNama.setVisibility(View.VISIBLE);
        etEmail.setVisibility(View.VISIBLE);
        etHp.setVisibility(View.VISIBLE);
        etAlamat.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View view) {
        if (btnBack == view.findViewById(R.id.btn_back)){
            onBackPressed();
        } else if (btnEdit == view.findViewById(R.id.btn_add)){
          Intent intent = new Intent(ProfilActivity.this, EditProfileActivity.class);
          intent.putExtra("nama",tvNama.getText());
          intent.putExtra("email",tvEmail.getText());
          intent.putExtra("hp",tvHp.getText());
          intent.putExtra("alamat",tvAlamat.getText());
          startActivity(intent);
        }else if (btn_simpan == view.findViewById(R.id.btn_simpan)){

            isEdit = false;
            updateProfileRequest();
            if (isChangePicture)
                uploadImage(bitmap);
        }else if (view == btnChooseImg){
            isChangePicture = true;
            Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//            galleryIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
            startActivityForResult(galleryIntent, 1);
        }
    }

    private void updateProfileRequest() {
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        pDialog.setMessage("wait ...");
        showDialog();

        RequestQueue queue = SingletonRequestQueue.getInstance(getApplicationContext()).getRequestQueue();

        VolleyLog.DEBUG = true;
        String uri = "http://showroom.theidealstore.id/public/api/updateuser";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(ProfilActivity.this, response, Toast.LENGTH_SHORT).show();
                tvNama.setText(etNama.getText().toString());
                tvEmail.setText(etEmail.getText().toString());
                tvHp.setText(etHp.getText().toString());
                tvAlamat.setText(etAlamat.getText().toString());
                goneEt();
                visibileTv();
                btn_simpan.setVisibility(View.GONE);
                GlobalVar.getInstance().setNama(etNama.getText().toString());
                GlobalVar.getInstance().setEmail(etEmail.getText().toString());
                GlobalVar.getInstance().setNohp(etHp.getText().toString());
                GlobalVar.getInstance().setAlamat(etAlamat.getText().toString());

            }
        }, errorListener) {
            @Override
            public Priority getPriority() {
                return Priority.LOW;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("name", etNama.getText().toString());
                params.put("password", GlobalVar.getInstance().getPassword());
                params.put("email", etEmail.getText().toString());
                params.put("nohp", etHp.getText().toString());
                params.put("alamat", etAlamat.getText().toString());
                params.put("role", GlobalVar.getInstance().getRole());
                params.put("id", GlobalVar.getInstance().getId());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
                return headers;
            }

        };


        queue.add(stringRequest);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request request) {
                hideDialog();
            }
        });
    }

    Response.ErrorListener errorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            hideDialog();
            if (error instanceof NetworkError) {
                Toast.makeText(getApplicationContext(), "No network available", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_LONG).show();
            }
        }
    };

    public String getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_PICTURE) {
                Uri selectedImageUri = data.getData();
                selectedImagePath = Images.getPath(selectedImageUri,ProfilActivity.this);
                try {
                    imgProfile.setImageBitmap(getBitmapFromUri(selectedImageUri));
                    bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImageUri);
                    uploadImage(bitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private Bitmap getBitmapFromUri(Uri uri) throws IOException {
        ParcelFileDescriptor parcelFileDescriptor =
                getContentResolver().openFileDescriptor(uri, "r");
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
        parcelFileDescriptor.close();
        return image;
    }


    private void uploadImage(final Bitmap bitmap){
        String uriUploadPhoto = "http://showroom.theidealstore.id/public/api/updateprofpict";
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, uriUploadPhoto,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        Log.d("ressssssoo",new String(response.data));
                        rQueue.getCache().clear();
                        btn_simpan.setVisibility(View.GONE);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * here we have only one parameter with the image
             * which is tags
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                 params.put("id", GlobalVar.getInstance().getId());
                return params;
            }

            /*
             *pass files using below method
             * */
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                long imagename = System.currentTimeMillis();
                params.put("file", new DataPart(imagename + ".png", getFileDataFromDrawable(bitmap)));
                return params;
            }
        };


        System.out.println("PARAM : "+volleyMultipartRequest.getPostBodyContentType());
        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        rQueue = Volley.newRequestQueue(ProfilActivity.this);
        rQueue.add(volleyMultipartRequest);
    }

    public byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 80, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    private void  requestMultiplePermissions(){
        Dexter.withActivity(this)
                .withPermissions(

                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            Toast.makeText(getApplicationContext(), "All permissions are granted by user!", Toast.LENGTH_SHORT).show();
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings

                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        Toast.makeText(getApplicationContext(), "Some Error! ", Toast.LENGTH_SHORT).show();
                    }
                })
                .onSameThread()
                .check();
    }


    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
