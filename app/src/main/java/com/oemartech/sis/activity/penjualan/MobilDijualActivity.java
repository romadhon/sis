package com.oemartech.sis.activity.penjualan;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.oemartech.sis.R;
import com.oemartech.sis.adapter.MobilDijualAdapter;
import com.oemartech.sis.base.BaseActivity;
import com.oemartech.sis.model.Customer;
import com.oemartech.sis.model.GlobalVar;
import com.oemartech.sis.model.MasterMobildijual;
import com.oemartech.sis.resful.SingletonRequestQueue;
import com.oemartech.sis.utils.Constants;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Sevima on 10/10/2018.
 */

public class MobilDijualActivity extends BaseActivity {
    private  RecyclerView recyclerView;
    private EditText etKeyword;
    private  ImageView back,btn_add, btn_search, btn_filter;
    private TextView title;
    ArrayList<MasterMobildijual> mobilDijuals = new ArrayList<>();
    ArrayList<Customer> customers = new ArrayList<>();
    private MobilDijualAdapter mobilDijualAdapter;
    FragmentManager manager;
    ProgressDialog pDialog;

    AlertDialog.Builder dialog;
    LayoutInflater inflater;
    View dialogView;
    SwipeRefreshLayout pullToRefresh;
    boolean isSearch =false;

    //
    ProgressBar progressBarRating;
    AlertDialog show;
    String strRating;
    String strCustomerId;
    String strIdDataMobil;
    Spinner sp_customer;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobil_dijual);
        title = (TextView) findViewById(R.id.title);
        title.setText("Mobil Dijual");

        pullToRefresh = (SwipeRefreshLayout) findViewById(R.id.pullRefresh);
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getCars();
                pullToRefresh.setRefreshing(false);
            }
        });

        GlobalVar.getInstance().setActivity(MobilDijualActivity.this);

        manager = getFragmentManager();
        android.app.Fragment frag = manager.findFragmentByTag("car");
        if (frag != null) {
            manager.beginTransaction().remove(frag).commit();
        }


        recyclerView = (RecyclerView) findViewById(R.id.rvCar);
        back = (ImageView) findViewById(R.id.btn_back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        btn_filter = findViewById(R.id.btn_filter);
        btn_filter.setVisibility(View.VISIBLE);
        btn_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MobilDijualActivity.this, FilterMobilDijualActivity.class);
                startActivityForResult(intent, FilterMobilDijualActivity.ACTION_FILTER);
            }
        });

        btn_search = findViewById(R.id.btn_search);
        btn_search.setVisibility(View.VISIBLE);
        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isSearch){
                    title.setVisibility(View.VISIBLE);
                    etKeyword.setVisibility(View.INVISIBLE);
                    isSearch = false;
                }else{
                    title.setVisibility(View.GONE);
                    etKeyword.setVisibility(View.VISIBLE);
                    etKeyword.setText("");
                    etKeyword.setFocusable(true);
                    isSearch = true;
                }

            }
        });

        btn_add = (ImageView) findViewById(R.id.btn_add);
        btn_add.setVisibility(View.GONE);

        getCars();
        customer_request();
    }

    public void CarList(){

        mobilDijualAdapter = new MobilDijualAdapter(mobilDijuals, getBaseContext(), new MobilDijualAdapter.CarAdapterListener() {
            @Override
            public void onBuyClick(MasterMobildijual mobilDijual) {
//                car_detail(mobilDijual);
                Intent intent = new Intent(MobilDijualActivity.this, AddPurchaseActivity.class);
                intent.putExtra("id",mobilDijual.getId_datamobil());
                intent.putExtra("idjenismobil",mobilDijual.getId_jenismobil());
                intent.putExtra("idpabrikan",mobilDijual.getId_pabrikan());
                intent.putExtra("idvarian",mobilDijual.getId_varian());
                intent.putExtra("idshowroom",mobilDijual.getIdshowroom());
                intent.putExtra("name", mobilDijual.getNama_Pabrikan());
                intent.putExtra("year", mobilDijual.getTahun());
                intent.putExtra("price", mobilDijual.getHargajual());
                intent.putExtra("plat", mobilDijual.getPlatnomor());
                intent.putExtra("color", mobilDijual.getWarna());
                intent.putExtra("condition", mobilDijual.getKondisi());
                intent.putExtra("type", mobilDijual.getNama_varian());//hilang
                intent.putExtra("transmission", "");//hilang
                intent.putExtra("booking", mobilDijual.getStatus_booking());
                intent.putExtra("hold", mobilDijual.getStatus_hold());
                startActivityForResult(intent, AddPurchaseActivity.action_purchase);
            }

            @Override
            public void onBookingClick(final MasterMobildijual mobilDijual) {
//                requestBooking("booking",String.valueOf(mobilDijual.getId()),String.valueOf(mobilDijual.getId()),"");
                if (mobilDijual.getStatus_booking() == 1){
                    //unbooking

                    dialog = new AlertDialog.Builder(MobilDijualActivity.this);
                    inflater = getLayoutInflater();
                    dialogView = inflater.inflate(R.layout.unbooking_dialog, null);
                    dialog.setView(dialogView);
                    dialog.setCancelable(true);
                    final CheckBox checkBox  = (CheckBox) dialogView.findViewById(R.id.cbDp);

                    Button btn_save  = (Button) dialogView.findViewById(R.id.btn_ok);
                    btn_save.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (checkBox.isChecked())
                                requestUnBooking(String.valueOf(mobilDijual.getId_datamobil()),"1");
                            else
                                requestUnBooking(String.valueOf(mobilDijual.getId_datamobil()),"0");

                        }
                    });

                    Button btn_cancel  = (Button) dialogView.findViewById(R.id.btn_cancel);
                    btn_cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            show.dismiss();
                        }
                    });

                    show = dialog.show();

                } else {
                    Intent intent = new Intent(MobilDijualActivity.this, AddBookingActivity.class);
                    intent.putExtra("id",mobilDijual.getId_datamobil());
                    intent.putExtra("name", mobilDijual.getNama_Pabrikan());
                    intent.putExtra("year", mobilDijual.getTahun());
                    intent.putExtra("price", mobilDijual.getHargajual());
                    startActivityForResult(intent, AddBookingActivity.action_booking);
                }

            }

            @Override
            public void onHoldClick(final MasterMobildijual mobilDijual) {
//                requestBooking("hold",String.valueOf(mobilDijual.getId()),String.valueOf(mobilDijual.getId()),"");
                if (mobilDijual.getStatus_hold() == 1){
                 //unhold
                    new AlertDialog.Builder(MobilDijualActivity.this)
                            .setTitle("Confirmation")
                            .setMessage("Do you really want to Unhold this car?")
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int whichButton) {
                                    requestUnHold(mobilDijual.getId_datamobil());
                                }})

                            .setNegativeButton(android.R.string.no, null).show();
                } else {
                    Intent intent = new Intent(MobilDijualActivity.this, AddHoldActivity.class);
                    intent.putExtra("id",mobilDijual.getId_datamobil());
                    intent.putExtra("name", mobilDijual.getNama_Pabrikan());
                    intent.putExtra("year", mobilDijual.getTahun());
                    intent.putExtra("price", mobilDijual.getHargajual());
                    startActivityForResult(intent, AddHoldActivity.action_hold);
                }
            }

            @Override
            public void onDetailClick(MasterMobildijual mobilDijual) {

                Intent intent = new Intent(MobilDijualActivity.this, DetailMobilDijualActivity.class);
                intent.putExtra("name",mobilDijual.getNama_Pabrikan());
                intent.putExtra("price",mobilDijual.getHargajual());
                intent.putExtra("buy",mobilDijual.getHargabeli());
                intent.putExtra("police",mobilDijual.getPlatnomor());
                intent.putExtra("type",mobilDijual.getNama_varian());
                intent.putExtra("expstnk",mobilDijual.getExpstnk());
                intent.putExtra("bahanbakar",mobilDijual.getBahanbakar());
                intent.putExtra("year",mobilDijual.getTahun());
                intent.putExtra("transmission","");//hilang
                intent.putExtra("color",mobilDijual.getWarna());
                intent.putExtra("condition",mobilDijual.getKondisi());
                intent.putExtra("img",mobilDijual.getUrl());
                intent.putExtra("id",mobilDijual.getId_datamobil());
                intent.putExtra("idjenismobil",mobilDijual.getId_jenismobil());
                intent.putExtra("idpabrikan",mobilDijual.getId_pabrikan());
                intent.putExtra("idvarian",mobilDijual.getId_varian());
                intent.putExtra("idshowroom",mobilDijual.getIdshowroom());
                intent.putExtra("booking",mobilDijual.getStatus_booking());
                intent.putExtra("hold",mobilDijual.getStatus_hold());
                startActivityForResult(intent, 1);
            }

            @Override
            public void onWistlist(MasterMobildijual mobilDijual) {
                        strIdDataMobil = String.valueOf(mobilDijual.getId_datamobil());
                        dialog = new AlertDialog.Builder(MobilDijualActivity.this);
                        inflater = getLayoutInflater();
                        dialogView = inflater.inflate(R.layout.wistlist_dialog, null);
                        dialog.setView(dialogView);
                        dialog.setCancelable(true);
                        final RatingBar ratingBar  = (RatingBar) dialogView.findViewById(R.id.ratingBar);
                        sp_customer = (Spinner) dialogView.findViewById(R.id.sp_customer);
                        progressBarRating = (ProgressBar) dialogView.findViewById(R.id.progressBarRating);
                        setCustomers();
                        Button btn_save  = (Button) dialogView.findViewById(R.id.btn_save);
                        btn_save.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                progressBarRating.setVisibility(View.VISIBLE);
                                strRating = String.valueOf(ratingBar.getRating());
                                setRating();

                            }
                        });

                        show = dialog.show();
                    }

        });
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MobilDijualActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(mobilDijualAdapter);

        etKeyword = findViewById(R.id.et_keyword);
        etKeyword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                System.out.println("SEARCH : "+charSequence);
                if (charSequence!=null)
                    mobilDijualAdapter.getFilter().filter(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void setCustomers(){

        String[]spCustomers = new String[customers.size()];

        for (int i=0; i<customers.size();i++){
            spCustomers[i] = customers.get(i).getNamacust();
        }

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_dropdown_item, spCustomers);
        sp_customer.setAdapter(adapter);
        sp_customer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                strCustomerId = customers.get(i).getIdcust();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    public void getCars() {
        pDialog = new ProgressDialog(MobilDijualActivity.this);
        pDialog.setCancelable(false);
        pDialog.setMessage("Loading ...");
        showDialog();
        RequestQueue queue = SingletonRequestQueue.getInstance(MobilDijualActivity.this).getRequestQueue();

        VolleyLog.DEBUG = true;
        String uri = Constants.IP+"mobildijual";

        StringRequest stringRequestPOSTJSON = new StringRequest(Request.Method.GET, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                hideDialog();
                Gson gson = new Gson();
                try{

                    Type mobilDijualListType = new TypeToken<ArrayList<MasterMobildijual>>(){}.getType();
                    mobilDijuals = gson.fromJson(response, mobilDijualListType);
                }catch (Exception e){

                }
                CarList();
            }
        }, errorListener) {
            @Override
            public Request.Priority getPriority() {
                return Request.Priority.HIGH;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", Constants.CONTENT_TYPE);
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                String requestBody = null;
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        queue.add(stringRequestPOSTJSON);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request request) {
                hideDialog();
            }
        });
    }

    public void requestUnHold(final int id){
        RequestQueue queue = SingletonRequestQueue.getInstance(MobilDijualActivity.this).getRequestQueue();
        VolleyLog.DEBUG = true;
        String uri = Constants.IP+"unholdmobil";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                VolleyLog.wtf(response);
                Toast.makeText(MobilDijualActivity.this, response, Toast.LENGTH_SHORT).show();
                getCars();
            }
        }, errorListener) {
            @Override
            public Priority getPriority() {
                return Priority.LOW;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("iddatamobil", String.valueOf(id));
                params.put("iduser", GlobalVar.getInstance().getId());

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
                return headers;
            }

        };
        queue.add(stringRequest);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request request) {
//                progressBar.setVisibility(View.GONE);
            }
        });
    }


    private void requestUnBooking(final String id, final String ishangus) {
        RequestQueue queue = SingletonRequestQueue.getInstance(MobilDijualActivity.this).getRequestQueue();
        VolleyLog.DEBUG = true;
        String uri = Constants.IP+"unbookingmobil";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                VolleyLog.wtf(response);
                Toast.makeText(MobilDijualActivity.this, response, Toast.LENGTH_SHORT).show();
                getCars();
            }
        }, errorListener) {
            @Override
            public Priority getPriority() {
                return Priority.LOW;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("iddatamobil", String.valueOf(id));
                params.put("iduser", GlobalVar.getInstance().getId());
                params.put("ishangus", ishangus);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
                return headers;
            }

        };
        queue.add(stringRequest);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request request) {
//                progressBar.setVisibility(View.GONE);
                show.dismiss();
            }
        });
    }

    public void customer_request(){
        RequestQueue queue = SingletonRequestQueue.getInstance(MobilDijualActivity.this).getRequestQueue();
        VolleyLog.DEBUG = true;
        String uri = Constants.IP+"customers";

        StringRequest stringRequestPOSTJSON = new StringRequest(Request.Method.GET, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Gson gson = new Gson();
                try{

                    Type customerType = new TypeToken<ArrayList<Customer>>(){}.getType();
                    customers = gson.fromJson(response, customerType);
                }catch (Exception e){

                }

            }
        }, errorListener) {
            @Override
            public Priority getPriority() {
                return Priority.HIGH;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", Constants.CONTENT_TYPE);
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                String requestBody = null;


                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        queue.add(stringRequestPOSTJSON);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request request) {

            }
        });
    }

    private void setRating() {

        RequestQueue queue = SingletonRequestQueue.getInstance(getApplicationContext()).getRequestQueue();

        VolleyLog.DEBUG = true;
        String uri = Constants.IP+"insertwishlist";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(MobilDijualActivity.this, response.replace("\"",""), Toast.LENGTH_SHORT).show();
                try{

                }catch (Exception e){

                }

            }
        }, errorListener) {
            @Override
            public Priority getPriority() {
                return Priority.LOW;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("idcust", strCustomerId);
                params.put("star", strRating);
                params.put("iddatamobil", strIdDataMobil);

                System.out.println("PARAM : "+ params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
                return headers;
            }

        };
        queue.add(stringRequest);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request request) {
                progressBarRating.setVisibility(View.GONE);
                show.dismiss();
            }
        });
    }

    Response.ErrorListener errorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            hideDialog();
            if (error instanceof NetworkError) {
                Toast.makeText(MobilDijualActivity.this, "No network available", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(MobilDijualActivity.this, error.toString(), Toast.LENGTH_LONG).show();
            }
        }
    };

    public void refreshCar(){
        getCars();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if(resultCode == Activity.RESULT_OK){
                refreshCar();
            }
        } else if (requestCode == FilterMobilDijualActivity.ACTION_FILTER){
            if (resultCode == FilterMobilDijualActivity.ACTION_FILTER_RESULT)
                mobilDijualAdapter.getFilter().filter(data.getStringExtra("filter"));
        } else if (requestCode == AddBookingActivity.action_booking){
            if (resultCode == AddBookingActivity.action_booking_result){
                refreshCar();
            }
        } else if (requestCode == AddPurchaseActivity.action_purchase){
            if (resultCode == AddPurchaseActivity.action_purchase_result){
                refreshCar();
            }
        } else if (requestCode == AddHoldActivity.action_hold){
            if (resultCode == AddHoldActivity.action_hold_success){
                refreshCar();
            }

        }

    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
