package com.oemartech.sis.activity.penjualan;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.oemartech.sis.R;
import com.oemartech.sis.adapter.filter.FuelAdapter;
import com.oemartech.sis.adapter.filter.ShowroomAdapter;
import com.oemartech.sis.adapter.filter.TypeAdapter;
import com.oemartech.sis.adapter.filter.YearAdapter;
import com.oemartech.sis.base.BaseActivity;
import com.oemartech.sis.model.MasterMobildijual;
import com.oemartech.sis.model.filter.Fuel;
import com.oemartech.sis.model.filter.Showroom;
import com.oemartech.sis.model.filter.Year;
import com.oemartech.sis.resful.SingletonRequestQueue;
import com.oemartech.sis.utils.Constants;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FilterMobilDijualActivity extends BaseActivity {

    public static final int ACTION_FILTER= 100;
    public static final int ACTION_FILTER_RESULT= 101;

    TextView datsun, honda , mitsubishi, suzuki, daihatsu, hyundai, nissan;
    TextView btn_10_50, btn_51_100, btn_101_200, btn_201_400, btn_501_1M;
    TextView btn_merah, btn_putih, btn_hitam;
    TextView btn_90_2000, btn_2001_2010, btn_2011_2020;

    RecyclerView rv_showroom;
    RecyclerView rv_type;
    RecyclerView rv_bbm;
    ProgressDialog pDialog;

    ShowroomAdapter showroomAdapter;
    FuelAdapter fuelAdapter;
    TypeAdapter typeAdapter;

    ArrayList<Showroom> showrooms = new ArrayList<>();
    ArrayList<Fuel> fuels = new ArrayList<>();
    ArrayList<Year> years = new ArrayList<>();
    ArrayList<com.oemartech.sis.model.filter.Type> types = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_mobil_dijual);

        datsun = findViewById(R.id.datsun);
        datsun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendBackData("h-"+datsun.getText().toString());
            }
        });
        honda = findViewById(R.id.honda);
        honda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendBackData("h-"+honda.getText().toString());
            }
        });
        mitsubishi = findViewById(R.id.mitsubishi);
        mitsubishi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendBackData("h-"+mitsubishi.getText().toString());
            }
        });
        suzuki = findViewById(R.id.suzuki);
        suzuki.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendBackData("h-"+suzuki.getText().toString());
            }
        });
        daihatsu = findViewById(R.id.daihatsu);
        daihatsu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendBackData("h-"+daihatsu.getText().toString());
            }
        });
        hyundai = findViewById(R.id.hyundai);
        hyundai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendBackData("h-"+hyundai.getText().toString());
            }
        });
        nissan = findViewById(R.id.nissan);
        nissan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendBackData("h-"+nissan.getText().toString());
            }
        });

        btn_10_50 = findViewById(R.id.btn_10_50);
        btn_10_50.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendBackData("n-50000000");
            }
        });
        btn_51_100 = findViewById(R.id.btn_51_100);
        btn_51_100.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendBackData("n-100000000");
            }
        });
        btn_101_200 = findViewById(R.id.btn_101_200);
        btn_101_200.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendBackData("n-200000000");
            }
        });
        btn_201_400 = findViewById(R.id.btn_201_400);
        btn_201_400.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendBackData("n-400000000");
            }
        });
        btn_501_1M = findViewById(R.id.btn_501_1M);
        btn_501_1M.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendBackData("n-1000000000");
            }
        });

        btn_90_2000 = findViewById(R.id.btn_90_2000);
        btn_90_2000.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendBackData("t-1990");
            }
        });
        btn_2001_2010 = findViewById(R.id.btn_2001_2010);
        btn_2001_2010.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendBackData("t-2001");
            }
        });
        btn_2011_2020 = findViewById(R.id.btn_2011_2020);
        btn_2011_2020.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendBackData("t-2011");
            }
        });

        btn_merah = findViewById(R.id.btn_merah);
        btn_merah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendBackData("h-"+btn_merah.getText().toString());
            }
        });

        btn_putih = findViewById(R.id.btn_putih);
        btn_putih.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendBackData("h-"+btn_putih.getText().toString());
            }
        });

        btn_hitam = findViewById(R.id.btn_hitam);
        btn_hitam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendBackData("h-"+btn_hitam.getText().toString());
            }
        });

        ImageView btn_back = findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        rv_bbm = findViewById(R.id.rv_bbm);
        rv_showroom = findViewById(R.id.rv_showroom);
        rv_type = findViewById(R.id.rv_type);

        getReference();

    }

    public void setRv_showroom(){
        showroomAdapter = new ShowroomAdapter(showrooms, new ShowroomAdapter.ShowroomListener() {
            @Override
            public void onBodyClick(Showroom showroom) {
                sendBackData("h-"+showroom.getNamashowroom());
            }
        });
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(FilterMobilDijualActivity.this);
        rv_showroom.setLayoutManager(layoutManager);
        rv_showroom.setAdapter(showroomAdapter);
    }

    public void setRv_bbm() {
        fuelAdapter = new FuelAdapter(fuels, new FuelAdapter.FuelListener() {
            @Override
            public void onBodyClick(Fuel fuel) {
                sendBackData("h-"+fuel.getBahanbakar());
            }
        });
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(FilterMobilDijualActivity.this);
        rv_bbm.setLayoutManager(layoutManager);
        rv_bbm.setAdapter(fuelAdapter);
    }



    public void setRv_type(){
        typeAdapter = new TypeAdapter(types, new TypeAdapter.TypeListener(){
            @Override
            public void onBodyClick(com.oemartech.sis.model.filter.Type type) {
                sendBackData("h-"+type.getNamajenis());
            }
        });
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(FilterMobilDijualActivity.this);
        rv_type.setLayoutManager(layoutManager);
        rv_type.setAdapter(typeAdapter);
    }

    public void getReference() {
        pDialog = new ProgressDialog(FilterMobilDijualActivity.this);
        pDialog.setCancelable(false);
        pDialog.setMessage("Loading ...");
        showDialog();
        RequestQueue queue = SingletonRequestQueue.getInstance(FilterMobilDijualActivity.this).getRequestQueue();

        VolleyLog.DEBUG = true;
        String uriShowroom = Constants.IP+"listshowroom";
        String uriFuel = Constants.IP+"listbahanbakar";
        String uriYear = Constants.IP+"listtahun";
        String uriType = Constants.IP+"listjenis";

        StringRequest getShowroom = new StringRequest(Request.Method.GET, uriShowroom, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                System.out.println("RESPINSE 1 : "+ response);
                Gson gson = new Gson();
                try{

                    Type showroomType = new TypeToken<ArrayList<Showroom>>(){}.getType();
                    showrooms = gson.fromJson(response, showroomType);
                }catch (Exception e){

                }
                setRv_showroom();
            }
        }, errorListener) {
            @Override
            public Request.Priority getPriority() {
                return Request.Priority.HIGH;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", Constants.CONTENT_TYPE);
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                String requestBody = null;
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };

        StringRequest getFuel = new StringRequest(Request.Method.GET, uriFuel, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                System.out.println("RESPINSE 2 : "+ response);
                Gson gson = new Gson();
                try{

                    Type fuelType = new TypeToken<ArrayList<Fuel>>(){}.getType();
                    fuels = gson.fromJson(response, fuelType);
                }catch (Exception e){

                }
                setRv_bbm();
            }
        }, errorListener) {
            @Override
            public Request.Priority getPriority() {
                return Request.Priority.HIGH;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", Constants.CONTENT_TYPE);
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                String requestBody = null;
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };

        StringRequest getType = new StringRequest(Request.Method.GET, uriType, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                System.out.println("RESPINSE 4 : "+ response);
                Gson gson = new Gson();
                try{

                    Type yearType = new TypeToken<ArrayList<com.oemartech.sis.model.filter.Type>>(){}.getType();
                    years = gson.fromJson(response, yearType);
                }catch (Exception e){

                }
                setRv_type();
            }
        }, errorListener) {
            @Override
            public Request.Priority getPriority() {
                return Request.Priority.HIGH;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", Constants.CONTENT_TYPE);
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                String requestBody = null;
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };

        queue.add(getShowroom);
        queue.add(getFuel);
        queue.add(getType);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request request) {
                hideDialog();
            }
        });
    }

    Response.ErrorListener errorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            hideDialog();
            if (error instanceof NetworkError) {
                Toast.makeText(FilterMobilDijualActivity.this, "No network available", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(FilterMobilDijualActivity.this, error.toString(), Toast.LENGTH_LONG).show();
            }
        }
    };

    public void sendBackData(String value){
        Intent returnIntent = new Intent();
        returnIntent.putExtra("filter", value);
        setResult(ACTION_FILTER_RESULT,returnIntent);
        finish();
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
