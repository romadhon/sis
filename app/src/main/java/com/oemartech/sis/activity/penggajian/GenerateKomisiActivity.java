package com.oemartech.sis.activity.penggajian;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.oemartech.sis.R;
import com.oemartech.sis.base.BaseActivity;

/**
 * Created by Sevima on 9/6/2018.
 */

public class GenerateKomisiActivity extends BaseActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generate_komisi);
        TextView title = (TextView) findViewById(R.id.title);
        title.setText("Generate Komisi");

        ImageView back = (ImageView) findViewById(R.id.btn_back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        ImageView add = (ImageView) findViewById(R.id.btn_add);
        add.setVisibility(View.GONE);

    }
}
