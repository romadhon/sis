package com.oemartech.sis.activity.penggajian;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.oemartech.sis.R;
import com.oemartech.sis.base.BaseActivity;
import com.oemartech.sis.model.DetailKomisi;
import com.oemartech.sis.model.Gaji;
import com.oemartech.sis.model.Komisi;
import com.oemartech.sis.resful.SingletonRequestQueue;
import com.oemartech.sis.utils.Constants;
import com.oemartech.sis.utils.CurrencyUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Sevima on 9/6/2018.
 */

public class DetailKomisiActivity extends BaseActivity {
    TextView tv_mobil, tv_nomor_polisi, tv_showroom, tv_harga, tv_nama_customer;
    String id_beli;
    ArrayList<DetailKomisi> detailKomisis = new ArrayList<>();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_komisi);
        TextView title = (TextView) findViewById(R.id.title);
        title.setText("Detail Komisi");

        ImageView back = (ImageView) findViewById(R.id.btn_back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        ImageView add = (ImageView) findViewById(R.id.btn_add);
        add.setVisibility(View.GONE);

        tv_mobil = findViewById(R.id.tv_mobil);
        tv_nomor_polisi = findViewById(R.id.tv_nomor_polisi);
        tv_showroom = findViewById(R.id.tv_showroom);
        tv_harga = findViewById(R.id.tv_harga);
        tv_nama_customer = findViewById(R.id.tv_nama_customer);

        id_beli = getIntent().getExtras().getString("idbeli");
        getDetail(id_beli);
    }

    private void getDetail(String id_beli) {

        RequestQueue queue = SingletonRequestQueue.getInstance(DetailKomisiActivity.this).getRequestQueue();

        VolleyLog.DEBUG = true;
        String uri = Constants.IP+"detaildbeli/"+id_beli;

        StringRequest stringRequestPOSTJSON = new StringRequest(Request.Method.GET, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Gson gson = new Gson();
                try{

                    Type komisiType = new TypeToken<ArrayList<DetailKomisi>>(){}.getType();
                    detailKomisis = gson.fromJson(response, komisiType);
                }catch (Exception e){

                }

                for (int i = 0; i<detailKomisis.size(); i++){
                    tv_mobil.setText(detailKomisis.get(i).getNamajenismobil());
                    tv_nomor_polisi.setText(detailKomisis.get(i).getPlatnomor());
                    tv_showroom.setText(detailKomisis.get(i).getNamashowroom());
                    tv_harga.setText(CurrencyUtils.rupiah(Double.parseDouble(detailKomisis.get(i).getHargaakhir())));
                    tv_nama_customer.setText(detailKomisis.get(i).getNamacust());

                }

            }
        }, errorListener) {
            @Override
            public Request.Priority getPriority() {
                return Request.Priority.HIGH;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", Constants.CONTENT_TYPE);
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                String requestBody = null;


                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        queue.add(stringRequestPOSTJSON);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request request) {
            }
        });
    }

    Response.ErrorListener errorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            if (error instanceof NetworkError) {
                Toast.makeText(DetailKomisiActivity.this, "No network available", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(DetailKomisiActivity.this, error.toString(), Toast.LENGTH_LONG).show();
            }
        }
    };

}
