package com.oemartech.sis.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.oemartech.sis.R;
import com.oemartech.sis.base.BaseActivity;


public class SplashScreenActivity extends BaseActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
}
