package com.oemartech.sis.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.google.firebase.iid.FirebaseInstanceId;
import com.oemartech.sis.MainActivity;
import com.oemartech.sis.R;
import com.oemartech.sis.base.BaseActivity;
import com.oemartech.sis.model.GlobalVar;
import com.oemartech.sis.resful.SingletonRequestQueue;
import com.oemartech.sis.utils.ValidationUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends BaseActivity {
    ProgressDialog pDialog;
    EditText txt_username;
    EditText txt_password;
    SharedPreferences sharedpreferences;
    public static final String my_shared_preferences = "my_shared_preferences";
    public static final String session_status = "session_status";
    String androidToken;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        androidToken = FirebaseInstanceId.getInstance().getToken();
        txt_username = (EditText) findViewById(R.id.txt_username);
        txt_password = (EditText) findViewById(R.id.txt_password);
//        txt_username.setText("akbar");
//        txt_password.setText("akbar123");
        Button btn_login = (Button) findViewById(R.id.btn_login);
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validasi();
            }
        });

    }

    public void validasi(){
        if (txt_username.getText().toString().isEmpty()){
            txt_username.setError("Silakan isi username");
        }else if(txt_password.getText().toString().isEmpty()){
            txt_password.setError("Silakan isi password");
        }else{
            pDialog = new ProgressDialog(this);
            pDialog.setCancelable(false);
            pDialog.setMessage("Logging in ...");
            showDialog();
            LoginRequest();
        }
    }

    private void LoginRequest() {

        RequestQueue queue = SingletonRequestQueue.getInstance(getApplicationContext()).getRequestQueue();

        VolleyLog.DEBUG = true;
        String uri = "http://showroom.theidealstore.id/public/api/login";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                 try{

                    if (ValidationUtils.cekValidResult(response, LoginActivity.this)) {
                        JSONObject jsonObject = new JSONObject(response);
                        String data = jsonObject.getString("data");
                        JSONArray jsonArray = new JSONArray(data);
                        GlobalVar.getInstance().setId(jsonArray.getJSONObject(0).getString("id"));
                        GlobalVar.getInstance().setRole(jsonArray.getJSONObject(0).getString("role"));
                        GlobalVar.getInstance().setPassword(jsonArray.getJSONObject(0).getString("password"));
                        GlobalVar.getInstance().setNama(jsonArray.getJSONObject(0).getString("name"));
                        GlobalVar.getInstance().setEmail(jsonArray.getJSONObject(0).getString("email"));
                        GlobalVar.getInstance().setNohp(jsonArray.getJSONObject(0).getString("nohp"));
                        GlobalVar.getInstance().setAlamat(jsonArray.getJSONObject(0).getString("alamat"));
                        GlobalVar.getInstance().setImageUrl(jsonArray.getJSONObject(0).getString("url"));

                        Intent intent = new Intent(LoginActivity.this,MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    }

                }catch (Exception e){

                }

            }
        }, errorListener) {
            @Override
            public Priority getPriority() {
                return Priority.LOW;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("name", txt_username.getText().toString());
                params.put("password", txt_password.getText().toString());
                params.put("android_token", androidToken);
                System.out.println("PARAM : "+params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
                return headers;
            }

        };
        queue.add(stringRequest);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request request) {
                hideDialog();
            }
        });
    }

    Response.ErrorListener errorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            hideDialog();
            if (error instanceof NetworkError) {
                Toast.makeText(getApplicationContext(), "No network available", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_LONG).show();
            }
        }
    };

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
