package com.oemartech.sis.activity.refund;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.oemartech.sis.MainActivity;
import com.oemartech.sis.R;
import com.oemartech.sis.activity.LoginActivity;
import com.oemartech.sis.base.BaseActivity;
import com.oemartech.sis.model.Bank;
import com.oemartech.sis.model.GlobalVar;
import com.oemartech.sis.resful.SingletonRequestQueue;
import com.oemartech.sis.utils.Constants;
import com.oemartech.sis.utils.CurrencyUtils;
import com.oemartech.sis.utils.ValidationUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class RefundBackDetailActivity extends BaseActivity {

    TextView title;
    ImageView img;
    TextView tv_id;
    TextView tv_police_number;
    TextView tv_car_name;
    TextView tv_customer_name;
    TextView tv_mount;
    Spinner sp_bank;
    EditText et_nominal;
    Button btn_save;
    ImageView btn_back;
    ImageView btn_add;
    ProgressBar progressBar;

    String strImg;
    String strId;
    String strIdUser;
    String strIdBank;
    String strPoliceNumber;
    String strCarName;
    String strCustomerName;
    Double strMount;
    ArrayList<Bank> banks = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_refund_bank_detail);

        title = findViewById(R.id.title);
        img = findViewById(R.id.img);
        tv_id = findViewById(R.id.tv_id);
        tv_police_number = findViewById(R.id.tv_police_number);
        tv_car_name = findViewById(R.id.tv_car_name);
        tv_customer_name = findViewById(R.id.tv_customer_name);
        tv_mount = findViewById(R.id.tv_mount);
        sp_bank = findViewById(R.id.sp_bank);
        et_nominal = findViewById(R.id.et_nominal);
        progressBar = findViewById(R.id.progressBar);

        title.setText("Refund Bank");
        if (getIntent().getExtras()!=null){
            strImg = getIntent().getExtras().getString("strImg");
            strId = getIntent().getExtras().getString("strId");
            strIdUser = getIntent().getExtras().getString("strIdUser");
            strPoliceNumber = getIntent().getExtras().getString("strPoliceNumber");
            strCarName = getIntent().getExtras().getString("strCarName");
            strCustomerName = getIntent().getExtras().getString("strCustomerName");
            strMount = getIntent().getExtras().getDouble("strMount");
        }


        if (strImg!=null)
            Glide.with(RefundBackDetailActivity.this).load(Constants.ASSETS+strImg).into(img);

        if (strId!=null)
            tv_id.setText(strId);

        if (strPoliceNumber!=null)
            tv_police_number.setText(strPoliceNumber);

        if (strCarName!=null)
            tv_car_name.setText(strCarName);

        if (strCustomerName!=null)
            tv_customer_name.setText(strCustomerName);

        if (strMount!=null)
            tv_mount.setText(CurrencyUtils.rupiah(strMount));

        btn_save = findViewById(R.id.btn_save);
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (et_nominal.getText().toString().isEmpty()){
                    Toast.makeText(RefundBackDetailActivity.this, "Nominal harus diisi", Toast.LENGTH_SHORT).show();
                } else {
                    progressBar.setVisibility(View.VISIBLE);
                    setRefund();
                }
            }
        });
        btn_back = findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        btn_add = findViewById(R.id.btn_add);
        btn_add.setVisibility(View.GONE);

        getBank();
    }

    private void setBank(){

        String[]spBanks = new String[banks.size()];

        for (int i=0; i<banks.size();i++){
            spBanks[i] = banks.get(i).getNamabank();
        }

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_dropdown_item, spBanks);
        sp_bank.setAdapter(adapter);
        sp_bank.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                strIdBank = banks.get(i).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private void getBank() {
        progressBar.setVisibility(View.VISIBLE);
        RequestQueue queue = SingletonRequestQueue.getInstance(RefundBackDetailActivity.this).getRequestQueue();

        VolleyLog.DEBUG = true;
        String uri = Constants.IP+"listbank";

        StringRequest getBank = new StringRequest(Request.Method.GET, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Gson gson = new Gson();
                try{
                    Type bankType = new TypeToken<ArrayList<Bank>>(){}.getType();
                    banks = gson.fromJson(response, bankType);
                }catch (Exception e){

                }
                setBank();
            }
        }, errorListener) {
            @Override
            public Priority getPriority() {
                return Priority.HIGH;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", Constants.CONTENT_TYPE);
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                String requestBody = null;


                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        queue.add(getBank);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request request) {
                progressBar.setVisibility(View.GONE);
            }
        });
    }


    private void setRefund() {

        RequestQueue queue = SingletonRequestQueue.getInstance(getApplicationContext()).getRequestQueue();

        VolleyLog.DEBUG = true;
        String uri = Constants.IP+"uploadrefund";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(RefundBackDetailActivity.this, response.replace("\"",""), Toast.LENGTH_SHORT).show();
                try{

                }catch (Exception e){

                }

            }
        }, errorListener) {
            @Override
            public Priority getPriority() {
                return Priority.LOW;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("iddbeli", strId);
                params.put("idbank", strIdBank);
                params.put("nominal_refund", et_nominal.getText().toString().replace(",",""));
                params.put("iduser", strIdUser);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
                return headers;
            }

        };
        queue.add(stringRequest);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request request) {
                progressBar.setVisibility(View.GONE);
            }
        });
    }


    Response.ErrorListener errorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            if (error instanceof NetworkError) {
                Toast.makeText(RefundBackDetailActivity.this, "No network available", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(RefundBackDetailActivity.this, error.toString(), Toast.LENGTH_LONG).show();
            }
        }
    };

}
