package com.oemartech.sis.activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.oemartech.sis.R;
import com.oemartech.sis.adapter.CustomerAdapter;
import com.oemartech.sis.base.BaseActivity;
import com.oemartech.sis.model.Customer;
import com.oemartech.sis.resful.SingletonRequestQueue;
import com.oemartech.sis.utils.Constants;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class EmployeeBirthDayActivity extends BaseActivity {
    ImageView btn_back;
    RecyclerView rv_customer;
    CustomerAdapter customerAdapter;
    ArrayList<Customer> customerArrayList = new ArrayList<>();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_birthday);
        rv_customer = findViewById(R.id.rv_customer);

        btn_back = findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        customer_request();
    }

    private void memberList(){
        customerAdapter = new CustomerAdapter(customerArrayList, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(EmployeeBirthDayActivity.this);
        rv_customer.setLayoutManager(layoutManager);
        rv_customer.setNestedScrollingEnabled(false);
        rv_customer.setAdapter(customerAdapter);
    }


    public void customer_request(){
        RequestQueue queue = SingletonRequestQueue.getInstance(EmployeeBirthDayActivity.this).getRequestQueue();

        VolleyLog.DEBUG = true;
        String uri = "http://showroom.theidealstore.id/public/api/birthdaycust";

        StringRequest stringRequestPOSTJSON = new StringRequest(Request.Method.GET, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Gson gson = new Gson();
                try{

                    Type memberType = new TypeToken<ArrayList<Customer>>(){}.getType();
                    customerArrayList = gson.fromJson(response, memberType);
                }catch (Exception e){

                }
                memberList();
            }
        }, errorListener) {
            @Override
            public Request.Priority getPriority() {
                return Request.Priority.HIGH;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", Constants.CONTENT_TYPE);
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                String requestBody = null;


                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        queue.add(stringRequestPOSTJSON);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request request) {
            }
        });
    }

    Response.ErrorListener errorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            if (error instanceof NetworkError) {
                Toast.makeText(EmployeeBirthDayActivity.this, "No network available", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(EmployeeBirthDayActivity.this, error.toString(), Toast.LENGTH_LONG).show();
            }
        }
    };
}
