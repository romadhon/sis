package com.oemartech.sis.activity.pembelian;

import android.app.Dialog;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.oemartech.sis.R;
import com.oemartech.sis.adapter.MemberAdapter;
import com.oemartech.sis.base.BaseActivity;
import com.oemartech.sis.fragment.AddCustimerFragment;
import com.oemartech.sis.model.Member;
import com.oemartech.sis.resful.SingletonRequestQueue;
import com.oemartech.sis.utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Sevima on 9/28/2018.
 */

public class AddMemberActivity extends BaseActivity {
    ImageView btn_back;
    TextView title;
    ImageView btn_add;
    RecyclerView recyclerView;
    Context context;
    MemberAdapter memberAdapter;
    ArrayList<Member> members = new ArrayList<>();
    FragmentManager manager;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_member);

        context = this;
        title = (TextView) findViewById(R.id.title);
        title.setText("Member");

        manager = getFragmentManager();
        android.app.Fragment frag = manager.findFragmentByTag("customer");
        if (frag != null) {
            manager.beginTransaction().remove(frag).commit();
        }

        btn_add = (ImageView) findViewById(R.id.btn_add);
        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AddCustimerFragment addCustomer = new AddCustimerFragment();
                Bundle bundle = new Bundle();
                bundle.putBoolean("iscreate",true);
                bundle.putString("from","addmember");
                addCustomer.setArguments(bundle);
                addCustomer.show(manager, "customer");
            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        customer_request();

        btn_back = (ImageView) findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    public void MemberList(){

        System.out.println("MEMBER SIZE: "+ members.size());
        memberAdapter = new MemberAdapter(members, this, new MemberAdapter.MemberAdapterListener() {
            @Override
            public void onDeleteClick(final int id) {
                new AlertDialog.Builder(context)
                        .setTitle("SIS")
                        .setMessage("Apa anda yakin ingin menghapus?")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton("Ya", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                                delete_customer_request(id);
                            }})
                        .setNegativeButton("Tidak", null).show();
            }

            @Override
            public void onEditClick(int id, String nama, String telp, String alamat) {
//                edit_customer(id,nama,telp,alamat);

                AddCustimerFragment addCustomer = new AddCustimerFragment();
                Bundle bundle = new Bundle();
                bundle.putBoolean("iscreate",false);
                bundle.putInt("id",id);
                bundle.putString("name",nama);
                bundle.putString("telp",telp);
                bundle.putString("address",alamat);
                addCustomer.setArguments(bundle);
                addCustomer.show(manager, "customer");
            }
        });
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setAdapter(memberAdapter);
    }

    public void customer_request(){
        RequestQueue queue = SingletonRequestQueue.getInstance(context).getRequestQueue();

        VolleyLog.DEBUG = true;
        String uri = "http://showroom.theidealstore.id/public/api/customers";

        StringRequest stringRequestPOSTJSON = new StringRequest(Request.Method.GET, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                members =  new ArrayList<>();
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0; i< jsonArray.length(); i++){
                        int id = jsonArray.getJSONObject(i).getInt("idcust");
                        String name = jsonArray.getJSONObject(i).getString("namacust");
                        String type = jsonArray.getJSONObject(i).getString("telpcust");
                        String transmission = jsonArray.getJSONObject(i).getString("alamatcust");
                        members.add(new Member(id, name, type,transmission));
                    }
                    MemberList();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("RESPONSE : "+response);
            }
        }, errorListener) {
            @Override
            public Request.Priority getPriority() {
                return Request.Priority.HIGH;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", Constants.CONTENT_TYPE);
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                String requestBody = null;


                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        queue.add(stringRequestPOSTJSON);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request request) {
            }
        });
    }


    public void delete_customer_request(final int id){
        RequestQueue queue = SingletonRequestQueue.getInstance(context).getRequestQueue();

        VolleyLog.DEBUG = true;
        String uri = "http://showroom.theidealstore.id/public/api/deletecustomer/"+id;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                VolleyLog.wtf(response);
                customer_request();
            }
        }, errorListener) {
            @Override
            public Priority getPriority() {
                return Priority.LOW;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
                return headers;
            }

        };
        queue.add(stringRequest);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request request) {
            }
        });
    }


    Response.ErrorListener errorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            if (error instanceof NetworkError) {
                Toast.makeText(context, "No network available", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(context, error.toString(), Toast.LENGTH_LONG).show();
            }
        }
    };

    public void refreshCustomer(){
        customer_request();
    }
}
