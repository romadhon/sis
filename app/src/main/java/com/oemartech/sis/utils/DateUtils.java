package com.oemartech.sis.utils;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

public class DateUtils {

    public static int getMonth(){
        java.util.Date date= new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int month = cal.get(Calendar.MONTH);
        return month + 1;
    }

    // 12 Januari 1994
    public static String getDate(String date){
        String indoDate="";
        String tahun ;
        String bulan = "";
        String tanggal;

        String[] strDate = date.split("-");

        tahun = strDate[0];

            if (Integer.parseInt(strDate[1]) == 1){
                bulan = "Januari";
            } else if (Integer.parseInt(strDate[1]) == 2) {
                bulan = "Februari";
            } else if (Integer.parseInt(strDate[1]) == 3) {
                bulan = "Maret";
            } else if (Integer.parseInt(strDate[1]) == 4) {
                bulan = "April";
            } else if (Integer.parseInt(strDate[1]) == 5) {
                bulan = "Mei";
            } else if (Integer.parseInt(strDate[1]) == 6) {
                bulan = "Juni";
            } else if (Integer.parseInt(strDate[1]) == 7) {
                bulan = "Juli";
            } else if (Integer.parseInt(strDate[1]) == 8) {
                bulan = "Agustus";
            } else if (Integer.parseInt(strDate[1]) == 9) {
                bulan = "September";
            } else if (Integer.parseInt(strDate[1]) == 10) {
                bulan = "Oktober";
            } else if (Integer.parseInt(strDate[1]) == 11) {
                bulan = "November";
            } else if (Integer.parseInt(strDate[1]) == 12) {
                bulan = "Desember";
            }

        tanggal = strDate[2];

        return tanggal+" "+ bulan+  " "+ tahun;
    }
}
