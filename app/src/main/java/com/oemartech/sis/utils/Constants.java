package com.oemartech.sis.utils;

public class Constants {
    public static final String IP = "http://showroom.theidealstore.id/public/api/";
    public static final String CONTENT_TYPE="application/x-www-form-urlencoded";
    public static final String ASSETS="http://showroom.theidealstore.id/storage/app/public/";

    // ROLE
    public static final String ROLE_MARKETING = "3";
    public static final String ROLE_OPERATOR = "2";
    public static final String ROLE_OWNER = "1";

    /*TITLE*/
    public static final String TITLE_DASHBOARD="Dashboard";
    public static final String TITLE_GENERATE_KOMISI="Generate Komisi";
    public static final String TITLE_GAJI_POKOK="Gaji Pokok Pegawai";
    public static final String TITLE_GAJI_DAN_KOMISI="Gaji & Komisi";
    public static final String TITLE_MASTER_MOBIL="Master Car";
    public static final String TITLE_MASTER_TIPE_MOBIL="Master Car Type";
    public static final String TITLE_MASTER_PRODUK="Master Product";
    public static final String TITLE_REFUND_BANK="Refund Bank";
    public static final String TITLE_INPUT_WISHLIST_CUSTOMER="Input Wishlist Customer";
    public static final String TITLE_LAPORAN_PENJUALAN="Laporan Penjualan Mobil";
    public static final String TITLE_LAPORAN_KREDIT = "Laporan Penjualan Kredit";
    public static final String TITLE_LAPORAN_SHOWROOM = "Laporan Penjualan Showroom";
    public static final String TITLE_LAPORAN_LABA_RUGI="Laporan Laba Rugi";
    public static final String TITLE_LAPORAN_MARKETING="Laporan Marketing";
    public static final String TITLE_LAPORAN_MOBIL_TERLARIS="Laporan Mobil Terlaris";
    public static final String TITLE_LAPORAN_KOMISI_KARYAWAN="Laporan Komisi Karyawan";
    public static final String TITLE_LAPORAN_PEMBELIAN="Laporan Pembelian";
    public static final String TITLE_LAPORAN_PENGELUARAN="Laporan Pengeluaran";
    public static final String TITLE_LAPORAN_STOCK="Laporan Stok Mobil";


    /*TRANSAKSI*/
    public static final String PENJUALAN="Penjualan";
    public static final String PEMBELIAN="Pembelian";
    public static final String PENGELUARAN="Pengeluaran";

    /*INVENTORY*/
    public static final String KARTU_STOK="Kartu Stok";

    /*ADMIN*/
    public static final String ADD_STAFF="Staff";
    public static final String ADD_USER="Add User";
    public static final String HAK_AKSES="Hak Akses";
}
