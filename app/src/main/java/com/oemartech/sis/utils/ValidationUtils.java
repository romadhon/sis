package com.oemartech.sis.utils;

import android.app.Activity;
import android.util.Log;

import com.oemartech.sis.helper.CustomDialog;

import org.json.JSONException;
import org.json.JSONObject;

public class ValidationUtils {

    public static boolean cekValidResult(String result, Activity activity) {

        try {
            Log.i("Result xx ", result);

            JSONObject jsonObject = new JSONObject(result);
            String errorCode = jsonObject.getString("status");
            if (errorCode.equals("205")){
                String message = jsonObject.getString("message");
                CustomDialog.pesan(activity,message);
                return false;
            }else if (errorCode.equals("202")){
                String message = jsonObject.getString("message");
                CustomDialog.pesan(activity,message);
                return false;
            }else{
                return true;
            }

        } catch (JSONException e) {
            e.printStackTrace();

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
