package com.oemartech.sis.utils;

import android.app.Activity;
import android.content.SharedPreferences;

import com.oemartech.sis.model.SettingCreditCount;

public class SharedPreferenceUtils {
    public static final String key_provisi = "profisi";
    public static final String key_bunga = "bunga";
    public static final String key_asuransi = "asuransi";
    public static final String key_credit_protection = "credit_protection";
    public static final String key_admin_cost = "admin_cost";

    public static void seveCountCreditSetting(Activity activity, SettingCreditCount settingCreditCount){



        SharedPreferences pref = activity.getSharedPreferences("sis", 0); // 0 - for private mode
        SharedPreferences.Editor editor = pref.edit();

        editor.putInt(key_provisi, settingCreditCount.getProvisi());
        editor.putInt(key_bunga, settingCreditCount.getBunga());
        editor.putFloat(key_asuransi, settingCreditCount.getAsuransi());
        editor.putFloat(key_credit_protection, settingCreditCount.getCredit_protection());
        editor.putString(key_admin_cost, settingCreditCount.getAdmin_cost());

        editor.commit(); // commit changes

    }

    public static SettingCreditCount getCountCreditSetting(Activity activity){
        SharedPreferences pref = activity.getSharedPreferences("sis", 0); // 0 - for private mode

        int provisi = pref.getInt(key_provisi,0);
        int bunga =  pref.getInt(key_bunga,0);
        float asuransi = pref.getFloat(key_asuransi,0);
        float creditProtection = pref.getFloat(key_credit_protection,0);
        String adminCost = pref.getString(key_admin_cost,"0");

        return new SettingCreditCount(provisi, bunga, asuransi, creditProtection, adminCost);
    }
}
