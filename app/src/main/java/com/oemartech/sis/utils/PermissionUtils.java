package com.oemartech.sis.utils;

import com.oemartech.sis.model.GlobalVar;

public class PermissionUtils {

    public static boolean permissionSalaryMenu(){
        boolean value = false ;

        if (GlobalVar.getInstance().getRole().equals(Constants.ROLE_OWNER)){
            value = true;
        } else {
            value = false;
        }

        return value;
    }

    public static boolean permissionSalesMenu(){
        boolean value = false ;

        if (GlobalVar.getInstance().getRole().equals(Constants.ROLE_OWNER)
        || GlobalVar.getInstance().getRole().equals(Constants.ROLE_MARKETING)
        || GlobalVar.getInstance().getRole().equals(Constants.ROLE_OPERATOR)){
            value = true;
        } else {
            value = false;
        }

        return value;
    }

    public static boolean permissionPurchaseMenu(){
        boolean value = false ;

        if (GlobalVar.getInstance().getRole().equals(Constants.ROLE_OWNER)
                        || GlobalVar.getInstance().getRole().equals(Constants.ROLE_OPERATOR)){
            value = true;
        } else {
            value = false;
        }

        return value;
    }

    public static boolean permissionMasterCustomerMenu(){
        boolean value = false ;

        if (GlobalVar.getInstance().getRole().equals(Constants.ROLE_OWNER)
                || GlobalVar.getInstance().getRole().equals(Constants.ROLE_MARKETING)
                || GlobalVar.getInstance().getRole().equals(Constants.ROLE_OWNER)){
            value = true;
        } else {
            value = false;
        }

        return value;
    }

    public static boolean permissionMasterProductOrCarMenu(){
        boolean value = false ;

        if (GlobalVar.getInstance().getRole().equals(Constants.ROLE_OWNER)
                || GlobalVar.getInstance().getRole().equals(Constants.ROLE_OPERATOR)){
            value = true;
        } else {
            value = false;
        }

        return value;
    }

    public static boolean permissionAddUserMenu(){
        boolean value = false ;

        if (GlobalVar.getInstance().getRole().equals(Constants.ROLE_OWNER)
                || GlobalVar.getInstance().getRole().equals(Constants.ROLE_OPERATOR)){
            value = true;
        } else {
            value = false;
        }

        return value;
    }

    // REPORT

    public static boolean permissionSalesReportMenu(){
        boolean value = false ;

        if (GlobalVar.getInstance().getRole().equals(Constants.ROLE_OWNER)
                || GlobalVar.getInstance().getRole().equals(Constants.ROLE_OPERATOR)){
            value = true;
        } else {
            value = false;
        }

        return value;
    }

    public static boolean permissionPurchaseReportMenu(){
        boolean value = false ;

        if (GlobalVar.getInstance().getRole().equals(Constants.ROLE_OWNER)
                || GlobalVar.getInstance().getRole().equals(Constants.ROLE_OPERATOR)){
            value = true;
        } else {
            value = false;
        }

        return value;
    }

    public static boolean permissionStockReportMenu(){
        boolean value = false ;

        if (GlobalVar.getInstance().getRole().equals(Constants.ROLE_OWNER)
                || GlobalVar.getInstance().getRole().equals(Constants.ROLE_OPERATOR)){
            value = true;
        } else {
            value = false;
        }

        return value;
    }

    public static boolean permissionLabaRugiReportMenu(){
        boolean value = false ;

        if (GlobalVar.getInstance().getRole().equals(Constants.ROLE_OWNER)
                || GlobalVar.getInstance().getRole().equals(Constants.ROLE_OPERATOR)){
            value = true;
        } else {
            value = false;
        }

        return value;
    }

    public static boolean permissionKomisiReportMenu(){
        boolean value = false ;

        if (GlobalVar.getInstance().getRole().equals(Constants.ROLE_OWNER)
                || GlobalVar.getInstance().getRole().equals(Constants.ROLE_OPERATOR)){
            value = true;
        } else {
            value = false;
        }

        return value;
    }

    public static boolean permissionCreditReportMenu(){
        boolean value = false ;

        if (GlobalVar.getInstance().getRole().equals(Constants.ROLE_OWNER)
                || GlobalVar.getInstance().getRole().equals(Constants.ROLE_OPERATOR)){
            value = true;
        } else {
            value = false;
        }

        return value;
    }

    public static boolean permissionPengeluaranReportMenu(){
        boolean value = false ;

        if (GlobalVar.getInstance().getRole().equals(Constants.ROLE_OWNER)
                || GlobalVar.getInstance().getRole().equals(Constants.ROLE_OPERATOR)){
            value = true;
        } else {
            value = false;
        }

        return value;
    }
}
